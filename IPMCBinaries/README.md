# CERN-IPMC Binaries

This folder contains the binaries to configure the CERN-IPMC module as required for an automatic test.

Only the Software part changes from the nominal module's configuration.
Therefore, when the module's FPGA is already configured, it can be flashed using IPMITOOL as described into the CERN-IPMC documentation.

Otherwise, the procedure to follow is described below (should concern new modules only - or FPGA configuration upgrade).

# Dependencies

List of dependency(ies): 
- <b>FlashPRO</b>: https://www.microsemi.com/product-directory/programming/4977-flashpro#software

# How to

- Open FlashPro and create a new project

<img src="http://ep-ese-xtca.web.cern.ch/IPMC/img/flashpro_new_project.png" />

- Open Configure device

<img src="http://ep-ese-xtca.web.cern.ch/IPMC/img/flashpro_configure_device.png" />

- Browse PDB file and select ipmc-tester\IPMCBinaries\Images\A2F200-ATCA.pdb

<img src="http://ep-ese-xtca.web.cern.ch/IPMC/img/flashpro_browse_pdb.png" />

- Click on modify and import the following files:
    - FPGA Array: ipmc-tester\IPMCBinaries\Images\A2F200-ATCA.pdb
    - FlashROM: ipmc-tester\IPMCBinaries\Images\MSS_UFROM_0.ufc
    - eNVM: ipmc-tester\IPMCBinaries\Images\MSS_ENVM_0.efc
    
<img src="http://ep-ese-xtca.web.cern.ch/IPMC/img/flashpro_modify_pdb.png" />

- Modify the FlashROM:

<img src="http://ep-ese-xtca.web.cern.ch/IPMC/img/flashpro_modify_flashrom.png" />

- Modify the eNVM:
    - Import content: ipmc-tester\IPMCBinaries\Images\nvm.ihx
    
<img src="http://ep-ese-xtca.web.cern.ch/IPMC/img/flashpro_modify_envm.png" />

And then, click Program (ensure that your FlashPRO cable is well connected to the IPMC_JTAG1 connector)
