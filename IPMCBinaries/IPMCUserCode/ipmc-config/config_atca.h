/* 
	General Application Parameters
*/ 
 
#define CFG_APP_FIRMWARE_REV1		0x01
#define CFG_APP_FIRMWARE_REV2		0x20

#define CFG_APP_DEVICE_ID			0x12 
#define CFG_APP_DEVICE_REV			0x00 
#define CFG_APP_MANUFACTURER_ID		0x000060L 
#define CFG_APP_PRODUCT_ID			0x1236 

#define CFG_RESET_ON_WRONG_HA

/* 
	General Serial interfaces Parameters
*/ 
 
#define CFG_ONCONNECTOR_UART			SDI_INTF 
#define CFG_REDIRECT_SDI_TO_SOL
 
/* 
	Handle switch 
*/ 

#define CFG_HANDLE_SWITCH_SIGNAL	\
	{								\
		pin: P(1, 9),				\
		active: SIGNAL_LOW,		\
		inactive: SIGNAL_HIGH		\
	}

/* 
	Hardware address signals 
*/ 

#define CFG_HARDWARE_ADDRESS_SIGNALS	\
	{									\
		pin: P(1, 0),					\
		active: SIGNAL_HIGH,			\
		inactive: SIGNAL_LOW			\
	},									\
	{									\
		pin: P(1, 1),					\
		active: SIGNAL_HIGH,			\
		inactive: SIGNAL_LOW			\
	},									\
	{									\
		pin: P(1, 2),					\
		active: SIGNAL_HIGH,			\
		inactive: SIGNAL_LOW			\
	},									\
	{									\
		pin: P(1, 3),					\
		active: SIGNAL_HIGH,			\
		inactive: SIGNAL_LOW			\
	},									\
	{									\
		pin: P(1, 4),					\
		active: SIGNAL_HIGH,			\
		inactive: SIGNAL_LOW			\
	},									\
	{									\
		pin: P(1, 5),					\
		active: SIGNAL_HIGH,			\
		inactive: SIGNAL_LOW			\
	},									\
	{									\
		pin: P(0, 18),					\
		active: SIGNAL_HIGH,			\
		inactive: SIGNAL_LOW			\
	},									\
	{									\
		pin: P(1, 7),					\
		active: SIGNAL_HIGH,			\
		inactive: SIGNAL_LOW			\
	}

