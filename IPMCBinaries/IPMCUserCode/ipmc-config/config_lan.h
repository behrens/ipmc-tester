/* 
	Define max Ethernet packet size of 640 bytes, 
	so that HPM.1 upgrades will be performed faster. 
*/ 
 
#define CFG_MAX_ETH_MSG_SIZE		1600 
 
/* 
	Serial Over Lan (SOL) parameters 
*/ 

#define CFG_SOL_ENABLE 
#define CFG_RMCPP_ENCRYPTION_ENABLE 
#define CFG_ANONYMOUS_LOGIN_ENABLE 
 
/* 
	SOL mode selection: 

		CFG_SOL_MODE_PT:		pass-through with the Intel 82571/82575 NC 
		CFG_SOL_MODE_SPT:		super pass-through with the Intel 82571 
		CFG_SOL_MODE_DIRECT:	direct Ethernet with Core10/100 
		CFG_SOL_MODE_NCSI:		NC-SI with Core10/100 and Intel 82575
		CFG_SOL_MODE_UMP:		UMP with Core10/100 and Broadcom 5714C
*/ 
#define CFG_SOL_MODE_DIRECT 
 
/* 
	Define the UART channel number for SOL. 
	This is used to enable corresponding UART support.
*/ 
#define CFG_SOL_CHANNEL			0
#define CFG_SOL_BAUD			115200 
 
/* 
	LAN parameters 
*/ 
#define CFG_LAN_CHANNEL1_MACADDR		{ 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x86 } 
#define CFG_LAN_CHANNEL1_IPADDR			{ 192, 168, 1, 34 }
#define CFG_LAN_CHANNEL1_NETMASK		{ 255, 255, 255, 0 }
#define CFG_LAN_CHANNEL1_GW_IPADDR		{ 192, 138, 1, 3 }
//#define CFG_LAN_CHANNEL1_IPADDR_SOURCE	IP_SOURCE_STATIC
#define CFG_LAN_CHANNEL1_IPADDR_SOURCE	IP_SOURCE_DHCP

#define CFG_HPM3_ENABLE
#define CFG_HPM2_ENABLE

/* 
	CFG_LAN_CHANNEL1_IPADDR_LIST provide IP addresses used to
	configure the channel interface according to the slot position 

	Configuration: {slot_addr, ip_addr}

		*Where slot_addr is the Hardware address 
			(from 0x41 [slot 1] to 0x50 [slot 16]) 
		*Where ip_addr is an array 
			(E.g: {192, 168, 1, 20} )

#define CFG_LAN_CHANNEL1_IPADDR_LIST {	\
	{ 0x41, {192,168,1,20} }, 			\
	{ 0x42, {192,168,1,21} }, 			\
	{ 0x43, {192,168,1,22} }, 			\
	{ 0x44, {192,168,1,23} }, 			\
	{ 0x45, {192,168,1,24} }, 			\
	{ 0x46, {192,168,1,25} }, 			\
	{ 0x47, {192,168,1,26} }, 			\
	{ 0x48, {192,168,1,27} }, 			\
	{ 0x49, {192,168,1,28} }, 			\
	{ 0x4a, {192,168,1,29} }, 			\
	{ 0x4b, {192,168,1,30} }, 			\
	{ 0x4c, {192,168,1,31} }, 			\
	{ 0x4d, {192,168,1,32} }, 			\
	{ 0x4e, {192,168,1,33} }, 			\
	{ 0x4f, {192,168,1,34} }, 			\
	{ 0x50, {192,168,1,35} }, 			\
	{ 0, {0,0,0,0} } 					\
}
*/

//#define DEBUG DEBUG_LEVEL_VERBOSE
