/* 
	Power properties
*/ 
#define CFG_APP_STABLE_POWER		50000	/* 50 Watts */

/* 
	Power ON Sequence
*/ 
#define CFG_PAYLOAD_POWER_ON_SEQ			\
	PSQ_ENABLE_SIGNAL(CFG_PAYLOAD_DCDC_EN_SIGNAL),		\
	PSQ_END

/* 
	Power OFF Sequence
*/ 
#define CFG_PAYLOAD_POWER_OFF_SEQ			\
	PSQ_DISABLE_SIGNAL(CFG_PAYLOAD_DCDC_EN_SIGNAL),		\
	PSQ_END
