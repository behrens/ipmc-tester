/* 
	Enable IRTM support
*/ 
#define CFG_IRTM_ENABLE 
#define CFG_MMCFRU_SITE_COUNT	1
#define CFG_IRTM_FRU_ID			(CFG_MODULE_FRU_ID_START+CFG_AMC_SITE_COUNT) 

#define CFG_MMCFRU_I2C_ADDR	0xea
/* 
	iRTM Site: hardware configuration
*/ 
#define CFG_MMCFRU_SITE_INFO								\
															\
	/* iRTM Site (0xea) */									\
	{														\
		entity_id: ATCA_ENTITY_ID_REAR_TRANSITION_MODULE,	\
		entity_instance: 0x60,								\
		site_type: ATCA_SITE_REAR_TRANSITION_MODULE,		\
		site_number: 0,										\
		targ_iface: IPMI_IFACE_IPMBL,						\
		ipmb_address: 0xea,									\
															\
		module_present_signal: {							\
			i2c_addr: 0x42,									\
			pin: PCA9555(15),								\
			active: SIGNAL_LOW,								\
			inactive: SIGNAL_HIGH							\
		},													\
															\
		module_enable_signal: {								\
			i2c_addr: 0x42,									\
			pin: PCA9555(14),								\
			active: SIGNAL_LOW,								\
			inactive: SIGNAL_HIGH							\
		},													\
															\
		mp_enable_signal: {									\
			i2c_addr: 0x44,									\
			pin: PCA9555(1),								\
			active: SIGNAL_HIGH,							\
			inactive: SIGNAL_LOW							\
		},													\
															\
		mp_good_signal: {									\
			i2c_addr: 0x44,									\
			pin: PCA9555(3),								\
			active: SIGNAL_HIGH,							\
			inactive: SIGNAL_LOW							\
		},													\
															\
		pwr_enable_signal: {								\
			i2c_addr: 0x44,									\
			pin: PCA9555(2),								\
			active: SIGNAL_HIGH,							\
			inactive: SIGNAL_LOW							\
		},													\
															\
		pwr_good_signal: {									\
			i2c_addr: 0x44,									\
			pin: PCA9555(4),								\
			active: SIGNAL_HIGH,							\
			inactive: SIGNAL_LOW							\
		},													\
															\
		ipmb_enable_signal: {								\
			i2c_addr: 0x44,									\
			pin: PCA9555(0),								\
			active: SIGNAL_HIGH,							\
			inactive: SIGNAL_LOW							\
		},													\
															\
		ipmb_ready_signal: {								\
			pin: PIN_INVALID								\
		},													\
															\
		mp_good_timeout: 300,								\
		pwr_good_timeout: 300,								\
		pwr_dcdc_efficiency: 85								\
	}
