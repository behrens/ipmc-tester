/* 
	AMC FRU IDs
*/ 
#define CFG_AMC1_FRU_ID (CFG_MODULE_FRU_ID_START+0)
#define CFG_AMC2_FRU_ID (CFG_MODULE_FRU_ID_START+1)
#define CFG_AMC3_FRU_ID (CFG_MODULE_FRU_ID_START+2)
#define CFG_AMC4_FRU_ID (CFG_MODULE_FRU_ID_START+3)
#define CFG_AMC5_FRU_ID (CFG_MODULE_FRU_ID_START+4)
#define CFG_AMC6_FRU_ID (CFG_MODULE_FRU_ID_START+5)
#define CFG_AMC7_FRU_ID (CFG_MODULE_FRU_ID_START+6)
#define CFG_AMC8_FRU_ID (CFG_MODULE_FRU_ID_START+7)

/* 
	AMC Site info
*/ 
#define CFG_AMC_SITE_COUNT		8 

/* 
	AMC Site: hardware configuration
*/ 
#define CFG_AMC_SITE_INFO						\
												\
	/* AMC Site 1 */							\
	{											\
		site_number: 1,							\
												\
		module_present_signal: {				\
			i2c_addr: 0x42,						\
			pin: PCA9555(7),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		module_enable_signal: {					\
			i2c_addr: 0x42,						\
			pin: PCA9555(6),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		mp_enable_signal: {						\
			i2c_addr: 0x42,						\
			pin: PCA9555(4),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		mp_good_signal: {						\
			i2c_addr: 0x42,						\
			pin: PCA9555(2),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_enable_signal: {					\
			i2c_addr: 0x42,						\
			pin: PCA9555(3),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_good_signal: {						\
			i2c_addr: 0x42,						\
			pin: PCA9555(1),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_enable_signal: {					\
			i2c_addr: 0x42,						\
			pin: PCA9555(5),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_ready_signal: {					\
			pin: PIN_INVALID					\
		},										\
												\
		mp_good_timeout: 300,					\
		pwr_good_timeout: 300,					\
		pwr_dcdc_efficiency: 85					\
	},											\
												\
	/* AMC Site 2 */							\
	{											\
		site_number: 2,							\
												\
		module_present_signal: {				\
			i2c_addr: 0x44,						\
			pin: PCA9555(8),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		module_enable_signal: {					\
			i2c_addr: 0x44,						\
			pin: PCA9555(9),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		mp_enable_signal: {						\
			i2c_addr: 0x44,						\
			pin: PCA9555(11),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		mp_good_signal: {						\
			i2c_addr: 0x44,						\
			pin: PCA9555(13),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_enable_signal: {					\
			i2c_addr: 0x44,						\
			pin: PCA9555(12),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_good_signal: {						\
			i2c_addr: 0x44,						\
			pin: PCA9555(14),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_enable_signal: {					\
			i2c_addr: 0x44,						\
			pin: PCA9555(10),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_ready_signal: {					\
			pin: PIN_INVALID					\
		},										\
												\
		mp_good_timeout: 300,					\
		pwr_good_timeout: 300,					\
		pwr_dcdc_efficiency: 85					\
	},											\
												\
	/* AMC Site 3 */							\
	{											\
		site_number: 3,							\
												\
		module_present_signal: {				\
			i2c_addr: 0x40,						\
			pin: PCA9555(13),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		module_enable_signal: {					\
			i2c_addr: 0x40,						\
			pin: PCA9555(12),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		mp_enable_signal: {						\
			i2c_addr: 0x40,						\
			pin: PCA9555(10),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		mp_good_signal: {						\
			i2c_addr: 0x40,						\
			pin: PCA9555(8),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_enable_signal: {					\
			i2c_addr: 0x40,						\
			pin: PCA9555(9),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_good_signal: {						\
			i2c_addr: 0x40,						\
			pin: PCA9555(7),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_enable_signal: {					\
			i2c_addr: 0x40,						\
			pin: PCA9555(11),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_ready_signal: {					\
			pin: PIN_INVALID					\
		},										\
												\
		mp_good_timeout: 300,					\
		pwr_good_timeout: 300,					\
		pwr_dcdc_efficiency: 85					\
	},											\
												\
	/* AMC Site 4 */							\
	{											\
		site_number: 4,							\
												\
		module_present_signal: {				\
			i2c_addr: 0x4a,						\
			pin: PCA9555(2),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		module_enable_signal: {					\
			i2c_addr: 0x4a,						\
			pin: PCA9555(3),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		mp_enable_signal: {						\
			i2c_addr: 0x4a,						\
			pin: PCA9555(5),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		mp_good_signal: {						\
			i2c_addr: 0x4a,						\
			pin: PCA9555(7),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_enable_signal: {					\
			i2c_addr: 0x4a,						\
			pin: PCA9555(6),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_good_signal: {						\
			i2c_addr: 0x4a,						\
			pin: PCA9555(8),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_enable_signal: {					\
			i2c_addr: 0x4a,						\
			pin: PCA9555(4),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_ready_signal: {					\
			pin: PIN_INVALID					\
		},										\
												\
		mp_good_timeout: 300,					\
		pwr_good_timeout: 300,					\
		pwr_dcdc_efficiency: 85					\
	},											\
												\
	/* AMC Site 5 */							\
	{											\
		site_number: 5,							\
												\
		module_present_signal: {				\
			i2c_addr: 0x40,						\
			pin: PCA9555(3),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		module_enable_signal: {					\
			i2c_addr: 0x40,						\
			pin: PCA9555(2),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		mp_enable_signal: {						\
			i2c_addr: 0x40,						\
			pin: PCA9555(0),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		mp_good_signal: {						\
			i2c_addr: 0x48,						\
			pin: PCA9555(14),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_enable_signal: {					\
			i2c_addr: 0x48,						\
			pin: PCA9555(15),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_good_signal: {						\
			i2c_addr: 0x48,						\
			pin: PCA9555(13),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_enable_signal: {					\
			i2c_addr: 0x40,						\
			pin: PCA9555(1),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_ready_signal: {					\
			pin: PIN_INVALID					\
		},										\
												\
		mp_good_timeout: 300,					\
		pwr_good_timeout: 300,					\
		pwr_dcdc_efficiency: 85					\
	},											\
												\
	/* AMC Site 6 */							\
	{											\
		site_number: 6,							\
												\
		module_present_signal: {				\
			i2c_addr: 0x4a,						\
			pin: PCA9555(12),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		module_enable_signal: {					\
			i2c_addr: 0x4a,						\
			pin: PCA9555(13),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		mp_enable_signal: {						\
			i2c_addr: 0x4a,						\
			pin: PCA9555(15),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		mp_good_signal: {						\
			i2c_addr: 0x46,						\
			pin: PCA9555(1),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_enable_signal: {					\
			i2c_addr: 0x46,						\
			pin: PCA9555(0),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_good_signal: {						\
			i2c_addr: 0x46,						\
			pin: PCA9555(2),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_enable_signal: {					\
			i2c_addr: 0x4a,						\
			pin: PCA9555(14),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_ready_signal: {					\
			pin: PIN_INVALID					\
		},										\
												\
		mp_good_timeout: 300,					\
		pwr_good_timeout: 300,					\
		pwr_dcdc_efficiency: 85					\
	},											\
												\
	/* AMC Site 7 */							\
	{											\
		site_number: 7,							\
												\
		module_present_signal: {				\
			i2c_addr: 0x48,						\
			pin: PCA9555(9),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		module_enable_signal: {					\
			i2c_addr: 0x48,						\
			pin: PCA9555(8),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		mp_enable_signal: {						\
			i2c_addr: 0x48,						\
			pin: PCA9555(6),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		mp_good_signal: {						\
			i2c_addr: 0x48,						\
			pin: PCA9555(4),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_enable_signal: {					\
			i2c_addr: 0x48,						\
			pin: PCA9555(5),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_good_signal: {						\
			i2c_addr: 0x48,						\
			pin: PCA9555(3),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_enable_signal: {					\
			i2c_addr: 0x48,						\
			pin: PCA9555(7),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_ready_signal: {					\
			pin: PIN_INVALID					\
		},										\
												\
		mp_good_timeout: 300,					\
		pwr_good_timeout: 300,					\
		pwr_dcdc_efficiency: 85					\
	},											\
												\
	/* AMC Site 8 */							\
	{											\
		site_number: 8,							\
												\
		module_present_signal: {				\
			i2c_addr: 0x46,						\
			pin: PCA9555(6),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		module_enable_signal: {					\
			i2c_addr: 0x46,						\
			pin: PCA9555(7),					\
			active: SIGNAL_LOW,					\
			inactive: SIGNAL_HIGH				\
		},										\
												\
		mp_enable_signal: {						\
			i2c_addr: 0x46,						\
			pin: PCA9555(9),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		mp_good_signal: {						\
			i2c_addr: 0x46,						\
			pin: PCA9555(11),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_enable_signal: {					\
			i2c_addr: 0x46,						\
			pin: PCA9555(10),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		pwr_good_signal: {						\
			i2c_addr: 0x46,						\
			pin: PCA9555(12),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_enable_signal: {					\
			i2c_addr: 0x46,						\
			pin: PCA9555(8),					\
			active: SIGNAL_HIGH,				\
			inactive: SIGNAL_LOW				\
		},										\
												\
		ipmb_ready_signal: {					\
			pin: PIN_INVALID					\
		},										\
												\
		mp_good_timeout: 300,					\
		pwr_good_timeout: 300,					\
		pwr_dcdc_efficiency: 85					\
	}
