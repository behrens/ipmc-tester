/*
	Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

	Description:
	This header file defines the interfaces
	provided by the sensor_QBDW033.c module.
    
    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_QBDW033_H__
#define __MASTER_SENSOR_QBDW033_H__

#include <sensor.h>

/* Read-only info structure of an QBDW033 temperature sensor */
typedef struct {
    sensor_ro_t s;

    unsigned char sns; /* Sensor channel number */
} sensor_qbdw033_ro_t;

/* QBDW033 temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_qbdw033_methods;

/* Auxiliary macro for defining QBDW033 sensor info */
#define SENSOR_QBDW033(s, a, alert) \
    { \
	SA(sensor_qbdw033_methods, s, alert), \
	sns: (a) \
    }

#endif /* __MASTER_SENSOR_QBDW033_H__ */
