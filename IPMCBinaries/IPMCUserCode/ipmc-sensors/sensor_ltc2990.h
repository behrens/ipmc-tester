/*
	Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

	Description:
	This header file defines the interfaces
	provided by the sensor_LTC2990.c module.
    
    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_LTC2990_H__
#define __MASTER_SENSOR_LTC2990_H__

#include <sensor.h>

/* Read-only info structure of an LTC2990 temperature sensor */
typedef struct {
    sensor_ro_t s;
    unsigned char bus_sns; /* Slave I2C channel number of PCA9545A switch and LTC channel number */
} sensor_ltc2990_ro_t;

/* LTC2990 temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_ltc2990_methods;

/* Auxiliary macro for defining LTC2990 sensor info */
#define SENSOR_LTC2990(s, a, alert) \
    { \
	SA(sensor_ltc2990_methods, s, alert), \
	bus_sns:  (a) \
    }

#endif /* __MASTER_SENSOR_LTC2990_H__ */
