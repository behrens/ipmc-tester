# CERN-IPMC Sensors

The goal of this folder consits in sharing the sensor development. 
Therefore, this readme file provide information about the drivers.

## MUCTPI Dev. (Stefan Haas):

I have added drivers for the following devices:

- LTC2990 (Linear Technology) <sup>1</sup>
- LTC2991 (Linear Technology) <sup>1</sup>
- TMP431 (Texas Instruments) <sup>1</sup>
- PIM400KZ (GE)
- QDBW033B (GE)

In general, the sensor driver code is optimized for the MUCTPI design, for example the sensor drivers contain the instructions 
to select the appropriate slave port in the I2C mux where required. In addition the I2C addresses of the devices are fixed in 
the sensor driver code, since there is no need to change them. This avoids having to pass many parameters from the config_sensors.h 
file. Most of the sensors also use some custom scaling from the native sensor reading to the 8 bit value returned by the driver 
(e.g. negative temperature readings are not possible). This is done to increase the resolution. Therefore some changes would 
probably be required to adapt the sensor drivers for other modules. I did test all the sensor drivers on the MUCTPI and the 
values reported make sense. Please have a look at the code and let me know how you want to proceed.

<sup>1</sup>: These devices are connected to the sensor I2C through a 4-port I2C mux (PCA9545A). The other two connect to the 
management I2C bus directly.
