/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This module implements the LTC2990 sensors.

    $Revision: 12269 $
*/

#define NEED_MASTERONLY_I2C

#include <defs.h>
#include <cfgint.h>

#ifdef CFG_SENSOR_LTC2990

#include <hal/i2c.h>
#include <hal/system.h>

#include <app.h>
#include <log.h>
#include <sensor.h>
#include <sensor_discrete.h>
#include <sensor_threshold.h>
#include <sensor_ltc2990.h>
#include <i2c_dev.h>
#include <sensor_pca9545.h>

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use LTC2990 sensors.
#endif

#undef DEBUG
//# define DEBUG

/* ------------------------------------------------------------------ */

// #define SNS_I2C_BUS      2
// #define I2CMUX_ADDR      0xE0 /* Address of the PCA9545A I2C multiplexer */
#define LTC2990_I2C_ADDR    MO_CHANNEL_ADDRESS(SENSOR_I2C_BUS, 0x4E << 1) /* I2C address of the LTC2990 voltage/temperature monitoring device */

/* LTC2990 Register definitions */
#define LTC2990_STATUS_REG		0x00
#define LTC2990_CONTROL_REG		0x01
#define LTC2990_TRIGGER_REG		0x02
#define LTC2990_TINT_REG	    0x04
#define LTC2990_ADC_V1_REG		0x06
#define LTC2990_ADC_V2_REG		0x08
#define LTC2990_ADC_V3_REG		0x0A
#define LTC2990_ADC_V4_REG		0x0C
#define LTC2990_VCC_REG		    0x0E

#define LTC2990_CONTROL_VALUE   0x1F // enable all channels as single-ended, repeated acquisition

/* ------------------------------------------------------------------ */

/* Forward declarations */
static char sensor_ltc2990_init(sensor_t *sensor);
static char sensor_ltc2990_fill_reading(sensor_t *sensor, unsigned char *msg);

/* ------------------------------------------------------------------ */

/* LTC2990 temperature sensor methods */
sensor_methods_t PROGMEM sensor_ltc2990_methods = {
    fill_event:		&sensor_threshold_fill_event,
    fill_reading:	&sensor_ltc2990_fill_reading,
    rearm:			&sensor_threshold_rearm,
    set_thresholds:	&sensor_threshold_set_thresholds,
    get_thresholds:	&sensor_threshold_get_thresholds,
    set_hysteresis:	&sensor_threshold_set_hysteresis,
    get_hysteresis:	&sensor_threshold_get_hysteresis,
    init:			&sensor_ltc2990_init
};

/* Read-only info structures of LTC2990 temperature sensors */
static const sensor_ltc2990_ro_t PROGMEM sensor_ltc2990_ro[] = { CFG_SENSOR_LTC2990 };

#define LTC2990_COUNT	sizeofarray(sensor_ltc2990_ro)

/* Read-write info structures of LTC2990 temperature sensors */
static struct sensor_ltc2990 {
    sensor_threshold_t	sensor;
} sensor_ltc2990[LTC2990_COUNT] WARM_BSS;
typedef struct sensor_ltc2990 sensor_ltc2990_t;

static unsigned short sensor_ltc2990_first;
DECL_SENSOR_GROUP(master, sensor_ltc2990_ro, sensor_ltc2990, &sensor_ltc2990_first);

/* Global flags for all LTC2990 sensors */
static unsigned char sensor_ltc2990_global_flags;
#define LTC2990_GLOBAL_INIT	    (1 << 0)	/* initialize all sensors */
#define LTC2990_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */

/*
    Internal functions for accessing LTC2990
*/

/* Write the PCA9545A I2C switch slave select register */
// char sensor_ltc2990_write_mux(unsigned char slave_select)
// {
	// unsigned short addr = MO_CHANNEL_ADDRESS(SNS_I2C_BUS, I2CMUX_ADDR);
	// unsigned char value = (slave_select > 0) ? (1 << ((slave_select-1) & 3)) : 0;
	
	// int ret = i2c_io(addr | I2C_START | I2C_STOP, &value, 1);
// #ifdef DEBUG
	// debug_printf("Write PCA9545A I2C mux at 0x%03x, data: 0x%02x, stats: %d\n", addr, value, ret);
// #endif
    // return (ret < I2C_OK) ? (-1) : 0;
// }

/* Write an LTC2990 register */
static char inline sensor_ltc2990_write_reg(unsigned char reg, unsigned char data)
{
	return i2c_dev_write_reg(LTC2990_I2C_ADDR, reg, &data, 1);
}

/* Read an LTC2990 temperature/adc register (temp, ADC) */
static char inline sensor_ltc2990_read_sensor(unsigned char reg, unsigned char *val)
{
    unsigned char data[2];

	*val = 0xFF; // unrealistic value to mark an error
    if (i2c_dev_read_reg(LTC2990_I2C_ADDR, reg, data, 2)) return -1;
	if (reg == LTC2990_TINT_REG) // internal temperature reading
		// assume Temperature is positive and D[12] is always 0, shift in 4 LSBs, resolution is then 0.5 degC
		*val = (data[0] << 5) | ((data[1] >> 3) & 0x1F);
	else // voltage reading
		// Sign bit and D[13] should always be 0, valid bit should be 1, the resolution is then 9.766 mV
		*val = (data[0] << 3) | ((data[1] >> 5) & 0x7);
#ifdef DEBUG
    debug_printf("LTC2990 @ 0x%03x, data = 0x%02x%02x, sensor = %d\n", LTC2990_I2C_ADDR, data[0], data [1], *val);
#endif
    return 0;
}

/* ------------------------------------------------------------------ */

/*
    The following function updates the reading
    of the given LTC2990 sensor.
*/
static void sensor_ltc2990_update_reading(unsigned char num, unsigned char flags)
{
    unsigned char reg, reading;
    /* get the I2C address of LTC2990, and channel number */
    unsigned char snum = sensor_ltc2990_first + num;
    // unsigned short addr = PRG_RD(sensor_ltc2990_ro[num].i2c_addr);
    unsigned char slv_bus = (PRG_RD(sensor_ltc2990_ro[num].bus_sns) >> 4) & 0x0F;
    unsigned char chan = PRG_RD(sensor_ltc2990_ro[num].bus_sns) & 0x0F;

    if (monly_i2c_is_ready(LTC2990_I2C_ADDR)) {
		/* select I2C switch slave channel */
		if (sensor_pca9545_write(1 << slv_bus) != I2C_OK) {
			debug_printf(PSTR("Failed to write to PCA9545A I2C switch\n"));
			return;
		}
		/* read temperature or adc */
		reg = LTC2990_TINT_REG + (2 * chan);
		if (!sensor_ltc2990_read_sensor(reg, &reading)) {
			/* update sensor reading */
			sensor_threshold_update(&master_sensor_set, snum, reading, flags);
		}
		/* disable I2C switch */
		//sensor_ltc2990_write_mux(0);
    }
}

/*
    The following function initializes the given
    LTC2990 sensor.
*/
static void sensor_ltc2990_initialize(unsigned char num)
{
    /* get I2C address of LTC2990 */
    // unsigned short addr = PRG_RD(sensor_ltc2990_ro[num].i2c_addr);
    // unsigned char slvb = PRG_RD(sensor_ltc2990_ro[num].slave_i2c);
	// unsigned char chan = PRG_RD(sensor_ltc2990_ro[num].channel);
    unsigned char slv_bus = (PRG_RD(sensor_ltc2990_ro[num].bus_sns) >> 4) & 3;
    unsigned char chan = PRG_RD(sensor_ltc2990_ro[num].bus_sns) & 7;

#ifdef DEBUG
    debug_printf("ltc2990 #%d, initialize\n", num);
#endif
    if (monly_i2c_is_ready(LTC2990_I2C_ADDR)) {
		if (chan == 0) { // only need to intitialize once per LTC2990 device, not every channel
			/* select I2C switch slave channel */
			if (sensor_pca9545_write(1 << slv_bus) != I2C_OK) {
				debug_printf(PSTR("Failed to write to PCA9545A I2C switch\n"));
				return;
			}
			/* configure LTC2990 control register */
			sensor_ltc2990_write_reg(LTC2990_CONTROL_REG, LTC2990_CONTROL_VALUE);
			/* trigger the acquisition */
			sensor_ltc2990_write_reg(LTC2990_TRIGGER_REG, 0xFF);
			/* disable I2C switch */
			//sensor_ltc2990_write_mux(0);
		}
		/* read the current sensor value */
		sensor_ltc2990_update_reading(num, SENSOR_INITIAL_UPDATE);
    }
}

/*
    Sensor initialization.
*/
static char sensor_ltc2990_init(sensor_t *sensor)
{
    unsigned char num = ((struct sensor_ltc2990 *) sensor) - sensor_ltc2990;
    sensor_ltc2990_initialize(num);
    return 0;
}

/* ------------------------------------------------------------------ */

#ifdef NEED_SLAVE_CALLBACKS
SLAVE_UP_CALLBACK(sensor_ltc2990_slave_up)
{
    /* schedule global LTC2990 init/update */
    sensor_ltc2990_global_flags = LTC2990_GLOBAL_INIT | LTC2990_GLOBAL_UPDATE;
}

SLAVE_DOWN_CALLBACK(sensor_ltc2990_slave_down)
{
    /* unschedule global LTC2990 init/update */
    sensor_ltc2990_global_flags = 0;
}
#endif

#ifdef NEED_CARRIER_CALLBACKS
CARRIER_UP_CALLBACK(sensor_ltc2990_carrier_up)
{
    /* schedule global LTC2990 init/update */
    sensor_ltc2990_global_flags = LTC2990_GLOBAL_INIT | LTC2990_GLOBAL_UPDATE;
}

CARRIER_DOWN_CALLBACK(sensor_ltc2990_carrier_down)
{
    /* unschedule global LTC2990 init/update */
    sensor_ltc2990_global_flags = 0;
}
#endif

/* ------------------------------------------------------------------ */

/*
    This section contains various callbacks
    registered by the LTC2990 driver.
*/

/* 1 second callback */
TIMER_CALLBACK(1s, sensor_ltc2990_1s_callback)
{
    unsigned char flags;

    /* schedule global LTC2990 update */
    save_flags_cli(flags);
    sensor_ltc2990_global_flags |= LTC2990_GLOBAL_UPDATE;
    restore_flags(flags);
}

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_ltc2990_poll)
{
    unsigned char i, flags, gflags;

    /* get/clear global LTC2990 flags */
    save_flags_cli(flags);
    gflags = sensor_ltc2990_global_flags;
    sensor_ltc2990_global_flags = 0;
    restore_flags(flags);

    if (gflags & LTC2990_GLOBAL_INIT) {
	/* make a delay to let the slave/carrier AVRs stabilize */
	udelay(20000);

        /* initialize all LTC2990 */
	for (i = 0; i < LTC2990_COUNT; i++) {
	    if (!(sensor_ltc2990[i].sensor.s.status & STATUS_NOT_PRESENT)) {
		sensor_ltc2990_initialize(i);
	    }
	}
    }

    if (gflags & LTC2990_GLOBAL_UPDATE) {
	/* update all sensor readings */
	for (i = 0; i < LTC2990_COUNT; i++) {
	    if (!(sensor_ltc2990[i].sensor.s.status & STATUS_NOT_PRESENT)) {
		sensor_ltc2990_update_reading(i, 0);
	    }
	}
    }
}

/* Initialization callback */
INIT_CALLBACK(sensor_ltc2990_init_all)
{
    /* schedule global initialization */
    sensor_ltc2990_global_flags = LTC2990_GLOBAL_INIT | LTC2990_GLOBAL_UPDATE;
}

/* ------------------------------------------------------------------ */

/*
    This section contains LTC2990 sensor methods.
*/

/* Fill the Get Sensor Reading reply */
static char sensor_ltc2990_fill_reading(sensor_t *sensor, unsigned char *msg)
{
    /* update current reading */
    sensor_ltc2990_update_reading((sensor_ltc2990_t *)sensor - sensor_ltc2990, 0);

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

#endif /* CFG_SENSOR_LTC2990 */
