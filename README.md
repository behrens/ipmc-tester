# CERN-IPMC Tester

This repository contains the files required to configure all of the programmable devices located on the CERN-IPMC Tester boad:

- uMGT Controller: <a href="https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-tester/tree/master/uMGTController">uMGTController folder</a>
- MMC Emulator: <a href="https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-tester/tree/master/MMCEmulator">MMCEmulator folder</a>
- CPLD: <a href="https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-tester/tree/master/IPMCTesterCPLD">IPMCTesterCPLD folder</a>

In addition, it contains the binaries files required to flash the CERN-IPMC mezzanine card with a specific software and the latest version of the FPGA firmware:

- IPMCBinaries files and documentation (how to flash the CERN-IPMC using FlashPRO): <a href="https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-tester/tree/master/IPMCBinaries">IPMCBinaries folder</a>

Finally, it contains the python script used to test the mezzanine card automatically:

- IPMCAutoTester tool: <a href="https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-tester/tree/master/IPMCAutoTester">IPMCAutoTester folder</a>
