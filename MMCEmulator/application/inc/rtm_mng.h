/*! \file rtm_mng.h \brief RTM management. */
//*****************************************************************************
//
// File Name	: 'rtm_mng.h'
// Title		: main management functions for RTM module
// Author		: Vahan Petrosyan
// Created		: 10/19/2010
// Target MCU	: Atmel AVR series
//
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//*****************************************************************************


#ifndef RTM_MNG_H
#define RTM_MNG_H

// pins on I/O Extender
#define RTM_EEPROM_WRITE_PR_PIN		8
#define RTM_BLUE_LED_PIN			2
#define RTM_RED_LED_PIN				3
#define RTM_GREEN_LED_PIN			4
#define RTM_HANDLE_PIN				1

// Function prototypes
unsigned char init_uRTM();
void manage_uRTM();
unsigned char get_rtm_state();
void calcul_fru_size();
unsigned short get_fru_size();
unsigned char rtm_set_payload_power(unsigned char cmd);
unsigned char rtm_check_compatibility();
unsigned char get_rtm_deactivation_flag();
void set_rtm_deactivation_flag();

#endif
