//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
//File Name	: project.h
// 
// Title		: project definitions
// Revision		: 1.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description : Pins configurations and main project definitions
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************
#ifndef PROJECT_H
#define PROJECT_H

	//Supported IPMI release, nibble swapped
	#define MMC_IPMI_REL		0x51	// V1.5

	//Additional device support
	#define IPMI_MSG_ADD_DEV_SUPP	0x29	// event receiver, accept sensor cmds

	#define AMC		0x00
	#define RTM		0x01

	#define DEACTIVATED		0
	#define ACTIVATED		1
	#define PRESENT			2
	#define COMPATIBLE		3
	#define NON_COMPATIBLE	4
	#define NOT_PRESENT		5
	#define ACTIVATION		6

	#define MP_PRESENT		0
	#define PP_PRESENT		1
	
#endif
