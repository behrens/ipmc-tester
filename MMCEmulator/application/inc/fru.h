//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: fru.h
// 
// Title		: FRU device header file
// Revision		: 1.1
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description : FRU information and payload control definitions
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************
#ifndef FRU_H
#define FRU_H

	// Constants
	#define MAX_FRU_SIZE              2048

	//enum M_Status{ M0, M1, M2, M3, M4, M5, M6, M7 };

	/** E-Keying */
		//Link type
		#define PCIE				0x02
		#define PCIE_ADV_SWITCHING	0x03
		#define ETHERNET			0x05
		#define SERIAL_RAPID_IO		0x06
		#define STORAGE				0x07
		#define OEM_TYPE			0xf0
		
		//Link type extension
		#define NO_EXT				0x00
	
		//PCIE ext
		#define GEN1_NO_SSC			0x00
		#define GEN1_SSC			0x01
		#define GEN2_NO_SSC			0x02
		#define GEN2_SSC			0x03
	
		//Ethernet ext
		#define BASE_1G_BX			0x00
		#define BASE_10G_BX4		0x01
	
		//Serial rapid io ext
		#define MBAUD_1250			0x00
		#define MBAUD_2500			0x01
		#define MBAUD_3125			0x02
		#define MBAUD_5000			0x03
		#define MBAUD_6250			0x04
	
	/** Clock configuration */
		//Clocks ID
		#define TCLKA				1
		#define TCLKB				2
		#define TCLKC				3
		#define TCLKD				4
		#define FCLKA				5
		
		//PLL
		#define PLL					0x02
		#define NO_PLL				0x00
		
		//Activation
		#define APP					0x01
		#define CIPMC				0x00
		
		//Direction
		#define RECEIVER			0x00
		#define SOURCE				0x01
		
		//Family
		#define UNSPEC_FAMILY		0x00
		#define SONET				0x01
		#define SDH					0x01
		#define PDH					0x01
		#define PCI_RESERVED		0x02
		
		//Conversion
		#define KHz(v)				v*1000
		#define MHz(v)				v*1000000
		#define GHz(v)				v*1000000000
		
	//Matching
	#define EXACT_MATCHES	0x00
	#define MATCHES_01		0x01
	#define MATCHES_10		0x02

	#define PORT(n) 		n

	//Function prototypes
	unsigned char ipmi_get_fru_inventory_area_info(unsigned char  fru, unsigned char * buf);
	unsigned char  ipmi_fru_data_read(unsigned char  fru, unsigned char * area, unsigned char  len, unsigned char * data);
	unsigned char  ipmi_fru_data_write(unsigned char * area,unsigned char * data, unsigned char  len);
	unsigned char  ipmi_picmg_port_state_get(unsigned char  channelID, unsigned char * buf);
	unsigned char ipmi_picmg_port_state_set(unsigned char channelID, unsigned char type, unsigned char ext, unsigned char groupID, unsigned char state);
	unsigned char  ipmi_picmg_set_clock_state(unsigned char  clockID, unsigned char  setting);
	unsigned char  ipmi_picmg_properties(unsigned char * buf);
	unsigned char  ipmi_set_fru_policy(unsigned char  mask_bits, unsigned char  set_bits);
	unsigned char  ipmi_get_fru_policy();
	unsigned char  ipmi_set_fru_activation(unsigned char  fru_activation_deactivation);
	void fru_init();
	
#endif //FRU_H