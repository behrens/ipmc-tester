//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: led.h
// 
// Title		: LED header file
// Revision		: 1.1
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description : LED control definitions
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************

//MJ to be done:
//- The files uses constants such as "PINB" without having a direct "#include" for the respective header


#ifndef LED_H
#define LED_H

//Constants
#define BLUE_LED              0
#define RED_LED               1
#define GREEN_LED             2

#define BLUE                  0x01
#define RED                   0x02
#define GREEN                 0x03
#define AMBER				  0x04
#define ORANGE				  0x05
#define WHITE				  0x06

#define LED_ON                0xff
#define LED_OFF               0
#define LED_BLINK             0xFA	//JM: Led blinking identifier and max time value
#define LED_LONG_BLINK        100	//JM: ON/OFF time in tens of milliseconds
#define LED_SHORT_BLINK       20	//JM: ON/OFF time in tens of milliseconds

#define LED_STATE_ON	      1
#define LED_STATE_OFF	      0

#define LOCAL_CONTROL_STATE   0x01
#define OVERRIDE_STATE        0x02
#define RESTORE_LOCAL_CONTROL 0xfc
#define LAMP_TEST_STATE		  0x06
#define EMPTY				  0x00
#define LAMP_TEST_CMD		  0xfb

#define AMC_GENERIC_LED_LIST								\
	{														\
		{	/* LEDi ID 0: BLUE LED*/						\
			io_type: MMC_PORT,								\
			pin:LOCAL_BLUE_LED,								\
			colour: BLUE,									\
			init: ACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		},													\
		{	/* LED ID 1: RED LED*/							\
			io_type: MMC_PORT,								\
			pin:LOCAL_RED_LED,								\
			colour: RED,									\
			init: ACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		},													\
		{	/* LED ID 2: GREEN LED*/						\
			io_type: MMC_PORT,								\
			pin:LOCAL_GREEN_LED,							\
			colour: GREEN,									\
			init: INACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		}													\
	}

#ifdef USE_RTM
	#define RTM_GENERIC_LED_LIST							\
	{														\
		{	/* LEDi ID 0: BLUE LED*/						\
			io_type: IO_EXTENDER_PCF8574AT,					\
			addr:RTM_IO_PORTS_ADDR,							\
			pin:RTM_BLUE_LED_PIN,							\
			colour: BLUE,									\
			init: INACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		},													\
		{	/* LED ID 1: RED LED*/							\
			io_type: IO_EXTENDER_PCF8574AT,					\
			addr:RTM_IO_PORTS_ADDR,							\
			pin:RTM_RED_LED_PIN,							\
			colour: RED,									\
			init: ACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		},													\
		{	/* LED ID 2: GREEN LED*/						\
			io_type: IO_EXTENDER_PCF8574AT,					\
			addr:RTM_IO_PORTS_ADDR,							\
			pin:RTM_GREEN_LED_PIN,							\
			colour: GREEN,									\
			init: INACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		}													\
	}
#endif

//Types
typedef struct led_control
{
  unsigned char local_cntr_fnc;
  unsigned char fnc_off;
  unsigned char on_duration;
  unsigned char colour;
  unsigned char control_state;
  unsigned char state;
  unsigned char lamp_test_duration;				//Used for Lamp Test command
  unsigned char lamp_test_return_control_state;	//Used for Lamp Test command
  unsigned short lamp_test_cnt;					//Used for Lamp Test command
  unsigned char local_on_duration;				//Used for local led blinking
  unsigned char cnt;								//Used for blinking
} leds;

typedef struct{
	unsigned char io_type;
	unsigned char addr;
	unsigned char port;
	unsigned char pin;
	unsigned char colour;
	unsigned char init;
	unsigned char active;
	unsigned char inactive;
}LED_t;

//Function prototypes
void leds_init();
void local_led_control(unsigned char fru, unsigned char led_n, unsigned char led_state);
void led_control(unsigned char fru, unsigned char led_n, unsigned char led_state);
unsigned char ipmi_get_fru_led_properties(unsigned char fru, unsigned char *buf);
unsigned char ipmi_get_led_color_capabilities(unsigned char fru, unsigned char LedId, unsigned char *buf);
unsigned char ipmi_set_fru_led_state(unsigned char fru, unsigned char LedId, unsigned char LedFn, unsigned char LedOn);
unsigned char ipmi_get_fru_led_state(unsigned char fru, unsigned char LedId, unsigned char *buf);
unsigned char state_led(unsigned char fru, unsigned char led_n);
void ledTimerCallback();
void reset_led(unsigned char fru, unsigned char led_n);
unsigned char get_num_of_led(unsigned char fru);

/* Pin control functions */
unsigned char get_mmcport_led_state(LED_t *led);
void active_mmcport_led(LED_t *led);
void desactive_mmcport_led(LED_t *led);
unsigned char get_PCF8574AT_port(unsigned char addr);
void set_PCF8574AT_port(unsigned char addr, unsigned char data);

#endif //LED_H
