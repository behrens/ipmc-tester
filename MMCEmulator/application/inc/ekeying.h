/*! \file ekeying.h \ E-keying management functions. */
//*****************************************************************************
//
// File Name	: 'ekeying.h'
// Title		: E-keying management functions
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************


#ifndef EKEYING_H_
#define EKEYING_H_

#define P2P_ENABLE	1
#define P2P_DISABLE 0
#define CLK_ENABLE	0x08
#define CLK_DISABLE 0x00

#define SUCCESS 0
#define FAILED  0xff
#define NI		0x01

#define PORT_ACTIVE		0x01
#define PORT_INACTIVE	0x00

typedef struct{
	unsigned char channelID;
	unsigned char port;
	unsigned char protocol;
	unsigned char groupID;
	unsigned char ext;
	unsigned char state;
}PortState_t;

unsigned char port_ekeying_enable(unsigned char port, unsigned char protocol, unsigned char extension);
unsigned char port_ekeying_disable(unsigned char port, unsigned char protocol, unsigned char extension);
unsigned char clock_ekeying_enable(unsigned char id);
unsigned char clock_ekeying_disable(unsigned char id);
void ekeying_init();

unsigned char set_channel_init(unsigned char port, unsigned char type, unsigned char ext, unsigned char state);

#endif /* EKEYING_H_ */