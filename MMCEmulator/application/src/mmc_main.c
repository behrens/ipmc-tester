//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
//File Name	: mmc_main.c
// 
// Title		: mmc main file
// Revision		: 1.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
//
// Modified by  : Frederic Bompard, CPPM (Centre Physique des Particules de Marseille)
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description : The main routine for MMC(Module Management Controller).
//
// This code is distributed under the GNU Public License
// which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************

//MJ: Files that are still to be reviewed:
// jtag.c & .h

// Include Files
#include <string.h>
#include "../../drivers/drivers.h"

#include "../../user/user_code/config.h"		//User code configuration header file

#include "../inc/ipmi_if.h"				//IPMI interface related functions
#include "../inc/hpm.h"					//HPM related functions
#include "../inc/led.h"					//Led management related functions
#include "../inc/rtm_mng.h"
#include "../inc/fru.h"					//FRU related functions
#include "../inc/hotswap.h"				//Hotswap monitoring functions
#include "../inc/payload.h"				//Payload monitoring functions
#include "../inc/sdr.h"					//Sensors related functions
#include "../inc/ekeying.h"
#include "../inc/project.h"

// Global variables
unsigned char rtm_sensor_enable = 0;
unsigned char rtm_hs_enable = 0;
unsigned char timer_event = 0;

//Initialization function prototype
unsigned char mmc_init();

unsigned char cnter = 0;
unsigned char wait_for_init_done = 0;


void mainTimerCallback(){
	timer_event = 1;
	if(cnter++ >= 50){
		wait_for_init_done = 1;	//Sending event too early makes I2C bus conflicts (5 second)
	}
}

//***********/
int main(void){
//***********/	
	unsigned char data[15];
	
	//watchdog_disable();					// Disable recovery mode (HPM)
	if(mmc_init())	return 0;
	//watchdog_enable();			// enable watchdog at 1 second	
		
	memset(data, 0x00, 15);
	
    while(1)
    {		
		//watchdog_reset();						// reset the watchdog
		
		//ipmb_send(0x20,15,data);
		
		//Check IPMI request(s)
        ipmi_check_request();
				
		//Management functions
		manage_payload();		  // Manage payload
		manage_hpm();			  // Manage HPM
		manage_hotswap();		  // Manage hot swap (Handle switch, power fault, ...)	
		
		//Green led monitoring
		if(payload_voltage_sensor() > 0x80)				local_led_control(AMC, GREEN_LED, LED_ON);	//Turn ON when 12V is present
		else											local_led_control(AMC, GREEN_LED, LED_OFF);	//Turn OFF when 12V is present
					
		#ifdef USE_RTM
			if(payload_voltage_sensor() > 0x80)		manage_uRTM();
		#endif
		
		//Polled tasks all 100 ms		
        if (timer_event){	/* if timer event and no cmd into buffer */
			timer_event = 0;
			ipmi_check_event_respond();
            if(wait_for_init_done) sensor_monitoring();      // check sensors	
		}
		
//		#ifdef USER_MAIN_CALLBACK
//			USER_MAIN_CALLBACK;
//		#endif
				
		//Watchdog and power consumption management
        //set_sleep_mode(SLEEP_MODE_IDLE);	// put the MCU to sleep
        //sleep_mode();
    }
	
    return 0;
}

#ifdef CUSTOM_ADDR_LIST
	unsigned char user_main(unsigned char addr);
#endif

unsigned char mmc_init(){
	unsigned char cardAddr;
	
	boot_uC();
	
	force_app_start();				// Used in the boot loader mode: Provide the state
	fru_init();						// Write FRU memory (if needed)
	leds_init();					// LEDs init
	
	sdr_init();						// SDR data init
	sensor_init();					// Sensors init
	//sensor_monitoring();			// Initialize sensors value
	
	#ifdef CUSTOM_ADDR_LIST
		cardAddr = get_address();
		
		#define ADDR(p_addr, p_gaddr1, p_gaddr2, p_gaddr3)		if(cardAddr == p_addr){ return user_main(cardAddr); }
		CUSTOM_ADDR_LIST
	#endif
	
	/*
	if ((inb(ACSR) & BV(ACO))){
		//Check if board is in the crate - JM: Turn on RED led and stop the program ?
	}
	sbi(ACSR, ACD);
	*/
	
	ekeying_init();
	ipmi_init();					//IPMI interface initialization
	hotswap_init();					//Hot swap management init
	
	local_led_control(AMC, RED_LED, LED_OFF);	//Turn off the red LED when initialization is finished	
	
	#ifdef USER_INIT_FUNC
		USER_INIT_FUNC();
	#endif
	
	timer_100ms_attach(mainTimerCallback);	
	
	return 0x00;
}