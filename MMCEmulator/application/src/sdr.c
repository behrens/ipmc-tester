//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: sdr_main.c
// 
// Title		: SDR records
// Revision		: 1.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Frederic Bompard (CPPM)
// Modified by  : Paschalis Vichoudis (CERN)
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description : Sensor Data Records, temperature and voltage management of the AMC module
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************
#include <string.h>

#include "../../drivers/drivers.h"

#include "../../user/user_code/sensors.h"
#include "../../user/user_code/config.h"
#include "../../user/user_code/sensor_drivers.h"

#include "../inc/ipmi_if.h"
#include "../inc/project.h"
#include "../inc/sensors.h"
#include "../inc/rtm_mng.h"
#include "../inc/sdr.h"
#include "../inc/toolfunctions.h"
#include "../inc/payload.h"

#include "../inc/12volts_sensor.h"
#include "../inc/hotswap_sensor.h"

#ifndef USER_SENSOR_CNT
	#define USER_SENSOR_CNT	0
#endif

signed char amc_deviceloc_sdr[sizeof(FRU_NAME)+15];

/** Generates the management controller device locator record according to the
	section 37.8 of the IPMI v.1.5 standard */
void device_locator_gen(unsigned char entity_inst, unsigned char entity_id, unsigned char owner_id){
	unsigned char i;
	
	amc_deviceloc_sdr[0] = 0x00;							//Record ID [LSB] (0x00)
	amc_deviceloc_sdr[1] = 0x00;							//Record ID [MSB] (0x00)
	amc_deviceloc_sdr[2] = 0x51;							//SDR Version (IPMI v1.5)
	amc_deviceloc_sdr[3] = 0x12;							//Management controller device locator record (SDR type 12h)
	amc_deviceloc_sdr[4] = 11 + strlen(FRU_NAME);			//Number of remaining record bytes (11 + strlen(FRU_NAME))
	amc_deviceloc_sdr[5] = owner_id;						//Owner ID (Device slave address)
	amc_deviceloc_sdr[6] = 0x07;							//Channel number (IPMB-L bus)
	amc_deviceloc_sdr[7] = 0x00;							//Power State notification
	amc_deviceloc_sdr[8] = 0x29;							//Device capabilities
	amc_deviceloc_sdr[9] = 0x00;							//Reserved
	amc_deviceloc_sdr[10]= 0x00;							//Reserved
	amc_deviceloc_sdr[11]= 0x00;							//Reserved
	amc_deviceloc_sdr[12]= entity_id;						//Entity ID (According to the ATCA standard, it must be 0xC1 for AMCs and 0xC0 for iRTM)
	amc_deviceloc_sdr[13]= entity_inst;						//Entity instance (According to the ATCA standard, it must be (0x60 | slot_position))
	amc_deviceloc_sdr[14]= 0x00;							//Reserved for OEM use
	amc_deviceloc_sdr[15]= 0xc0 | strlen(FRU_NAME);			//Device ID string type/Length
	
	for(i=0; i < strlen(FRU_NAME); i++){					//Device ID string
		amc_deviceloc_sdr[16+i] = FRU_NAME[i];
	}
}


sensor_info_t sensor_list_methods[USER_SENSOR_CNT+2];
sensor_t sensors[USER_SENSOR_CNT+2];

//**********************/
void sdr_init()   //Called from mmc_main.c
//**********************/
{    
    unsigned char ipmiId = get_ipmi_address();
	unsigned char sensor_nb;
	signed char buff[10];
	
	/** Define the entity_inst and entity_id values */
	unsigned char entity_inst = 0x60 | ((ipmiId - 0x70) >> 1);
	unsigned char entity_id = 0xC1;
	
	#ifdef iRTM_ADDR_LIST
		#define ADDR(p_addr, p_gaddr1, p_gaddr2, p_gaddr3)		if(get_ipmi_address() == p_addr){		entity_inst = 0x60;  entity_id = 0xC0; }
		iRTM_ADDR_LIST
		#undef ADDR
	#endif
	
	device_locator_gen(entity_inst, entity_id, ipmiId);
	
	/** Register hot swap sensor */
	sensor_list_methods[0].update_f = update_hotswap_value;
	sensor_list_methods[0].sdr_init = init_hotswap_sdr;
	sensor_list_methods[0].get_sdr = get_hotswap_sdr_byte;
	sensor_list_methods[0].update_thresholds = set_hotswap_threshold;
	sensor_list_methods[0].pulling_time = -1;
	sensor_list_methods[0].time_cnter = 0;
	
	
	/** Register 12 volts sensor */
	sensor_list_methods[1].update_f = update_volt12_value;
	sensor_list_methods[1].sdr_init = init_volt12_sdr;
	sensor_list_methods[1].get_sdr = get_volt12_sdr_byte;
	sensor_list_methods[1].update_thresholds = set_volt12_threshold;
	sensor_list_methods[1].pulling_time = 1;
	sensor_list_methods[1].time_cnter = 0;
		
	/** Initialize the sensor_list_method array */
	#define SENSOR_DECLARATION(update_func, sdr_init_func, get_sdr_func, update_thresholds_func, pull_time)	\
		if(get_sdr_func(buff, 15, sensor_nb, 0) != 0){														\
			sensor_list_methods[sensor_nb].update_f = update_func;											\
			sensor_list_methods[sensor_nb].sdr_init = sdr_init_func;										\
			sensor_list_methods[sensor_nb].get_sdr = get_sdr_func;											\
			sensor_list_methods[sensor_nb].update_thresholds = update_thresholds_func;						\
			sensor_list_methods[sensor_nb].pulling_time = pull_time;										\
			sensor_list_methods[sensor_nb].time_cnter = 0;													\
		}
		
	#define REGISTER_SENSOR
	
	for(sensor_nb=2; sensor_nb < USER_SENSOR_CNT+2; sensor_nb++){
		#include "../../user/user_code/sensor_drivers.h"		
	}
	
	for(sensor_nb=0; sensor_nb < USER_SENSOR_CNT+2; sensor_nb++){
		sensor_list_methods[sensor_nb].sdr_init(sensor_nb+1, sensor_nb, entity_inst, entity_id, ipmiId);
	}
}

//***************/
void sensor_init()   //Called from mmc_main.c
//***************/
{
    unsigned char sensor_nb;
    signed char sdr_p[21];
	
	for(sensor_nb = 0; sensor_nb < (USER_SENSOR_CNT+2); sensor_nb++){
		sensor_list_methods[sensor_nb].get_sdr(sdr_p, 21, sensor_nb, 0);
		
		if(sdr_p[3] == 0x01){
			sensors[sensor_nb].rec_type           = sdr_p[3];
			sensors[sensor_nb].type               = sdr_p[12];
			sensors[sensor_nb].event_type_code    = sdr_p[13];
			sensors[sensor_nb].value              = 0;
			sensors[sensor_nb].state              = 0;
			sensors[sensor_nb].old_state          = 0;
			sensors[sensor_nb].value_changed_flag = 0;
			sensors[sensor_nb].evnt_scan          = 0xC0;
			sensors[sensor_nb].signed_flag        = (sdr_p[20] & 0xC0) ? 0x01 : 0x00;
		}
	}
	
	sensor_init_user();
}

void enable_sensor(unsigned char sensorID){
	sensors[sensorID].evnt_scan = 0xC0;
}

void disable_sensor(unsigned char sensorID){
	sensors[sensorID].evnt_scan = 0x00;
}

void sensor_state_check(unsigned char sensID, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr){
			
	if(compare_val(sensors[sensID].value, lnc, UPPER_EQ, sensors[sensID].signed_flag) && compare_val(sensors[sensID].value, unc, LOWER_EQ, sensors[sensID].signed_flag))
		sensors[sensID].state = TEMP_STATE_NORMAL;
	else if(compare_val(sensors[sensID].value, unc, UPPER_EQ, sensors[sensID].signed_flag) && compare_val(sensors[sensID].value, uc, LOWER_EQ, sensors[sensID].signed_flag))
		sensors[sensID].state = TEMP_STATE_HIGH;
	else if(compare_val(sensors[sensID].value, uc, UPPER_EQ, sensors[sensID].signed_flag) && compare_val(sensors[sensID].value, unr, LOWER_EQ, sensors[sensID].signed_flag))
		sensors[sensID].state = TEMP_STATE_HIGH_CRIT;
	else if(compare_val(sensors[sensID].value, unr, UPPER_EQ, sensors[sensID].signed_flag))
		sensors[sensID].state = TEMP_STATE_HIGH_NON_REC;		
	else if(compare_val(sensors[sensID].value, lnc, LOWER_EQ, sensors[sensID].signed_flag) && compare_val(sensors[sensID].value, lc, UPPER_EQ, sensors[sensID].signed_flag))
		sensors[sensID].state = TEMP_STATE_LOW;
	else if(compare_val(sensors[sensID].value, lc, LOWER_EQ, sensors[sensID].signed_flag) && compare_val(sensors[sensID].value, lnr, UPPER_EQ, sensors[sensID].signed_flag))
		sensors[sensID].state = TEMP_STATE_LOW_CRIT;
	else if(compare_val(sensors[sensID].value, lnr, LOWER_EQ, sensors[sensID].signed_flag))
		sensors[sensID].state = TEMP_STATE_LOW_NON_REC;		
		
}

//***********************************/
void check_temp_event(unsigned char sensID, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr, unsigned char pos_hyst, unsigned char neg_hyst)
//***********************************/
{	
	
	if(sensors[sensID].rec_type == FULL_SENSOR && sensors[sensID].evnt_scan & 0xC0){
			
			if(sensors[sensID].event_management.sent_err){
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
			
			/** Compare threshold with the upper thresholds */
			
			/** Upper non-critical threshold going-high */
			if(!sensors[sensID].event_management.asserted.upper_non_critical_go_high && compare_val(sensors[sensID].value, unc, UPPER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = ASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_UNC_GH;
				sensors[sensID].event_management.asserted.upper_non_critical_go_high = 1;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
			
			if(sensors[sensID].event_management.asserted.upper_non_critical_go_high && compare_val(sensors[sensID].value, (unc - neg_hyst), LOWER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = DEASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_UNC_GH;
				sensors[sensID].event_management.asserted.upper_non_critical_go_high = 0;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;				
			}
			
			/** Upper critical threshold going-high */
			if(!sensors[sensID].event_management.asserted.upper_critical_go_high && compare_val(sensors[sensID].value, uc, UPPER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = ASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_UC_GH;
				sensors[sensID].event_management.asserted.upper_critical_go_high = 1;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
			
			if(sensors[sensID].event_management.asserted.upper_critical_go_high && compare_val(sensors[sensID].value, (uc - neg_hyst), LOWER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = DEASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_UC_GH;
				sensors[sensID].event_management.asserted.upper_critical_go_high = 0;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
		
			/** Upper non-recoverable threshold going-high */
			if(!sensors[sensID].event_management.asserted.upper_non_recoverable_go_high && compare_val(sensors[sensID].value, unr, UPPER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = ASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_UNR_GH;
				sensors[sensID].event_management.asserted.upper_non_recoverable_go_high = 1;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
		
			if(sensors[sensID].event_management.asserted.upper_non_recoverable_go_high && compare_val(sensors[sensID].value, (unr - neg_hyst), LOWER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = DEASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_UNR_GH;
				sensors[sensID].event_management.asserted.upper_non_recoverable_go_high = 0;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
			
			/** Lower non-critical threshold going-high */
			if(!sensors[sensID].event_management.asserted.lower_non_critical_go_low && compare_val(sensors[sensID].value, lnc, LOWER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = ASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_LNC_GL;
				sensors[sensID].event_management.asserted.lower_non_critical_go_low = 1;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
			
			if(sensors[sensID].event_management.asserted.lower_non_critical_go_low && compare_val(sensors[sensID].value, (lnc + pos_hyst), UPPER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = DEASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_LNC_GL;
				sensors[sensID].event_management.asserted.lower_non_critical_go_low = 0;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
			
			/** Lower critical threshold going-high */
			if(!sensors[sensID].event_management.asserted.lower_critical_go_low && compare_val(sensors[sensID].value, lc, LOWER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = ASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_LC_GL;
				sensors[sensID].event_management.asserted.lower_critical_go_low = 1;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
			
			if(sensors[sensID].event_management.asserted.lower_critical_go_low && compare_val(sensors[sensID].value, (lc + pos_hyst), UPPER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = DEASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_LC_GL;
				sensors[sensID].event_management.asserted.lower_critical_go_low = 0;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
		
			/** Lower non-recoverable threshold going-high */
			if(!sensors[sensID].event_management.asserted.lower_non_recorverable_go_high && compare_val(sensors[sensID].value, lnr, LOWER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = ASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_LNR_GL;
				sensors[sensID].event_management.asserted.lower_non_recorverable_go_high = 1;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
		
			if(sensors[sensID].event_management.asserted.lower_non_recorverable_go_high && compare_val(sensors[sensID].value, (lnr + pos_hyst), UPPER_EQ, sensors[sensID].signed_flag)){
				sensors[sensID].event_management.assert_deassert = DEASSERTION_EVENT;
				sensors[sensID].event_management.ev = IPMI_THRESHOLD_LNR_GL;
				sensors[sensID].event_management.asserted.lower_non_recorverable_go_high = 0;
				sensors[sensID].event_management.sent_err = ipmi_event_send(sensID, sensors[sensID].event_management.assert_deassert, &(sensors[sensID].event_management.ev), 1);
				return;
			}
	}
}


//****************************************/
unsigned char ipmi_sdr_info(unsigned char* buf, unsigned char sdr_sensor)  //Called from ipmi_if.c
//****************************************/
{
	unsigned char count = 0;
	
	count = (sdr_sensor) ? (USER_SENSOR_CNT+3) : (USER_SENSOR_CNT+2);
		
	buf[0] = count;		
    buf[1] = 0x01;														//static population / Lun 0 ob
	
    return 2;
}

//****************************************************/
unsigned char ipmi_sdr_get(unsigned char* id, unsigned char offs, unsigned char size, unsigned char* buf)  //Called from ipmi_if.c
//****************************************************/
{
    unsigned short recordID = id[0] | (id[1] << 8);	
	unsigned char len = 0;
	unsigned int sdr_len;
	
    if (recordID >= (USER_SENSOR_CNT+3) || (size+2) > MAX_BYTES_READ)
        return(0xfe);
	
    if ((recordID + 1) < (USER_SENSOR_CNT+3)) {
        buf[len++] = (recordID + 1) & 0xff; //next record ID
        buf[len++] = (recordID + 1) >> 8;   //next record ID
    }
    else {
        buf[len++] = 0xff;                  //last record ID
        buf[len++] = 0xff;                  //last record ID
    }

	if(recordID == 0x00){
		for(sdr_len = 0; sdr_len < size && (offs+sdr_len) < sizeof(amc_deviceloc_sdr); sdr_len++){
			buf[sdr_len+2] = amc_deviceloc_sdr[sdr_len+offs];
		}
	}else{
		sdr_len = sensor_list_methods[recordID-1].get_sdr((signed char *)(&(buf[2])), size, (recordID-1), offs);
	}
		
	//if(size != sdr_len)	return 0xff;
	len += size;
	
    return(len);
}

//*******************************************************/
unsigned char ipmi_get_sensor_threshold(unsigned char sensor_number, unsigned char* buf)  //Called from ipmi_if.c
//*******************************************************/
{
	//JM: Not supported yet !!!
	
   /*
   unsigned char max_sens;
   signed char *sdr_p = get_sdr_pointer(sensor_number);
   if (sdr_p == NULL)	return 0xff;
	
   max_sens = AMC_SENSOR_CNT + 1 + sensor_driver_cnt;
    
    #ifdef USE_RTM
		//if(get_rtm_state() == PRESENT)	//If RTM is present, only RTM HOT SWAP sensor should be enabled
		//	max_sens+= 2;
		//else if(get_rtm_state() == ACTIVATED)	//If RTM is activated, all RTM's sensor(s) should be enabled
			max_sens += (RTM_SENSOR_CNT + 1);
    #endif
	
    if (sensor_number > (max_sens - 1))
        return(0xff);

    //Only full sensors have limits. Therefore we have to check what sensor we are dealing with
	if(sensor_number < (AMC_SENSOR_CNT+sensor_driver_cnt)){
		if(sens[sensor_number].rec_type == 0x1)        //If it is a full sensor it has to have thresholds
		{
			buf[0] = 0x3f;    //MJ: We claim that all 6 thresholds are readable. This may not be true for all sensors. Therefore this hard coded value
							  //MJ: should be replaced by a sensor specific value. It is not yet clear how to implement this
							  //MJ: In any case the function should (partially) be moved into the user code section
			
			buf[1] = sdr_p[41];		//Lower non critical
			buf[2] = sdr_p[40];		//Lower critical
			buf[3] = sdr_p[39];		//Lower non recoverable
			buf[4] = sdr_p[38];		//Upper non critical
			buf[5] = sdr_p[37];     //Upper critical
			buf[6] = sdr_p[36];		//Upper non recoverable
		}
		else
		{
			buf[0] = 0x0;    //There are no thresholds
			buf[1] = 0x0;    //Dummy (Lower non critical)
			buf[2] = 0x0;    //Dummy (Lower critical)
			buf[3] = 0x0;    //Dummy (Lower non recoverable)
			buf[4] = 0x0;    //Dummy (Upper non critical)
			buf[5] = 0x0;    //Dummy (Upper critical)
			buf[6] = 0x0;    //Dummy (Upper non recoverable)
		}
	}
	*/
	return(7);
}

//*************************************************************************************************************/
unsigned char ipmi_set_sensor_threshold(unsigned char sensor_number, unsigned char mask, unsigned char lnc, unsigned char lcr, unsigned char lnr, unsigned char unc, unsigned char ucr, unsigned char unr)  //Called from ipmi_if.c
//*************************************************************************************************************/
{
	//JM: Not supported yet !!!
	
	/*
    unsigned char max_sens;
    signed char *sdr_p;
    unsigned char i;

    //Note: Bits in the bytes 19 & 20 in the full sensor SDR define which thresholds can be modified
    //This function does not evaluate these bits. I assume the MCH takes care of that

	max_sens = AMC_SENSOR_CNT + 1 + sensor_driver_cnt;

	#ifdef USE_RTM
		//if(get_rtm_state() == PRESENT)	//If RTM is present, only RTM HOT SWAP sensor should be enabled
		//	max_sens+= 2;
		//else if(get_rtm_state() == ACTIVATED)	//If RTM is activated, all RTM's sensor(s) should be enabled
			max_sens += (RTM_SENSOR_CNT + 1);
	#endif

    if (sensor_number > (max_sens - 1))
        return(0xff);
	
	if(sensor_number < (AMC_SENSOR_CNT+sensor_driver_cnt)){
		if(sens[sensor_number].rec_type == 0x1)                     //If it is a full sensor it has to have thresholds
		{
			if(sensor_number < AMC_SENSOR_CNT){
				sdr_p = get_sdr_pointer(sensor_number);
				if (sdr_p == NULL)	return 0xff;
				
				if (mask & 0x01) sdr_p[41] = lnc;   //Lower non critical		if (mask & 0x01)
				if (mask & 0x02) sdr_p[40] = lcr;   //Lower critical			if (mask & 0x02)
				if (mask & 0x04) sdr_p[39] = lnr;   //Lower non recoverable	if (mask & 0x04)
				if (mask & 0x08) sdr_p[38] = unc;   //Upper non critical		if (mask & 0x08)
				if (mask & 0x10) sdr_p[37] = ucr;   //Upper critical			if (mask & 0x10)
				if (mask & 0x20) sdr_p[36] = unr;   //Upper non recoverable	if (mask & 0x20)				
			}
			else{				
				for(i=0; i < driver_cnt; i++){
					if(sensor_list_methods[i].update_thresholds(sensor_number, mask, unr, ucr, unc, lnc, lcr, lnr))	return 0x00;
				}
				
				return 0xFF;
			}
			
			//if (mask & 0x01) sens[sensor_number].threshold.lower_non_critical = lnc;   //Lower non critical			if (mask & 0x01)
			//if (mask & 0x02) sens[sensor_number].threshold.lower_critical = lcr;   //Lower critical					if (mask & 0x02)
			//if (mask & 0x04) sens[sensor_number].threshold.lower_non_recoverable = lnr;   //Lower non recoverable	if (mask & 0x04)
			//if (mask & 0x08) sens[sensor_number].threshold.upper_non_critical = unc;   //Upper non critical			if (mask & 0x08)
			//if (mask & 0x10) sens[sensor_number].threshold.upper_critical = ucr;   //Upper critical					if (mask & 0x10)
			//if (mask & 0x20) sens[sensor_number].threshold.upper_non_recoverable = unr;   //Upper non recoverable	if (mask & 0x20) 
			
			return 0x00;
		}
	}
	*/
	
    return(0);
}


//*****************************************************/
unsigned char ipmi_get_sensor_reading(unsigned char sensor_number, unsigned char* buf)  //Called from ipmi_if.c
//*****************************************************/
{
    if (sensor_number > (USER_SENSOR_CNT+2))
        return(0xff);

	if(sensor_number == HOT_SWAP_SENSOR)		buf[0] = 0x00;								  //Raw value of the read sensor should be 0 in case of hot swap sensor
    else										buf[0] = sensors[sensor_number].value;        //raw value of sensor. 
	
	buf[1] = sensors[sensor_number].evnt_scan;	
	
	if(sensor_number == HOT_SWAP_SENSOR)		buf[2] = sensors[sensor_number].value;
    else										buf[2] = sensors[sensor_number].state;

    //Section 35.14 of IPMI 1.5 suggests that we should return "buf[3] = 0". When reading a sensor Natview shows
    //State = 0xrr00 with "rr" being a random value if "buf[3] = 0" is missing
    buf[3] = 0;

    return(4);
	
}


//****************************************/
unsigned char ipmi_picmg_get_device_locator(unsigned char fru, unsigned char* buf)  //Called from ipmi_if.c
//****************************************/
{
    unsigned char len = 0;

	if(fru == AMC){
		buf[len++] = amc_deviceloc_sdr[0]; //record ID LSB
		buf[len++] = amc_deviceloc_sdr[1]; //record ID MSB
	}
	else
		return 0xFF;
		
    return(len);
}

void set_sensor_value(unsigned char sensID, unsigned char val){
	sensors[sensID].value = val;
}

unsigned char get_sensor_value(unsigned char sensID){
	return sensors[sensID].value;
}

void set_sensor_value_changed(unsigned char sensID){
	sensors[sensID].value_changed_flag = 1;
}

unsigned char get_sensor_value_changed_flag(unsigned char sensID){
	unsigned char tmp = sensors[sensID].value_changed_flag;
	sensors[sensID].value_changed_flag = 0;
	return tmp;	
}

unsigned char get_sensor_type(unsigned char sensID){
	return sensors[sensID].type;
}

unsigned char get_event_type_code(unsigned char sensID){
	return sensors[sensID].event_type_code;
}

unsigned char driver_to_poll = 0;

void sensor_monitoring(){
	unsigned char i;	
	
	for(i=0; i < (USER_SENSOR_CNT+2); i++){
		if(sensor_list_methods[i].pulling_time != -1 && ++sensor_list_methods[i].time_cnter >= sensor_list_methods[i].pulling_time){
			sensor_list_methods[i].update_f();
			sensor_list_methods[i].time_cnter = 0;
		}
	}
	
	//if(driver_to_poll >= driver_cnt)	driver_to_poll = 0;
	//if(!sensor_list_methods[driver_to_poll].update_f())	driver_to_poll++;
	
	sensor_monitoring_user();
	/*
	for(i=0; i < (SENSOR_CNT); i++){
		if(sens[i].evnt_scan == 0xC0 && (get_sensor_type(i) == VOLTAGE || get_sensor_type(i) == TEMPERATURE)){
			if(i != VOLT_12 || get_fru_state() == ACTIVATED){
				sdr_p = get_sdr_pointer(i);
				if(sdr_p != NULL){
					sensor_state_check(i, sdr_p[36], sdr_p[37], sdr_p[38], sdr_p[41], sdr_p[40], sdr_p[39]);
					check_temp_event(i, sdr_p[36], sdr_p[37], sdr_p[38], sdr_p[41], sdr_p[40], sdr_p[39], sdr_p[42], sdr_p[43]);
				}		
			}
		}
	}
	*/
}