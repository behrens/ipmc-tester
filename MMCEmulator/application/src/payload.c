/*! \file payload.c \ Payload management */
//*****************************************************************************
//
// File Name	: 'hotswap.c'
// Title		: Payload management
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#include "../../drivers/drivers.h"

#include "../../user/user_code/config.h"

#include "../inc/hotswap.h"
#include "../inc/sdr.h"
#include "../inc/sensors.h"
#include "../inc/project.h"
#include "../inc/ipmi_if.h"
#include "../inc/led.h"
#include "../inc/payload.h"

unsigned char deactivation_state = 0;
unsigned char payload_state = DEACTIVATED;

void set_deactivation_flag(){
	deactivation_state = 1;
}

unsigned char get_deactivation_flag(){
	unsigned char tmp = deactivation_state;
	deactivation_state = 0;
	return tmp;
}

unsigned char ev_to_send = 0, ev_err = 0;

void manage_payload(){
	
	if(ev_err){
		ev_err = sendHotswapEvent(ev_to_send);
	}
	
	if(ipmi_is_init()){
		if(payload_state == DEACTIVATED && payload_voltage_sensor() > 0x82){		
			set_sensor_value(HOT_SWAP_SENSOR, get_sensor_value(HOT_SWAP_SENSOR) & 0xFB);
			payload_state = ACTIVATED;
			active_payload();
		}else if(payload_state == ACTIVATED && payload_voltage_sensor() <= 0x40){
			payload_state = DEACTIVATED;
		}
	
		#ifdef LOW_VOLTAGE_DETECTION_ENABLE
			if(payload_state == ACTIVATED && get_handle_switch_state() == HANDLE_SWITCH_CLOSED && get_signal(LOCAL_LOW_VOLTAGE_POK) == LOW){
				set_sensor_value(HOT_SWAP_SENSOR, get_sensor_value(HOT_SWAP_SENSOR) | 0x08);
				ev_to_send = HOTSWAP_POWER_FAULT;
				ev_err = sendHotswapEvent(HOTSWAP_POWER_FAULT);
			}
		#endif
	
		if(get_deactivation_flag()){
			desactive_payload();
			set_sensor_value(HOT_SWAP_SENSOR, get_sensor_value(HOT_SWAP_SENSOR) | 0x04);
			ev_to_send = HOTSWAP_QUIESCED;
			ev_err = sendHotswapEvent(HOTSWAP_QUIESCED);
		}
	}else{
		if(payload_state == DEACTIVATED &&  payload_voltage_sensor() > 0x82 && get_handle_switch_state() == HANDLE_SWITCH_CLOSED){
			payload_state = ACTIVATED;
			active_payload();
		}
		else if(payload_state == ACTIVATED && (payload_voltage_sensor() < 0x40 || get_handle_switch_state() == HANDLE_SWITCH_OPENED)){
			payload_state = DEACTIVATED;
			desactive_payload();
		}
	}
}

unsigned char get_fru_state(){
	return payload_state;
}

unsigned char active_payload(){
	
	int i = 0;	
	POWER_ON_SEQ
	
	
	set_sensor_value(HOT_SWAP_SENSOR, get_sensor_value(HOT_SWAP_SENSOR) & 0xE7);
	return 0;
}

unsigned char desactive_payload(){
	
	int i = 0;
	POWER_OFF_SEQ
		
	set_sensor_value(HOT_SWAP_SENSOR, get_sensor_value(HOT_SWAP_SENSOR) | 0x10);	
	return 0;
}


unsigned char ipmi_picmg_fru_control(unsigned char control_type){
	
	int i = 0;
	
	
	switch (control_type){
		case FRU_COLD_RESET:
			#ifdef COLD_RESET_SEQ
				COLD_RESET_SEQ
			#else
				return 0xFF;
			#endif
			break;
			
		case FRU_WARM_RESET:
			#ifdef WARM_RESET_SEQ
				WARM_RESET_SEQ
			#else
				return 0xFF;
			#endif
			break;
			
		case FRU_REBOOT:
			#ifdef REBOOT_SEQ
				REBOOT_SEQ
			#else
				return 0xFF;
			#endif
			break;
			
		default:
			return(0xff);
			break;
    }

    return(0);
}

unsigned char ipmi_picmg_get_fru_control_capabilities(){
	/*
	FRU Control Capabilities Mask.
		[4-7] - Reserved
		[3] - 1b - Capable of issuing a diagnostic interrupt
		[2] - 1b - Capable of issuing a graceful reboot
		[1] - 1b - Capable of issuing a warm reset
		[0] - Reserved
	*/
	unsigned char capa = 0;
	
	#ifdef REBOOT_SEQ
		capa |= 0x04;
	#endif
	
	#ifdef WARM_RESET_SEQ
		capa |= 0x02;
	#endif
	
	return capa;
}