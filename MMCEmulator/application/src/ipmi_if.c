//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: ipmi_if.c
// 
// Title		: IPMI interface
// Revision		: 1.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description : The IPMI interface initialization and functions
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************
#include <string.h>

#include "../../drivers/drivers.h"

#include "../../user/user_code/sensors.h"
#include "../../user/user_code/config.h"

#include "../inc/toolfunctions.h"
#include "../inc/project.h"
#include "../inc/payload.h"
#include "../inc/rtm_mng.h"
#include "../inc/sdr.h"
#include "../inc/fru.h"
#include "../inc/led.h"
#include "../inc/hpm.h"
#include "../inc/ipmi_if.h"

//Globals
//extern sensor_t sens[SDR_MAX_ID];
//extern enum M_Status RTM_M_Status;

ipmi_status status_bits;

LocalBuffer eventBuffer;
unsigned char eventBufferIndex = 0;
unsigned char eventRetry = 0;
unsigned char event_waitforresponse;
unsigned char event_cc;

LocalBuffer readBuffer;
ipmi_msg_t rqs;
unsigned char rsp_data[MAX_BYTES_READ+5], rsp_data_len, rqSAddr, rqSeq, rsSAddr, seq_no, retry;
static unsigned short reservationID;
unsigned char i = 0, event_receiver = 0xFF /*IPMC_ADDRESS*/, lun_event_receiver, ipmb_address;
char temp_net[80], temp_cmd[80];
unsigned char event_request_ok = 0;

unsigned char is_init = 0;

unsigned int event_timeout = 0;

//*************/
void ipmi_init()
//*************/
{
    ipmb_address = get_address();
	ipmb_i2c_configuration(ipmb_address, ipmb_get);
    event_receiver = 0xFF /*IPMC_ADDRESS*/;              // set default event receiver address
	event_waitforresponse = 0;
    seq_no = 1;
	lun_event_receiver = 0x00;
    retry = 0;
	is_init = 1;
}

unsigned char ipmi_is_init(){
	return is_init;
}

unsigned char get_ipmi_address(){
	if(ipmb_address == 0x00){
		return get_address();
	}
	
	return ipmb_address;
}

//**************************************/
void ipmi_set_event_receiver(unsigned char address, unsigned char lun)    //Called from ipmi_response_send in this file
//**************************************/
{
    event_receiver = address;
	lun_event_receiver = lun;
	
    if (event_receiver == 0xff)
        event_request_ok = 0;
	else
        event_request_ok = 1;
    //set_led_pattern(1, 0x10 + event_request_ok, 1);
}   


//***********************/
unsigned char get_event_request_ok()
//***********************/
{
  return(event_request_ok);
}

unsigned char nbEventSent = 0;


//**********************/
void ipmi_check_request()
//**********************/
{
    unsigned char i, j;


    if (status_bits.ipmb_rqs == 0)
	    return;
	
    //fill request
    //For a definition of the format of the request see paragraph 2.11.1 of "Intelligent Platform Management Bus Communications Protocol Specification"

    rsSAddr = readBuffer.Buffer[0];
    rqs.netfn = readBuffer.Buffer[1] >> 2;  //Seems we are not interested in the bottom 2 bits of this byte which contain the responder's LUN
    rqSAddr = readBuffer.Buffer[3];         //Address of the requester (i.e. MCH)
    rqSeq = readBuffer.Buffer[4] >> 2;
    rqs.lun = readBuffer.Buffer[4] & 0x03;
    rqs.cmd = readBuffer.Buffer[5];

	if (checsum_verify(readBuffer.Buffer, 2, readBuffer.Buffer[2]) != 0)        {                                     // first
		ipmb_send_rsp(0xC6);
		return; //ignore request
	}

	if ((checsum_verify(readBuffer.Buffer, readBuffer.Length - 1, readBuffer.Buffer[readBuffer.Length - 1])) != 0){   // second checksum
		ipmb_send_rsp(0xC6);
		return; //ignore request
	}

    if (rqs.netfn & 0x01)  //MJ: In this branch we handle responses to messages sent by the MMC (I guess this can only be event requests)
    {
        if (rqSAddr == event_receiver)  //Acknowledgement for an event request
        {
            //status_bits.event_pending = 0;
            status_bits.ipmb_rqs = 0;
            retry = 0;

			if(readBuffer.Buffer[6] == IPMI_CC_OK){
				eventRetry = 0;
				event_waitforresponse = 0;
			}else if(eventRetry < 3){
				ipmb_send_event();
				eventRetry++;
			}else{
				//JM: What happens if we lost the communication (Event never sent) -> reset MMC ?
				reset_uC();
			}
        }
        //MJ: In case of any other completion code (especially "IPMI_CC_NODE_BUSY") status_bits.event_pending and the message will be re-sent up to 3 times.
        return;
    }

    if (readBuffer.Length > 7)
    {
        rqs.data_len = readBuffer.Length - 7;
        for (i = 6, j = 0; i < readBuffer.Length - 1; i++, j++)
            rqs.data[j] = readBuffer.Buffer[i];
    }
    else
        rqs.data_len = 0;
		
	ipmi_response_send();
}

//**********************/
void ipmi_response_send()
//**********************/
{
	#if defined(ENABLE_CONTROLLER_SPECIFIC) || defined(ENABLE_OEM)
		unsigned char user_error_code;
	#endif
	unsigned char error = IPMI_CC_OK;
    rsp_data_len = 0;

    temp_net[i] = rqs.netfn; // snooping of queries
    temp_cmd[i] = rqs.cmd;
    i++;

    if (i >= 79)
        i = 0;

    switch (rqs.netfn)
    {
    case IPMI_APP_NETFN:
        if ((rqs.cmd == IPMI_GET_DEVICE_ID_CMD) || (rqs.cmd == IPMI_BROADCAST_GET_DEVICE_ID_CMD))
            rsp_data_len = ipmi_get_device_id();
        else
            error = IPMI_CC_INV_CMD;
        break;

    case IPMI_SENSOR_EVENT_NETFN:
        switch (rqs.cmd)
        {
        case IPMI_SET_EVENT_RECEIVER_CMD:
            ipmi_set_event_receiver(rqs.data[0], rqs.data[1]);   //MJ: unclear: According to IPMI 1.5 there should be a second byte with the LUN. Why don't we need it?
            break;

        case IPMI_GET_EVENT_RECEIVER_CMD:
			rsp_data[0] = event_receiver;
            rsp_data[1] = lun_event_receiver; //event_receiver;   //MJ: unclear: According to IPMI 1.5 there should be a second byte with the LUN. Why don't we need it? //JM: Modified to be compliance with the IPMI 1.5 standard
            rsp_data_len = 2;
            break;

        case IPMI_GET_DEVICE_SDR_INFO_CMD:
            if (rqs.data_len > 0)
                rsp_data_len = ipmi_sdr_info(rsp_data, rqs.data[0]);
            else
                rsp_data_len = ipmi_sdr_info(rsp_data, 0);
            break;

        case IPMI_GET_DEVICE_SDR_CMD:
            if (rqs.data[4] != 0)
            {
                /*if ((rqs.data[0] != (reservationID & 0xff)) || (rqs.data[1] != (reservationID >> 8)))
                    error = IPMI_CC_RES_CANCELED;
                else*/ if ((rsp_data_len = ipmi_sdr_get(&rqs.data[2], rqs.data[4], rqs.data[5], rsp_data)) == 0xff){
					rsp_data_len = 0;
                    error = IPMI_CC_PARAM_OUT_OF_RANGE;
				}
            }else{
				if ((rsp_data_len = ipmi_sdr_get(&rqs.data[2], rqs.data[4], rqs.data[5], rsp_data)) == 0xff){
					rsp_data_len = 0;
					error = IPMI_CC_PARAM_OUT_OF_RANGE;
				}
			}
		
			if(rsp_data_len == 0xfe){	rsp_data_len = 0x00;	error = IPMI_CC_CANT_RET_NUM_REQ_BYTES; }
			
            break;

        case IPMI_GET_SENSOR_THRESHOLD_CMD:             //For data format see IPMI1.5, Table 29-9
            if ((rsp_data_len = ipmi_get_sensor_threshold(rqs.data[0], rsp_data)) == 0xff)
                error = IPMI_CC_ILL_SENSOR_OR_RECORD;
            break;

        case IPMI_SET_SENSOR_THRESHOLD_CMD:             //For data format see IPMI1.5, Table 29-8
            if ((rsp_data_len = ipmi_set_sensor_threshold(rqs.data[0], rqs.data[1], rqs.data[2], rqs.data[3], rqs.data[4], rqs.data[5], rqs.data[6], rqs.data[7])) == 0xff)
                error = IPMI_CC_ILL_SENSOR_OR_RECORD;
            break;

        case IPMI_RESERVE_DEVICE_SDR_REPOSITORY_CMD:
            reservationID++;
            if (reservationID == 0)
                reservationID = 1;
            rsp_data[0] = reservationID & 0xff;
            rsp_data[1] = reservationID >> 8;
            rsp_data_len = 2;
            break;

        case IPMI_GET_SENSOR_READING_CMD:
            if ((rsp_data_len = ipmi_get_sensor_reading(rqs.data[0], rsp_data)) == 0xff)
                error = IPMI_CC_ILL_SENSOR_OR_RECORD;
            break;

        default:
            error = IPMI_CC_INV_CMD;
            break;
        }
        break;

    case IPMI_STORAGE_NETFN:
        switch (rqs.cmd)
        {
        case IPMI_GET_FRU_INVENTORY_AREA_INFO_CMD:
			rsp_data_len = ipmi_get_fru_inventory_area_info(rqs.data[0], rsp_data);			
			if(rsp_data_len == 0xFF)
				error = IPMI_CC_INV_CMD;
            break;

        case IPMI_READ_FRU_DATA_CMD:
			if ((rsp_data_len = ipmi_fru_data_read(rqs.data[0], &rqs.data[1], rqs.data[3], rsp_data)) == 0xff)
                    error = IPMI_CC_CANT_RET_NUM_REQ_BYTES;
            break;

        case IPMI_WRITE_FRU_DATA_CMD:
			#ifdef USE_RTM
				if(rqs.data[0] == 1)		error = IPMI_CC_INV_CMD;
				//rsp_data_len = rtm_fru_data_write(&rqs.data[1], &rqs.data[3], rqs.data_len - 3);
				else if(rqs.data[0] == 0)
			#else
				if(rqs.data[0] == 0)
			#endif
                rsp_data_len = ipmi_fru_data_write(&rqs.data[1], &rqs.data[3], rqs.data_len - 3);
			else
				error = IPMI_CC_INV_CMD;
			
            if(rsp_data_len == 0xff)
                error = IPMI_CC_PARAM_OUT_OF_RANGE;
            break;

        default:
            error = IPMI_CC_INV_CMD;
            break;
        }
        break;

    case IPMI_GROUP_EXTENSION_NETFN + IPMI_PICMG_GRP_EXT:
        switch (rqs.cmd)
        {
		case IPMI_PICMG_CMD_GET_ADDRESS_INFO:
			rsp_data[0] = (get_ipmi_address() >> 1);
			rsp_data[1] = get_ipmi_address();
			rsp_data[2] = 0xFF;
			rsp_data[3] = 0;
			rsp_data[4] = ((get_ipmi_address() - 0x70) >> 1);
			rsp_data[5] = 0x07;
			
			rsp_data_len = 6;
			
			break;
			
        case IPMI_PICMG_CMD_GET_PROPERTIES:
            //set_led_pattern(0, 0xff, 2);
            rsp_data_len = ipmi_picmg_properties(rsp_data);
            break;
			
		case IPMI_PICMG_CMD_FRU_CONTROL_CAPABILITIES:
			rsp_data_len = 1;
			rsp_data[0] = ipmi_picmg_get_fru_control_capabilities();
			break;
			
        case IPMI_PICMG_CMD_FRU_CONTROL:
			
            if (rqs.data[1] == RTM && rqs.data[2] == FRU_QUIESCE){
				#ifdef USE_RTM
				   set_rtm_deactivation_flag(); //MTCA.4, 3.5.2-6 OK
				#else
					error = IPMI_CC_INV_CMD;
				#endif
			}
            else
            {
                if(rqs.data[2] == FRU_QUIESCE)								set_deactivation_flag();
				else if ((ipmi_picmg_fru_control(rqs.data[2])) == 0xff)     error = IPMI_CC_INV_DATA_FIELD_IN_REQ;
												
						//#ifdef USE_RTM
						//	if (RTM_M_Status > M1){
						//		rtm_quiesce();              //MJ: Should this be done here or in the state machine in rtm_mng.c?
						//		rtm_set_power_level(0);
						//	}
						//#endif	
            }
            break;

        case IPMI_PICMG_CMD_GET_FRU_LED_PROPERTIES:
            rsp_data_len = ipmi_get_fru_led_properties(rqs.data[1], rsp_data);
            break;

        case IPMI_PICMG_CMD_GET_LED_COLOR_CAPABILITIES:
            if ((rsp_data_len = ipmi_get_led_color_capabilities(rqs.data[1], rqs.data[2], rsp_data)) == 0xff)
                error = IPMI_CC_INV_DATA_FIELD_IN_REQ;
            break;

        case IPMI_PICMG_CMD_SET_FRU_LED_STATE:  //Defined in Table 3-31 of PICMG 3.0 R3.0
			if ((rsp_data_len = ipmi_set_fru_led_state(rqs.data[1], rqs.data[2], rqs.data[3], rqs.data[4])) == 0xff)
					error = IPMI_CC_INV_DATA_FIELD_IN_REQ;
			
            break;
        case IPMI_PICMG_CMD_GET_FRU_LED_STATE:
            if ((rsp_data_len = ipmi_get_fru_led_state(rqs.data[1], rqs.data[2], rsp_data)) == 0xff)
                error = IPMI_CC_INV_DATA_FIELD_IN_REQ;
            break;

		case IPMI_PICMG_CMD_SET_FRU_ACTIVATION:
			if (ipmi_set_fru_activation(rqs.data[2]) == 0xff){
				error = IPMI_CC_INV_DATA_FIELD_IN_REQ;
			}
			break;
			
		case IPMI_PICMG_CMD_SET_FRU_ACTIVATION_POLICY:
			if (ipmi_set_fru_policy(rqs.data[2], rqs.data[3]) == 0xff){
				error = IPMI_CC_INV_DATA_FIELD_IN_REQ;
			}
			break;

		case IPMI_PICMG_CMD_GET_FRU_ACTIVATION_POLICY:
			rsp_data[0] = ipmi_get_fru_policy();
			rsp_data_len = 1;
			break;
			
        case IPMI_PICMG_CMD_GET_DEVICE_LOCATOR_RECORD:			
            if (rqs.data[2] != 0)
               error = IPMI_CC_PARAM_OUT_OF_RANGE;
            else{
                rsp_data_len = ipmi_picmg_get_device_locator(rqs.data[1], rsp_data);
				if(rsp_data_len == 0xFF)
					error = IPMI_CC_PARAM_OUT_OF_RANGE;
			}
            break;

        case IPMI_PICMG_CMD_SET_AMC_PORT_STATE:
			if(ipmi_picmg_port_state_set(rqs.data[1], ((rqs.data[2] & 0xF0) >> 4) | ((rqs.data[3] & 0x0F) << 4), ((rqs.data[3] & 0xF0) >> 4), rqs.data[4], rqs.data[5]))
				error = IPMI_CC_INV_CMD;
            rsp_data_len = 0;
            break;

        case IPMI_PICMG_CMD_GET_AMC_PORT_STATE:
            rsp_data_len = ipmi_picmg_port_state_get(rqs.data[1], rsp_data);
			if(!rsp_data_len)	error = IPMI_CC_INV_CMD;
            break;

        case IPMI_PICMG_CMD_SET_POWER_LEVEL:  //MTCA.4, 3.5.1-16 or 3.5.2-9 OK
			#ifdef USE_RTM
				if (rqs.data[1] == RTM){					//We are only interested in FRU1 (RTM) //MJ: Do we not have to care about FRU0 (AMC)?
					if(rtm_set_payload_power(rqs.data[2]))
						error = IPMI_CC_INV_DATA_FIELD_IN_REQ;	
				}else
					error = IPMI_CC_INV_CMD;
			#else
				error = IPMI_CC_INV_CMD;
			#endif
            break;

		case IPMI_PICMG_CMD_SET_CLOCK_STATE:
			rsp_data_len = ipmi_picmg_set_clock_state(rqs.data[1], rqs.data[3]);
			break;
			
        default:
			//JM: HPM commands should be implemented here
            error = hpm_response_send(rqs.cmd, rqs.data, rsp_data, &rsp_data_len);
			break;
        }
        break;

    case IPMI_OEM_GROUP_NETFN:
        // For information on how OEM commands work see:
        // table 5-1 of the IPMI 1.5 Spec
        // Example: http://docs.oracle.com/cd/E19536-01/820-0457-11/oemipmi.html

        //We pass all OEM commands directly to the user code. Therefore there is no switch{} in this section
		#ifdef ENABLE_OEM
			rsp_data_len = ipmi_oem_user(rqs.cmd, &rqs.data[0], &rqs.data[3], rqs.data_len - 3, rsp_data, &user_error_code); 
			error = user_error_code;
		#endif
        break;

    case IPMI_CONTROLLER_SPECIFIC:
		#ifdef ENABLE_CONTROLLER_SPECIFIC
			rsp_data_len = ipmi_controller_specific(rqs.cmd, &rqs.data[0], rqs.data_len, rsp_data, &user_error_code);
			error = user_error_code;
		#endif
        break;

    default:
        error = IPMI_CC_INV_CMD;
        break;
    }
			
    ipmb_send_rsp(error);
}


//****************************/
void ipmi_check_event_respond()    //Called from the main (every 100ms)
//****************************/
{

	if(!event_waitforresponse)	return;
	
	if(event_waitforresponse)
		event_timeout++;
    
	if(event_timeout > 10){	//Timeout = 1 s
		
		if(eventRetry >= 3){
			//Timeout - What are we doing ? Reset of the MMC ?
			#ifdef ENABLE_RESET_ON_EVENT_ERR 
				reset_uC();
			#else
				eventRetry = 0;
			#endif
		}
		else{
			ipmb_send_event();
			eventRetry++;
			event_timeout = 0;
		}
	}
}

//*********************/
unsigned char ipmi_get_device_id()  //Called from ipmi_response_send in this file
//*********************/
{
    unsigned char len = 0;

    rsp_data[len++] = 0x00;                            // Device ID
    rsp_data[len++] = 0x80;                            // Device revision and SDRs
    rsp_data[len++] = MMC_FW_REL_MAJ;                  // Major Firmware revision
    rsp_data[len++] = MMC_FW_REL_MIN;                  // Minor Firmware revision
    rsp_data[len++] = MMC_IPMI_REL;                    // IPMI version 1.5
    rsp_data[len++] = IPMI_MSG_ADD_DEV_SUPP;           // Additional device support(commands and functions)
    rsp_data[len++] = IPMI_MSG_MANU_ID_LSB;            // Manufacturer ID LSB
    rsp_data[len++] = IPMI_MSG_MANU_ID_B2;
    rsp_data[len++] = IPMI_MSG_MANU_ID_MSB;
    rsp_data[len++] = IPMI_MSG_PROD_ID_LSB;            // Product ID LSB
    rsp_data[len++] = IPMI_MSG_PROD_ID_MSB;

    return len;
}

//get message from ipmb 
//***************************************************/
void ipmb_get(unsigned char receiveDataLength, unsigned char* receiveData)  //Installed as asynchronous handler in ipmi_init in this file
//***************************************************/
{
    unsigned char i;

    status_bits.connected = 1;

    if (status_bits.ipmb_rqs == 1)
        return;

    for (i = 0; i < receiveDataLength; i++)   // copy the received data to a local buffer
        readBuffer.Buffer[i] = *receiveData++;

    readBuffer.Length = receiveDataLength;
    status_bits.ipmb_rqs = 1;
}



//master write message
//*****************************/
void ipmb_send_rsp(unsigned char cc_error)
//*****************************/
{
    LocalBuffer writeBuffer;
    unsigned char i = 0, j;

    writeBuffer.Buffer[i++] = rqSAddr;
    writeBuffer.Buffer[i++] = ((rqs.netfn + 1) << 2) | (rqs.lun & 0x03);
    writeBuffer.Buffer[i++] = checksum_clc(writeBuffer.Buffer, 2);
    writeBuffer.Buffer[i++] = rsSAddr;
    writeBuffer.Buffer[i++] = (rqSeq << 2);
    writeBuffer.Buffer[i++] = rqs.cmd;
    writeBuffer.Buffer[i++] = cc_error;

    if (rqs.netfn == IPMI_GROUP_EXTENSION_NETFN)
        writeBuffer.Buffer[i++] = IPMI_PICMG_GRP_EXT;

    if (!cc_error && (rsp_data_len > 0))
    {
        for (j = 0; j < rsp_data_len; j++)
            writeBuffer.Buffer[i++] = rsp_data[j];
    }
		
    writeBuffer.Buffer[i] = checksum_clc(writeBuffer.Buffer, i);
    ipmb_send(rqSAddr, i, &writeBuffer.Buffer[1]);
    status_bits.ipmb_rqs = 0;
}


//send event message
//************************************************/
unsigned char ipmi_event_send(unsigned char sensor_number, unsigned char assert_deassert, unsigned char *evData, unsigned char length)   //Called from multiple places
//************************************************/
{
    //if (status_bits.event_pending == 1 || event_receiver == 0xFF)    //If the previous message has not yet gone out or if event request are disabled we refuse to accept another one. MJ: What are the consequences?
    //    return 1
	if(event_receiver == 0xFF)	return 1;		//Event receiver address not ready
	//if(eventBufferIndex == 0)	return 1;		//No event to sent
	if(event_waitforresponse)	return 1;		//No response received
	if(status_bits.ipmb_rqs)	return 1;
	
	//if(eventBufferIndex >= EVENT_BUFFER_SIZE)
	//	return 1;
		
    //IPMI message header
    //eventBuffer[eventBufferIndex].Buffer[0] = event_receiver;
    //eventBuffer[eventBufferIndex].Buffer[1] = (IPMI_SENSOR_EVENT_NETFN << 2) | (lun_event_receiver & 0x03);       // send netfn
    //eventBuffer[eventBufferIndex].Buffer[2] = checksum_clc(eventBuffer[eventBufferIndex].Buffer, 2);
    //eventBuffer[eventBufferIndex].Buffer[3] = ipmb_address;
    eventBuffer.Buffer[4] = (++seq_no << 2);
    eventBuffer.Buffer[5] = IPMI_PLATFORM_EVENT_CMD;
	
    //IPMI event request message. See table 23-5 of IPMI 1.5
    eventBuffer.Buffer[6] = 0x04;                               // Event Message revision
    eventBuffer.Buffer[7] = get_sensor_type(sensor_number);
    eventBuffer.Buffer[8] = sensor_number;
    eventBuffer.Buffer[9] = assert_deassert | (get_event_type_code(sensor_number) & 0x7F);
    eventBuffer.Buffer[10] = (length >= 1)? evData[0] : 0xFF;
    eventBuffer.Buffer[11] = (length >= 2)? evData[1] : 0xFF;
    eventBuffer.Buffer[12] = (length >= 3)? evData[2] : 0xFF;
	//eventBuffer[eventBufferIndex].Buffer[13] = checksum_clc(eventBuffer[eventBufferIndex].Buffer, 13);
	
	//eventBufferIndex++;
	
	ipmb_send_event();
	
	eventRetry = 0;
	event_timeout = 0;
	
	return 0;
}


//*******************/
void ipmb_send_event(){	
	event_waitforresponse = 1;
	
	eventBuffer.Buffer[0] = event_receiver;
	eventBuffer.Buffer[1] = (IPMI_SENSOR_EVENT_NETFN << 2) | (lun_event_receiver & 0x03); 
	eventBuffer.Buffer[3] = ipmb_address;
	
	eventBuffer.Buffer[2] = checksum_clc(eventBuffer.Buffer, 2);
	eventBuffer.Buffer[13] = checksum_clc(eventBuffer.Buffer, 13);
	
	ipmb_send(event_receiver, 13, &eventBuffer.Buffer[1]);
}

unsigned char get_ipmb_buffer_state(){
	return status_bits.ipmb_rqs;
}

 unsigned char geog_addr = 0;
 
//get IPMB_L address 	
//**************
unsigned char get_address()
//**************
{
    unsigned char GAddr1 = 0, GAddr2 = 0, GAddr3 = 0;
	
    // configure GAx as high impedance input
    clear_signal(GA0);
    clear_signal(GA1);
    clear_signal(GA2);

    set_signal_dir(GA_PULLUP, 0x00);			// configure GA pull-up as low output
    clear_signal(GA_PULLUP);		// set pin to low
	
    //GA_PULLUP_PPORT &= ~GA_PULLUP_PIN;		// set Pull up to low
	
    delay_us(100);									// give Pin a chance to change

    GAddr1 = get_signal(GA0);
    GAddr2 = get_signal(GA1);
    GAddr3 = get_signal(GA2);

    set_signal(GA_PULLUP);     // set pin to high
    delay_us(100);

    //read lines and compare
    if (GAddr1 != get_signal(GA0))
        GAddr1 = UNCONNECTED;
    if (GAddr2 != get_signal(GA1))
        GAddr2 = UNCONNECTED;
    if (GAddr3 != get_signal(GA2))
        GAddr3 = UNCONNECTED;

	//** Check standard ADDR **
	if(GAddr1 == GROUNDED && GAddr2 == GROUNDED && GAddr3 == UNCONNECTED)			geog_addr = 0x72;
	else if(GAddr1 == GROUNDED && GAddr2 == UNCONNECTED && GAddr3 == GROUNDED)		geog_addr = 0x74;
	else if(GAddr1 == GROUNDED && GAddr2 == UNCONNECTED && GAddr3 == UNCONNECTED)	geog_addr = 0x76;
	else if(GAddr1 == UNCONNECTED && GAddr2 == GROUNDED && GAddr3 == GROUNDED)		geog_addr = 0x78;
	else if(GAddr1 == UNCONNECTED && GAddr2 == GROUNDED && GAddr3 == UNCONNECTED)	geog_addr = 0x7A;
	else if(GAddr1 == UNCONNECTED && GAddr2 == UNCONNECTED && GAddr3 == GROUNDED)	geog_addr = 0x7C;
	else if(GAddr1 == UNCONNECTED && GAddr2 == UNCONNECTED && GAddr3 == POWERED)	geog_addr = 0x7E;
	else if(GAddr1 == UNCONNECTED && GAddr2 == POWERED && GAddr3 == UNCONNECTED)	geog_addr = 0x80;
	else if(GAddr1 == UNCONNECTED && GAddr2 == POWERED && GAddr3 == POWERED)		geog_addr = 0x82;
	else if(GAddr1 == POWERED && GAddr2 == UNCONNECTED && GAddr3 == UNCONNECTED)	geog_addr = 0x84;
	else if(GAddr1 == POWERED && GAddr2 == UNCONNECTED && GAddr3 == POWERED)		geog_addr = 0x86;
	else if(GAddr1 == POWERED && GAddr2 == POWERED && GAddr3 == UNCONNECTED)		geog_addr = 0x88;
	else if(GAddr1 == POWERED && GAddr2 == POWERED && GAddr3 == UNCONNECTED)		geog_addr = 0x88;

	//** Check custom ADDR **
	#ifdef CUSTOM_ADDR_LIST
		#define ADDR(p_addr, p_gaddr1, p_gaddr2, p_gaddr3)		else if(GAddr1 == p_gaddr1 && GAddr2 == p_gaddr2 && GAddr3 == p_gaddr3)		geog_addr = p_addr;
		CUSTOM_ADDR_LIST
		#undef ADDR
	#endif
	
	#ifdef iRTM_ADDR_LIST
		#define ADDR(p_addr, p_gaddr1, p_gaddr2, p_gaddr3)		else if(GAddr1 == p_gaddr1 && GAddr2 == p_gaddr2 && GAddr3 == p_gaddr3)		geog_addr = p_addr;
		iRTM_ADDR_LIST
		#undef ADDR
	#endif
	
    return(geog_addr ? geog_addr : 0x70);
}