/*! \file pca9698.h \ Driver for pca9698 IO extender -Header file */
//*****************************************************************************
//
// File Name	: 'pca9698.h'
// Title		: Driver for pca9698 IO extender - Header file
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 28/01/2016
// Target MCU	: Atmel AVR series
//
//*****************************************************************************


#ifndef PCA9698_H_
#define PCA9698_H_

unsigned char get_PCA9698_port(unsigned char addr, unsigned char port);
void set_PCA9698_port(unsigned char addr, unsigned char port, unsigned char data);
void set_PCA9698_bit(unsigned char addr, unsigned char port, unsigned char pin);
void clear_PCA9698_bit(unsigned char addr, unsigned char port, unsigned char pin);
unsigned char get_PCA9698_bit(unsigned char addr, unsigned char port, unsigned char pin);

void set_dir_pin(unsigned char addr, unsigned char port, unsigned char pin, unsigned char dir);
unsigned char get_dir_port(unsigned char addr, unsigned char port);
void set_dir_port(unsigned char addr, unsigned char port, unsigned char dir);

#endif /* PCF8574_H_ */