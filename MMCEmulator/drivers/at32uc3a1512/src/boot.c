/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include "../inc/i2c.h"
#include "../inc/io.h"
#include "../inc/adc.h"
#include "../inc/ext_i2c.h"
#include "../inc/timer.h"

void boot_uC (void)
{
	/* Insert system clock initialization code here (sysclk_init()). */
	irq_initialize_vectors();
	
	sysclk_init();
	//osc_init();
		
	sleepmgr_init();
	init_port();
	a2dInit();
	i2cInit();
	i2cswInit();
	timerInit();
	
	stdio_usb_init();
	stdio_usb_enable();
	
	//init_sd();
	
	cpu_irq_enable();
	//a2dInit();
}

void reset_uC(){
	reset_do_soft_reset();
}

void vbus_event(unsigned char b_vbus_high){
	if(b_vbus_high)	udc_attach();
	else			udc_detach();
}
