/*
 * adc.c
 *
 * Created: 16/10/2015 09:17:06
 *  Author: jumendez
 */ 
#include <asf.h>

#include "../../drivers.h"

// initialize a2d converter
//***************/
void a2dInit(void){
	adc_configure ( &AVR32_ADC );
}

// Perform a 8-bit conversion.
unsigned char payload_voltage_sensor(){
	unsigned char raw;
	
	clear_signal(MX0);
	clear_signal(MX1);
	clear_signal(MX2);
	clear_signal(MX3);
	
	adc_enable ( &AVR32_ADC, PWR_GPIO_12V_CHAN);
	adc_start ( &AVR32_ADC );
	long xRawValue = adc_get_value ( &AVR32_ADC, PWR_GPIO_12V_CHAN);	
	adc_disable( &AVR32_ADC, PWR_GPIO_12V_CHAN);
	
	raw = xRawValue*(255.0f/1024.0f);
	
	return (unsigned char)(raw*(0.8593194514982224));	//SDR(255) = 19.69V - HW(255) = 16.69V (Vref = 3V)
}