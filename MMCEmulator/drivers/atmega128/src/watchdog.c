/*
 * watchdog.c
 *
 * Created: 14/10/2015 18:09:36
 *  Author: jumendez
 */ 

#include <avr/wdt.h>

void watchdog_enable(){
	wdt_enable(WDTO_2S);
}

void watchdog_disable(){
	wdt_disable();
}

void watchdog_reset(){
	wdt_reset();
}