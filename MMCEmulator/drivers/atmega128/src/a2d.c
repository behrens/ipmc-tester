//*****************************************************************************
//
// File Name	: 'a2d.c'
// Title		: Analog-to-digital converter functions
// Author		: Pascal Stang - Copyright (C) 2002
// Created		: 2002-04-08
// Revised		: 2002-09-30
// Version		: 1.1
// Target MCU	: Atmel AVR series
// Editor Tabs	: 4
//
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************

#include <avr/io.h>

#include "../inc/avrlibdefs.h"

#include "../inc/a2d.h"
#include "../inc/iodeclaration.h"

// initialize a2d converter
//***************/
void a2dInit(void)    //Called from sdr.c
//***************/
{
	sbi(ADCSR, ADEN);				        // enable ADC (turn on ADC power)
	//sbi(ADCSR, ADFR);				        // free running mode
	a2dSetPrescaler(ADC_PRESCALE);	        // set default prescaler
	a2dSetReference(ADC_REFERENCE_AREF);	// set default reference
	sbi(ADMUX, ADLAR);				        // set to left-adjusted result
}


// turn off a2d converter
void a2dOff(void){
	cbi(ADCSR, ADEN);	// disable ADC (turn off ADC power)
}


// configure A2D converter clock division (prescaling)
void a2dSetPrescaler(unsigned char prescale){
	outb(ADCSR, ((inb(ADCSR) & ~ADC_PRESCALE_MASK) | prescale));
}


// configure A2D converter voltage reference
void a2dSetReference(unsigned char ref){
	outb(ADMUX, ((inb(ADMUX) & ~ADC_REFERENCE_MASK) | (ref<<6)));
}


void a2dSetChannel(unsigned char ch){
	outb(ADMUX, (inb(ADMUX) & ~ADC_MUX_MASK) | (ch & ADC_MUX_MASK));	// set channel
}


// start a conversion on the current a2d input channel
void a2dStartConvert(void){
	sbi(ADCSR, ADIF);	// clear hardware "conversion complete" flag 
	sbi(ADCSR, ADSC);	// start conversion
}


// return TRUE if conversion is complete
unsigned char a2dIsComplete(void){
	return bit_is_set(ADCSR, ADSC); 
}


// Perform a 10-bit conversion
// starts conversion, waits until conversion is done, and returns result
unsigned short a2dConvert10bit(unsigned char ch){
	outb(ADMUX, (inb(ADMUX) & ~ADC_MUX_MASK) | (ch & ADC_MUX_MASK));	// set channel
	sbi(ADCSR, ADIF);						                            // clear hardware "conversion complete" flag
	sbi(ADCSR, ADSC);						                            // start conversion
	while(bit_is_set(ADCSR, ADSC)); 		                            // wait until conversion complete

	// CAUTION: MUST READ ADCL BEFORE ADCH!!!
	return(inb(ADCL) | (inb(ADCH) << 8));	                            // read ADC (full 10 bits);
}


// Perform a 8-bit conversion.
// starts conversion, waits until conversion is done, and returns result
unsigned char get_8bit_adc(unsigned char ch){
	
	a2dSetChannel(ch);
	a2dStartConvert();
	
	while(!(inb(ADCSR) & BV(ADIF)));
		
    return (inb(ADCH));	// read ADC
}


unsigned char payload_voltage_sensor(){
	return get_8bit_adc(0);
}
