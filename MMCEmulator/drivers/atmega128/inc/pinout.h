/*
 * pinout.h
 *
 * Created: 14/10/2015 16:18:17
 *  Author: jumendez
 */ 


#ifndef PINOUT_H_
#define PINOUT_H_
	
	//IO state
	#define HIGH                                                    1
	#define LOW                                                     0
	#define ACTIVE                                                  0xFF
	#define INACTIVE                                                0x00
	
	//IO Direction
	#define INPUT                                                   0x00
	#define OUTPUT                                                  0x01
	
	//IO Type
	#define MMC_PORT                                                0
	#define IO_EXTENDER_PCF8574AT									1
	
	/** General pins declaration */
	#ifndef LOCAL_LOW_VOLTAGE_POK
		#define LOCAL_LOW_VOLTAGE_POK	0x00
	#endif
	
	#ifndef RTM_PS
		#define RTM_PS					0x02
	#endif
	
	#ifndef RTM_12V_ENABLE
		#define RTM_12V_ENABLE			0x03
	#endif
	
	#ifndef RTM_3V3_ENABLE
		#define RTM_3V3_ENABLE			0x04
	#endif
	
	#ifndef RTM_I2C_ENABLE
		#define RTM_I2C_ENABLE			0x05
	#endif
	
	#ifndef GA_PULLUP
		#define GA_PULLUP				0x10
	#endif
	
	#ifndef GA0
		#define GA0						0x13
	#endif
	
	#ifndef GA1
		#define GA1						0x12
	#endif
	
	#ifndef GA2
		#define GA2						0x11
	#endif
	
	#ifndef LOCAL_RED_LED
		#define LOCAL_RED_LED			0x15
	#endif
	
	#ifndef LOCAL_GREEN_LED
		#define LOCAL_GREEN_LED			0x16
	#endif
	
	#ifndef LOCAL_BLUE_LED
		#define LOCAL_BLUE_LED			0x17
	#endif
	
	#ifndef LOCAL_RESET_FPGA
		#define LOCAL_RESET_FPGA		0x22
	#endif
	
	#ifndef LOCAL_RELOAD_FPGA
		#define LOCAL_RELOAD_FPGA		0x23
	#endif
	
	#ifndef LOCAL_FPGA2_INIT_DONE
		#define LOCAL_FPGA2_INIT_DONE	0x24
	#endif
	
	#ifndef LOCAL_FPGA1_INIT_DONE
		#define LOCAL_FPGA1_INIT_DONE	0x25
	#endif
	
	#ifndef LOCAL_REG_ENABLE
		#define LOCAL_REG_ENABLE		0x26
	#endif
	
	#ifndef LOCAL_DCDC_ENABLE
		#define LOCAL_DCDC_ENABLE		0x27
	#endif
	
	#ifndef IPMB_SCL
		#define IPMB_SCL				0x30
	#endif
	
	#ifndef IPMB_SDA
		#define IPMB_SDA				0x31
	#endif
	
	#ifndef LOCAL_HANDLE_SWITCH
		#define LOCAL_HANDLE_SWITCH		0x32
	#endif
	
	#ifndef LOCAL_I2C_SCL
		#define LOCAL_I2C_SCL			0x34
	#endif
	
	#ifndef LOCAL_I2C_SDA
		#define LOCAL_I2C_SDA			0x35
	#endif
	
	#ifndef PS1
		#define PS1						0x42
	#endif
	
	#ifndef PS0
		#define PS0						0x43
	#endif
	
	#ifndef PRESENCE_12V
		#define PRESENCE_12V			0x50
	#endif
	
	#ifndef MASTER_TCK
		#define MASTER_TCK				0x60
	#endif
	
	
	#ifndef MASTER_TMS
		#define MASTER_TMS				0x61
	#endif
	
	#ifndef MASTER_TDO
		#define MASTER_TDO				0x62
	#endif
	
	#ifndef MASTER_TDI	
		#define MASTER_TDI				0x63
	#endif
	
	/** User pins declaration */
	#ifndef GPIO_0
		#define GPIO_0					0x22
	#endif
	
	#ifndef GPIO_1
		#define GPIO_1					0x24
	#endif
	
	#ifndef GPIO_2
		#define GPIO_2					0x23
	#endif
	
	#ifndef GPIO_3
		#define GPIO_3					0x25
	#endif
	
	#ifndef GPIO_4
		#define GPIO_4					0x26
	#endif
	
	#ifndef GPIO_5
		#define GPIO_5					0x27
	#endif
	
	#ifndef GPIO_6
		#define GPIO_6					0x51
	#endif
	
	#ifndef GPIO_7
		#define GPIO_7					0x52
	#endif
	
	#ifndef GPIO_8
		#define GPIO_8					0x53
	#endif
	
	#ifndef GPIO_9
		#define GPIO_9					0x46
	#endif
	
	#ifndef GPIO_10
		#define GPIO_10					0x44
	#endif
	
	#ifndef GPIO_11
		#define GPIO_11					0x45
	#endif
	
	#ifndef GPIO_12
		#define GPIO_12					0x47
	#endif
	
	#ifndef GPIO_13
		#define GPIO_13					0x02
	#endif
	
	#ifndef GPIO_14
		#define GPIO_14					0x03
	#endif
	
	#ifndef GPIO_15
		#define GPIO_15					0x04
	#endif
	
	#ifndef GPIO_16
		#define GPIO_16					0x05
	#endif

#endif /* PINOUT_H_ */