/*
 * eeprom.c
 *
 * Created: 15/10/2015 22:47:46
 *  Author: jumendez
 */ 
#include <asf.h>
#include "../inc/eeprom.h"

unsigned char isInit = 0;

void eeprom_init(){
	
	struct spi_device spi_device_conf = {
		.id = 0
	};
	
	gpio_enable_module_pin(AVR32_SPI0_SCK_0_0_PIN, AVR32_SPI0_SCK_0_0_FUNCTION);
	gpio_enable_module_pin(AVR32_SPI0_MISO_0_0_PIN, AVR32_SPI0_MISO_0_0_FUNCTION);
	gpio_enable_module_pin(AVR32_SPI0_MOSI_0_0_PIN, AVR32_SPI0_MOSI_0_0_FUNCTION);
	gpio_enable_module_pin(AVR32_SPI0_NPCS_0_0_PIN, AVR32_SPI0_NPCS_0_0_FUNCTION);

	spi_master_init(&AVR32_SPI0);
	spi_master_setup_device(&AVR32_SPI0, &spi_device_conf, SPI_MODE_0, 1000000, 0);
	spi_enable(&AVR32_SPI0);
	
	isInit = 1;
}

void write_eeprom_byte(unsigned short uiAddress, unsigned char ucData){
	
	uint8_t data_buffer[] = {0x02, (uiAddress & 0xFF00) >> 8, (uiAddress & 0x00FF), ucData};
	unsigned char stop[] = {0x00, 0x01};
	
	struct spi_device spi_device_conf = {
		.id = 0
	};
	
	if(!isInit)
	eeprom_init();
	
	EEPROM_writeEnable();
	
	spi_select_device(&AVR32_SPI0, &spi_device_conf);
	spi_write_packet(&AVR32_SPI0, data_buffer, 4);
	spi_deselect_device(&AVR32_SPI0, &spi_device_conf);
	
	cpu_delay_ms ( 10, CONFIG_PLL0_MUL*BOARD_OSC0_HZ );
	
	return;
}

unsigned char read_eeprom_byte(unsigned short uiAddress){
	
	uint8_t data_buffer[] = {0x03, (uiAddress & 0xFF00) >> 8, (uiAddress & 0x00FF)};
	unsigned char val;
	
	struct spi_device spi_device_conf = {
		.id = 0
	};
	
	if(!isInit)
	eeprom_init();
	
	spi_select_device(&AVR32_SPI0, &spi_device_conf);

	spi_write_packet(&AVR32_SPI0, data_buffer, 3);
	spi_read_packet(&AVR32_SPI0, &val, 1);

	spi_deselect_device(&AVR32_SPI0, &spi_device_conf);
	
	return val;
}

unsigned char EEPROM_status(){
	
	uint8_t data_buffer[] = {0x05};
	unsigned char val;
	
	struct spi_device spi_device_conf = {
		.id = 0
	};
	
	if(!isInit)
	eeprom_init();
	
	spi_select_device(&AVR32_SPI0, &spi_device_conf);

	spi_write_packet(&AVR32_SPI0, data_buffer, 1);
	spi_read_packet(&AVR32_SPI0, &val, 1);

	spi_deselect_device(&AVR32_SPI0, &spi_device_conf);
	
	return val;
}


unsigned char EEPROM_writeEnable(){
	
	uint8_t data_buffer[] = {0x06};
	unsigned char val = 0x00;
	
	struct spi_device spi_device_conf = {
		.id = 0
	};
	
	if(!isInit)
	eeprom_init();

	spi_select_device(&AVR32_SPI0, &spi_device_conf);
	spi_write_packet(&AVR32_SPI0, data_buffer, 1);
	spi_deselect_device(&AVR32_SPI0, &spi_device_conf);
	
	return val;
}

unsigned char EEPROM_writeDisable(){
	
	uint8_t data_buffer[] = {0x04};
	unsigned char val = 0x00;
	
	struct spi_device spi_device_conf = {
		.id = 0
	};
	
	if(!isInit)
	eeprom_init();

	spi_select_device(&AVR32_SPI0, &spi_device_conf);
	spi_write_packet(&AVR32_SPI0, data_buffer, 1);
	spi_deselect_device(&AVR32_SPI0, &spi_device_conf);
	
	return val;
}

void write_startup_flag(unsigned char flag){
	volatile unsigned char* hpmFlagByte = ( volatile unsigned char* ) AVR32_FLASHC_USER_PAGE_ADDRESS;
	flashc_memset8 ( ( void* ) hpmFlagByte, flag, 1, 1 ); // Erase
}