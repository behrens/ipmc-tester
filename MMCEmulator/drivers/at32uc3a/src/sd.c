/*
 * sd.c
 *
 * Created: 20/10/2015 14:49:36
 *  Author: jumendez
 */ 
#include <asf.h>
#include "config/conf_sd_mmc_mci.h"

void init_sd(){
	
	static const gpio_map_t SD_MMC_MCI_GPIO_MAP =
	{
		{SD_SLOT_4BITS_CMD_PIN,   SD_SLOT_4BITS_CMD_FUNCTION },
		{SD_SLOT_4BITS_CLK_PIN,   SD_SLOT_4BITS_CLK_FUNCTION},
		{SD_SLOT_4BITS_DATA0_PIN, SD_SLOT_4BITS_DATA0_FUNCTION},
		{SD_SLOT_4BITS_DATA1_PIN, SD_SLOT_4BITS_DATA1_FUNCTION},
		{SD_SLOT_4BITS_DATA2_PIN, SD_SLOT_4BITS_DATA2_FUNCTION},
		{SD_SLOT_4BITS_DATA3_PIN, SD_SLOT_4BITS_DATA3_FUNCTION}
	};
		
	gpio_enable_module(SD_MMC_MCI_GPIO_MAP,	sizeof(SD_MMC_MCI_GPIO_MAP) / sizeof(SD_MMC_MCI_GPIO_MAP[0]));
	sd_mmc_mci_init(0 /*SD_SLOT_4BITS*/, sysclk_get_pbb_hz(), sysclk_get_cpu_hz());
}

unsigned char card_present(){
	return sd_mmc_mci_mem_check(0/*SD_SLOT_4BITS*/);	// 0x00: not present / 0x01: present
}

unsigned int card_capacity(){
	uint32_t card_size;
	sd_mmc_mci_read_capacity(0/*SD_SLOT_4BITS*/, &card_size);
	return card_size;
}

int open_file(char *name, int flags){
	if(!sd_mmc_mci_mem_check(0))	return -1;
	
	b_fsaccess_init();
	
	nav_reset();
	nav_select(0);
	if(!nav_drive_set(0))			return -2;
	if(!nav_partition_mount())		return -3;
	
	return open(name,flags);
}

ssize_t write_file(int fd, char *str, size_t size){
	if(card_present())
		return write(fd, str, size);
	else
		return -1;
}

ssize_t read_file(int fd, char *str, size_t size){
	if(card_present())
		return read(fd, str, size);
	else
		return -1;
}

int close_file(int fd){
	int ret = close(fd);
	nav_exit();
	return ret;
}

unsigned char create_file(char *filename){
	if(!sd_mmc_mci_mem_check(0))	return -1;
	
	nav_reset();
	nav_select(0);
	if(!nav_drive_set(0))			return -2;
	if(!nav_partition_mount())		return -3;
	
	if(open(filename, O_RDONLY) < 0)
		return nav_file_create(filename);
	else
		return 0xFF;
}