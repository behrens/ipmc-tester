/*
* AT32UC3A1512_ADC_Current.h
*
* Created: 2016/01/06 11:19:22
*  Author: 
*/

#ifdef REGISTER_SENSOR
	#ifdef AT32UC3A1512_ADC_Current
		SENSOR_DECLARATION(update_AT32UC3A1512_ADC_Current_value, init_AT32UC3A1512_ADC_Current_sdr, get_AT32UC3A1512_ADC_Current_sdr_byte, set_AT32UC3A1512_ADC_Current_threshold, 1);
	#endif
#else

	#ifndef AT32UC3A1512_ADC_Current_H_
	#define AT32UC3A1512_ADC_Current_H_

		typedef struct AT32UC3A1512_ADC_Current_s{

			/** Generic information */
			unsigned char sensor_location;  /** AMC (default) or RTM */
			unsigned char init_time; /** Execute init when MP (default) or PP is present */
			unsigned char is_init; 
			unsigned char sensor_number;
			char *name;

			/** Raw to data computing */
			signed short M, B;
			signed char Bexp, Rexp;
			float p1[2];
			float p2[2];

			/** Threshold information */
			unsigned char upper_non_rec;
			unsigned char upper_critical;
			unsigned char upper_non_critical;
			unsigned char lower_non_critical;
			unsigned char lower_critical;
			unsigned char lower_non_rec;

			/** Data used to fill SDR */
			unsigned char id;
			unsigned char entity_inst;
			unsigned char owner_id;

/** User specific function */	
unsigned char  (*pre_user_func)(unsigned char sensor_number);
unsigned char  (*post_user_func)(unsigned char sensor_number);

			/** Sensor dependent data */
			unsigned char MX1_pin;
			unsigned char Mux_ch;
			unsigned char MX3_pin;
			unsigned char MX0_pin;
			unsigned char MX2_pin;
			unsigned char ADC_ch;
		} AT32UC3A1512_ADC_Current_t; 

		unsigned char update_AT32UC3A1512_ADC_Current_value();  
		unsigned char get_AT32UC3A1512_ADC_Current_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset); 
		unsigned char init_AT32UC3A1512_ADC_Current_sdr(unsigned char sdr_id, unsigned char entity_inst, unsigned char owner_id); 
		unsigned char set_AT32UC3A1512_ADC_Current_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr);

		#define AT32UC3A1512_ADC_Current_SDR { \
			0x00, 	  \
			0x00, 	  \
			0x51, 	  \
			0x01, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0xc1, 	  \
			0x00, 	  \
			0x7f, 	  \
			0xFC, 	  \
			CURRENT, 	  \
			0x01, 	  \
			0xff, 	  \
			0x7f, 	  \
			0xff, 	  \
			0x7f, 	  \
			0x3f, 	  \
			0x3f, 	  \
			0x0, 	  \
			0x5, 	  \
			0x00, 	  \
			0x00, 	  \
			0x01, 	  \
			0x01, 	  \
			0x00, 	  \
			0x25, 	  \
			0x88, 	  \
			0x00, 	  \
			0x07, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x7F, 	  \
			0x80, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x02, 	  \
			0x02, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0xC0 	  \
		}

	#endif 
#endif 
