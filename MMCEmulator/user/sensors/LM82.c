/*
* LM82.h
*
* Created: 24/06/2015 10:56:20
*  Author: jumendez
*/

#include <string.h> 
#include <math.h> 

#include "../../drivers/drivers.h" 
#include "../../application/inc/sdr.h"
#include "../../application/inc/payload.h"
#include "../../application/inc/toolfunctions.h"
#include "../../application/inc/project.h"
#include "../user_code/sensors.h" 

#include "LM82.h" 

#ifdef LM82
	LM82_t LM82_sensors[] = LM82; 

	#define LM82_CNT		(sizeof(LM82_sensors) / sizeof((LM82_sensors)[0]))
	
	void LM82_init(unsigned char i2c_addr){ 
		/** Nothing to be done */
	}

	unsigned char LM82_getvalue(unsigned char i2c_addr){ 
		unsigned char temp;
		ext_i2c_received(i2c_addr, 0x0000, 0, 1, &temp);
		return temp;
	}

	unsigned char update_LM82_value(){
		unsigned int i = 0;		
		unsigned char sdr[] = LM82_SDR;
		
		for(i=0; i < LM82_CNT; i++){
			/** Check if init */
			if(!LM82_sensors[i].is_init){
				if(LM82_sensors[i].init_time == MP_PRESENT || (LM82_sensors[i].init_time == PP_PRESENT && get_fru_state() == ACTIVATED)){
					
					if(LM82_sensors[i].pre_user_func && LM82_sensors[i].pre_user_func(LM82_sensors[i].sensor_number)){
						set_sensor_value(LM82_sensors[i].sensor_number,0);
						continue;
					}
					
					LM82_init(LM82_sensors[i].i2c_addr);
					
					if(LM82_sensors[i].post_user_func && LM82_sensors[i].post_user_func(LM82_sensors[i].sensor_number)){
						set_sensor_value(LM82_sensors[i].sensor_number,0);
						continue;
					}
						
					LM82_sensors[i].is_init = 1;
				}
			}else{				
				if(LM82_sensors[i].init_time == PP_PRESENT && get_fru_state() != ACTIVATED){
					LM82_sensors[i].is_init = 0;
				}
			}
			
			/** Call getValue function */
			if(LM82_sensors[i].is_init){
				
				if(LM82_sensors[i].pre_user_func)
					LM82_sensors[i].pre_user_func(LM82_sensors[i].sensor_number);
				
				set_sensor_value(LM82_sensors[i].sensor_number,LM82_getvalue(LM82_sensors[i].i2c_addr));
				
				if(LM82_sensors[i].post_user_func)
					LM82_sensors[i].post_user_func(LM82_sensors[i].sensor_number);
					
				sensor_state_check(LM82_sensors[i].sensor_number, LM82_sensors[i].upper_non_rec, LM82_sensors[i].upper_critical, LM82_sensors[i].upper_non_critical, LM82_sensors[i].lower_non_critical, LM82_sensors[i].lower_critical, LM82_sensors[i].lower_non_rec);
				check_temp_event(LM82_sensors[i].sensor_number, LM82_sensors[i].upper_non_rec, LM82_sensors[i].upper_critical, LM82_sensors[i].upper_non_critical, LM82_sensors[i].lower_non_critical, LM82_sensors[i].lower_critical, LM82_sensors[i].lower_non_rec, sdr[42], sdr[43]);
					
			}
			else						
				set_sensor_value(LM82_sensors[i].sensor_number,0);
		}
		return 0;
	} 

	unsigned char init_LM82_sdr(unsigned char sdr_id, unsigned char entity_inst, unsigned char owner_id){
		/** Variables */
		unsigned char i;	
		float x1, x2, y1, y2;	
		float a, b;	
		signed char Bexp, Rexp;	
		unsigned char sdr[] = LM82_SDR;
		
		/** Computing */
		for(i=0; i < LM82_CNT; i++){ 
			LM82_sensors[i].id = sdr_id+i;
			LM82_sensors[i].entity_inst = entity_inst;
			LM82_sensors[i].owner_id = owner_id;

			x1 = LM82_sensors[i].p1[0];
			x2 = LM82_sensors[i].p2[0];
			y1 = LM82_sensors[i].p1[1];
			y2 = LM82_sensors[i].p2[1];

			a = (y2-y1)/(x2-x1);
			b = y2-(a*x2);

			Bexp = Rexp = 0;

			if(floorf(a) != a || (a > 0 && a > 512) || (a < 1 && a < 512)){
				for(;((a > 0 && a > 512) || (a < 0 && a < -512)) && Rexp < 8; a/=10){
					Rexp++;
					Bexp--;
				}

				for(; ((a > 0 && a < 512) || (a < 0 && a > -512)) && Rexp > -8; a*=10){
					Rexp--;
					Bexp++;
				}

				if((a > 0 && a >= 512) || (a < 0 && a <= -512)){
					a /= 10;
					Rexp++;
					Bexp--;
				}
			}

			if(floorf(b) != b || (b > 0 && b > 512) || (b < 1 && b < 512)){
				for(;((b > 0 && b > 512) || (b < 0 && b < -512)) && Bexp < 8; b/=10){
					Bexp++;
				}

				for(; ((b > 0 && a < 512) || (b < 0 && b > -512)) && Bexp > -8; b*=10){
					Bexp--;
				}

				if((b > 0 && b >= 512) || (b < 0 && b <= -512)){
					b /= 10;
					Bexp++;
				}
			}

			LM82_sensors[i].M = floor(a);
			LM82_sensors[i].B = floor(b);
			LM82_sensors[i].Rexp = Rexp;
			LM82_sensors[i].Bexp = Bexp;
			
			/** Decimal to raw for thresholds */
			LM82_sensors[i].upper_non_rec = val_to_raw(LM82_sensors[i].upper_non_rec, LM82_sensors[i].M, LM82_sensors[i].B, LM82_sensors[i].Rexp, LM82_sensors[i].Bexp, (sdr[20] & 0x80));
			LM82_sensors[i].upper_critical = val_to_raw(LM82_sensors[i].upper_critical, LM82_sensors[i].M, LM82_sensors[i].B, LM82_sensors[i].Rexp, LM82_sensors[i].Bexp, (sdr[20] & 0x80));
			LM82_sensors[i].upper_non_critical = val_to_raw(LM82_sensors[i].upper_non_critical, LM82_sensors[i].M, LM82_sensors[i].B, LM82_sensors[i].Rexp, LM82_sensors[i].Bexp, (sdr[20] & 0x80));
			LM82_sensors[i].lower_non_rec = val_to_raw(LM82_sensors[i].lower_non_rec, LM82_sensors[i].M, LM82_sensors[i].B, LM82_sensors[i].Rexp, LM82_sensors[i].Bexp, (sdr[20] & 0x80));
			LM82_sensors[i].lower_critical = val_to_raw(LM82_sensors[i].lower_critical, LM82_sensors[i].M, LM82_sensors[i].B, LM82_sensors[i].Rexp, LM82_sensors[i].Bexp, (sdr[20] & 0x80));
			LM82_sensors[i].lower_non_critical = val_to_raw(LM82_sensors[i].lower_non_critical, LM82_sensors[i].M, LM82_sensors[i].B, LM82_sensors[i].Rexp, LM82_sensors[i].Bexp, (sdr[20] & 0x80));
		}
		
		return LM82_CNT;
	}

	unsigned char get_LM82_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset){
		unsigned int i, j;
		unsigned char sdr[] = LM82_SDR;
		unsigned char max_sdr_size = sizeof(sdr);
		unsigned char read_len = 0;	
		
		for(i=0; i < LM82_CNT; i++){
			if(LM82_sensors[i].sensor_number == sensor_id){
				for(j=0; j < len && (j+offset) < (max_sdr_size+strlen((const char *)LM82_sensors[i].name)); j++, read_len++){
					if(j+offset == 0)					byte[j] = LM82_sensors[i].id;
					else if(j+offset == 4)				byte[j] = (max_sdr_size+strlen((const char *)LM82_sensors[i].name)) - 5;
					else if(j+offset == 8 && LM82_sensors[i].entity_inst == 0x60)	byte[j] = 0xC0;
					else if(j+offset == 9)				byte[j] = LM82_sensors[i].entity_inst;
					else if(j+offset == 5)				byte[j] = LM82_sensors[i].owner_id;
					else if(j+offset == 7)				byte[j] = LM82_sensors[i].sensor_number;
					else if(j+offset == max_sdr_size-1)	byte[j] = 0xC0 | strlen((const char *)LM82_sensors[i].name);
					else if(j+offset == 24)				byte[j] = ((unsigned char)LM82_sensors[i].M) & 0xFF;
					else if(j+offset == 25)				byte[j] = (((unsigned char)(LM82_sensors[i].M >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 26)				byte[j] = ((unsigned char)LM82_sensors[i].B) & 0xFF;
					else if(j+offset == 27)				byte[j] = (((unsigned char)(LM82_sensors[i].B >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 29)				byte[j] = ((LM82_sensors[i].Rexp & 0x0F) << 4) | (LM82_sensors[i].Bexp & 0x0F);
					else if(j+offset == 31)				byte[j] = (LM82_sensors[i].upper_non_critical - LM82_sensors[i].lower_non_critical)/2;
					else if(j+offset == 32)				byte[j] = LM82_sensors[i].upper_non_critical;
					else if(j+offset == 33)				byte[j] = LM82_sensors[i].lower_non_critical;
					else if(j+offset == 36)				byte[j] = LM82_sensors[i].upper_non_rec;
					else if(j+offset == 37)				byte[j] = LM82_sensors[i].upper_critical;
					else if(j+offset == 38)				byte[j] = LM82_sensors[i].upper_non_critical;
					else if(j+offset == 39)				byte[j] = LM82_sensors[i].lower_non_rec;
					else if(j+offset == 40)				byte[j] = LM82_sensors[i].lower_critical;
					else if(j+offset == 41)				byte[j] = LM82_sensors[i].lower_non_critical;
					else if(j+offset >= max_sdr_size)	byte[j] = LM82_sensors[i].name[(j+offset)-max_sdr_size];
					else								byte[j] = sdr[j+offset];
				}
				return read_len;
			}
		}
		return 0x00;
	}

	unsigned char set_LM82_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr){
		unsigned char i;
		
		for(i = 0; i < LM82_CNT; i++){
			if(LM82_sensors[i].sensor_number == sensor_id){
				if (mask & 0x01) LM82_sensors[i].lower_non_critical = lnc; 
				if (mask & 0x02) LM82_sensors[i].lower_critical = lc; 
				if (mask & 0x04) LM82_sensors[i].lower_non_rec = lnr; 
				if (mask & 0x08) LM82_sensors[i].upper_non_critical = unc;
				if (mask & 0x10) LM82_sensors[i].upper_critical = uc;
				if (mask & 0x20) LM82_sensors[i].upper_non_rec = unr;
				
				return 0x00;			
			}
		}
		
		return 0xFF;
	}
	
#endif 
