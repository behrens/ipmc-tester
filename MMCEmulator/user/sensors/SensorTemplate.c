/*
* SensorTemplate.c
*
* Created: 2015/12/16 18:48:15
* Author: Julian Mendez <julian.mendez@cern.ch>
*
* HowTo:
*	-> Replace all SensorTemplate by the name of the sensors (you can use replace all funtion)
*	-> Write the init and getValue functions
*
*/

#include <string.h> 
#include <math.h> 

#include "../../drivers/drivers.h" 
#include "../../application/inc/sdr.h" 
#include "../../application/inc/payload.h" 
#include "../../application/inc/project.h" 
#include "../../application/inc/toolfunctions.h" 
#include "../user_code/sensors.h" 

#include "SensorTemplate.h" 

#ifdef SensorTemplate
	SensorTemplate_t SensorTemplate_sensors[] = SensorTemplate; 

	#define SensorTemplate_CNT		(sizeof(SensorTemplate_sensors) / sizeof((SensorTemplate_sensors)[0]))

	void SensorTemplate_init(unsigned id){ 
		/** To be defined: called to init the sensor 
				Sensor specific data defined in .h file can be used here.
				E.g.:
					-> SensorTemplate_sensors[id].i2c_addr
		*/
	}

	unsigned char SensorTemplate_getvalue(unsigned id){ 
		/** To be defined: called to init the sensor 
				Sensor specific data defined in .h file can be used here.
				E.g.:
					-> SensorTemplate_sensors[id].i2c_addr
		*/
	}

	unsigned char update_SensorTemplate_value(){
		unsigned int i; 
		unsigned char sdr[] = SensorTemplate_SDR;	

		for(i=0; i < SensorTemplate_CNT; i++){ 
			/** Check if init */
			if(!SensorTemplate_sensors[i].is_init){
				if(SensorTemplate_sensors[i].init_time == MP_PRESENT || (SensorTemplate_sensors[i].init_time == PP_PRESENT && get_fru_state() == ACTIVATED)){
					if(SensorTemplate_sensors[i].pre_user_func && SensorTemplate_sensors[i].pre_user_func(SensorTemplate_sensors[i].sensor_number)){
						set_sensor_value(SensorTemplate_sensors[i].sensor_number,0);
						continue;
					}
					SensorTemplate_init(i); 
					if(SensorTemplate_sensors[i].post_user_func && SensorTemplate_sensors[i].post_user_func(SensorTemplate_sensors[i].sensor_number)){
						set_sensor_value(SensorTemplate_sensors[i].sensor_number,0);
						continue;
					}
					SensorTemplate_sensors[i].is_init = 1;
				}
			}else{
				if(SensorTemplate_sensors[i].init_time == PP_PRESENT && get_fru_state() != ACTIVATED){
					SensorTemplate_sensors[i].is_init = 0;
				}
			}

			/** Call getValue function */
			if(SensorTemplate_sensors[i].is_init){
				if(SensorTemplate_sensors[i].pre_user_func && SensorTemplate_sensors[i].pre_user_func(SensorTemplate_sensors[i].sensor_number)){
					set_sensor_value(SensorTemplate_sensors[i].sensor_number,0);
					continue;
				}
				set_sensor_value(SensorTemplate_sensors[i].sensor_number,SensorTemplate_getvalue(i)); 
				if(SensorTemplate_sensors[i].post_user_func && SensorTemplate_sensors[i].post_user_func(SensorTemplate_sensors[i].sensor_number)){
					set_sensor_value(SensorTemplate_sensors[i].sensor_number,0);
					continue;
				}
				sensor_state_check(SensorTemplate_sensors[i].sensor_number, SensorTemplate_sensors[i].upper_non_rec, SensorTemplate_sensors[i].upper_critical, SensorTemplate_sensors[i].upper_non_critical, SensorTemplate_sensors[i].lower_non_critical, SensorTemplate_sensors[i].lower_critical, SensorTemplate_sensors[i].lower_non_rec); 
				check_temp_event(SensorTemplate_sensors[i].sensor_number, SensorTemplate_sensors[i].upper_non_rec, SensorTemplate_sensors[i].upper_critical, SensorTemplate_sensors[i].upper_non_critical, SensorTemplate_sensors[i].lower_non_critical, SensorTemplate_sensors[i].lower_critical, SensorTemplate_sensors[i].lower_non_rec, sdr[42], sdr[43]); 
			}
			else						set_sensor_value(SensorTemplate_sensors[i].sensor_number,0); 
		}
		return 0;
	} 

	unsigned char init_SensorTemplate_sdr(unsigned char sdr_id, unsigned char entity_inst, unsigned char owner_id){
		/** Variables */
		unsigned char i;	
		float x1, x2, y1, y2;	
		float a, b;	
		signed char Bexp, Rexp;	

		unsigned char sdr[] = SensorTemplate_SDR;	

		/** Computing */
		for(i=0; i < SensorTemplate_CNT; i++){ 
			SensorTemplate_sensors[i].id = sdr_id+i;
			SensorTemplate_sensors[i].entity_inst = entity_inst;
			SensorTemplate_sensors[i].owner_id = owner_id;

			x1 = SensorTemplate_sensors[i].p1[0];
			x2 = SensorTemplate_sensors[i].p2[0];
			y1 = SensorTemplate_sensors[i].p1[1];
			y2 = SensorTemplate_sensors[i].p2[1];

			a = (y2-y1)/(x2-x1);
			b = y2-(a*x2);

			Bexp = Rexp = 0;

			if(floorf(a) != a || (a > 0 && a > 512) || (a < 1 && a < 512)){
				for(;((a > 0 && a > 512) || (a < 0 && a < -512)) && Rexp < 8; a/=10){
					Rexp++;
					Bexp--;
				}

				for(; ((a > 0 && a < 512) || (a < 0 && a > -512)) && Rexp > -8; a*=10){
					Rexp--;
					Bexp++;
				}

				if((a > 0 && a >= 512) || (a < 0 && a <= -512)){
					a /= 10;
					Rexp++;
					Bexp--;
				}
			}

			if(floorf(b) != b || (b > 0 && b > 512) || (b < 1 && b < 512)){
				for(;((b > 0 && b > 512) || (b < 0 && b < -512)) && Bexp < 8; b/=10){
					Bexp++;
				}

				for(; ((b > 0 && a < 512) || (b < 0 && b > -512)) && Bexp > -8; b*=10){
					Bexp--;
				}

				if((b > 0 && b >= 512) || (b < 0 && b <= -512)){
					b /= 10;
					Bexp++;
				}
			}

			SensorTemplate_sensors[i].M = floor(a);
			SensorTemplate_sensors[i].B = floor(b);
			SensorTemplate_sensors[i].Rexp = Rexp;
			SensorTemplate_sensors[i].Bexp = Bexp;

			/** Decimal to raw for thresholds */
			SensorTemplate_sensors[i].upper_non_rec = val_to_raw(SensorTemplate_sensors[i].upper_non_rec, SensorTemplate_sensors[i].M, SensorTemplate_sensors[i].B, SensorTemplate_sensors[i].Rexp, SensorTemplate_sensors[i].Bexp, (sdr[20] & 0x80)); 
			SensorTemplate_sensors[i].upper_critical = val_to_raw(SensorTemplate_sensors[i].upper_critical, SensorTemplate_sensors[i].M, SensorTemplate_sensors[i].B, SensorTemplate_sensors[i].Rexp, SensorTemplate_sensors[i].Bexp, (sdr[20] & 0x80)); 
			SensorTemplate_sensors[i].upper_non_critical = val_to_raw(SensorTemplate_sensors[i].upper_non_critical, SensorTemplate_sensors[i].M, SensorTemplate_sensors[i].B, SensorTemplate_sensors[i].Rexp, SensorTemplate_sensors[i].Bexp, (sdr[20] & 0x80)); 
			SensorTemplate_sensors[i].lower_non_rec = val_to_raw(SensorTemplate_sensors[i].lower_non_rec, SensorTemplate_sensors[i].M, SensorTemplate_sensors[i].B, SensorTemplate_sensors[i].Rexp, SensorTemplate_sensors[i].Bexp, (sdr[20] & 0x80)); 
			SensorTemplate_sensors[i].lower_critical = val_to_raw(SensorTemplate_sensors[i].lower_critical, SensorTemplate_sensors[i].M, SensorTemplate_sensors[i].B, SensorTemplate_sensors[i].Rexp, SensorTemplate_sensors[i].Bexp, (sdr[20] & 0x80)); 
			SensorTemplate_sensors[i].lower_non_critical = val_to_raw(SensorTemplate_sensors[i].lower_non_critical, SensorTemplate_sensors[i].M, SensorTemplate_sensors[i].B, SensorTemplate_sensors[i].Rexp, SensorTemplate_sensors[i].Bexp, (sdr[20] & 0x80)); 
		}
		return SensorTemplate_CNT;
	}

	unsigned char get_SensorTemplate_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset){
		unsigned int i, j;
		unsigned char sdr[] = SensorTemplate_SDR;
		unsigned char max_sdr_size = sizeof(sdr);
		unsigned char read_len = 0;	

		for(i=0; i < SensorTemplate_CNT; i++){
			if(SensorTemplate_sensors[i].sensor_number == sensor_id){
				for(j=0; j < len && (j+offset) < (max_sdr_size+strlen((const char *)SensorTemplate_sensors[i].name)); j++, read_len++){
					if(j+offset == 0)					byte[j] = SensorTemplate_sensors[i].id;
					else if(j+offset == 4)				byte[j] = (max_sdr_size+strlen((const char *)SensorTemplate_sensors[i].name)) - 5;
					else if(j+offset == 9)				byte[j] = SensorTemplate_sensors[i].entity_inst;
					else if(j+offset == 5)				byte[j] = SensorTemplate_sensors[i].owner_id;
					else if(j+offset == 7)				byte[j] = SensorTemplate_sensors[i].sensor_number;
					else if(j+offset == max_sdr_size-1)	byte[j] = 0xC0 | strlen((const char *)SensorTemplate_sensors[i].name);
					else if(j+offset == 24)				byte[j] = ((unsigned char)SensorTemplate_sensors[i].M) & 0xFF;
					else if(j+offset == 25)				byte[j] = (((unsigned char)(SensorTemplate_sensors[i].M >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 26)				byte[j] = ((unsigned char)SensorTemplate_sensors[i].B) & 0xFF;
					else if(j+offset == 27)				byte[j] = (((unsigned char)(SensorTemplate_sensors[i].B >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 29)				byte[j] = ((SensorTemplate_sensors[i].Rexp & 0x0F) << 4) | (SensorTemplate_sensors[i].Bexp & 0x0F);
					else if(j+offset == 31)				byte[j] = (SensorTemplate_sensors[i].upper_non_critical - SensorTemplate_sensors[i].lower_non_critical)/2;
					else if(j+offset == 32)				byte[j] = SensorTemplate_sensors[i].upper_non_critical;
					else if(j+offset == 33)				byte[j] = SensorTemplate_sensors[i].lower_non_critical;
					else if(j+offset == 36)				byte[j] = SensorTemplate_sensors[i].upper_non_rec;
					else if(j+offset == 37)				byte[j] = SensorTemplate_sensors[i].upper_critical;
					else if(j+offset == 38)				byte[j] = SensorTemplate_sensors[i].upper_non_critical;
					else if(j+offset == 39)				byte[j] = SensorTemplate_sensors[i].lower_non_rec;
					else if(j+offset == 40)				byte[j] = SensorTemplate_sensors[i].lower_critical;
					else if(j+offset == 41)				byte[j] = SensorTemplate_sensors[i].lower_non_critical;
					else if(j+offset >= max_sdr_size)	byte[j] = SensorTemplate_sensors[i].name[(j+offset)-max_sdr_size];
					else								byte[j] = sdr[j+offset];
				}
				return read_len;
			}
		}
		return 0x00;
	}

unsigned char set_SensorTemplate_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr){ 
	unsigned char i; 

	for(i = 0; i < SensorTemplate_CNT; i++){ 
		if(SensorTemplate_sensors[i].sensor_number == sensor_id){ 
			if (mask & 0x01) SensorTemplate_sensors[i].lower_non_critical = lnc;  
			if (mask & 0x02) SensorTemplate_sensors[i].lower_critical = lc;  
			if (mask & 0x04) SensorTemplate_sensors[i].lower_non_rec = lnr;  
			if (mask & 0x08) SensorTemplate_sensors[i].upper_non_critical = unc; 
			if (mask & 0x10) SensorTemplate_sensors[i].upper_critical = uc; 
			if (mask & 0x20) SensorTemplate_sensors[i].upper_non_rec = unr; 

			return 0x00;		 
		} 
	} 

	return 0xFF; 
} 

#endif 
