/*
* LTC2990_voltage.h
*
* Created: 2015/12/16 18:48:15
*  Author: 
*/

#ifdef REGISTER_SENSOR
	#ifdef LTC2990_voltage
		SENSOR_DECLARATION(update_LTC2990_voltage_value, init_LTC2990_voltage_sdr, get_LTC2990_voltage_sdr_byte, set_LTC2990_voltage_threshold, 1);
	#endif
#else

	#ifndef LTC2990_voltage_H_
	#define LTC2990_voltage_H_

		typedef struct LTC2990_voltage_s{

			/** Generic information */
			unsigned char sensor_location;  /** AMC (default) or RTM */
			unsigned char init_time; /** Execute init when MP (default) or PP is present */
			unsigned char is_init; 
			unsigned char sensor_number;
			char *name;

			/** Raw to data computing */
			signed short M, B;
			signed char Bexp, Rexp;
			float p1[2];
			float p2[2];

			/** Threshold information */
			unsigned char upper_non_rec;
			unsigned char upper_critical;
			unsigned char upper_non_critical;
			unsigned char lower_non_critical;
			unsigned char lower_critical;
			unsigned char lower_non_rec;

			/** Data used to fill SDR */
			unsigned char id;
			unsigned char entity_inst;
			unsigned char owner_id;
			unsigned char entity_id;

/** User specific function */	
unsigned char  (*pre_user_func)(unsigned char sensor_number);
unsigned char  (*post_user_func)(unsigned char sensor_number);

			/** Sensor dependent data */
			unsigned char i2c_addr;
			unsigned char opMode;
			unsigned char channelAddress;
		} LTC2990_voltage_t; 

		unsigned char update_LTC2990_voltage_value();  
		unsigned char get_LTC2990_voltage_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset); 
		unsigned char init_LTC2990_voltage_sdr(unsigned char sdr_id, unsigned char sensor_id, unsigned char entity_inst, unsigned char entity_id, unsigned char owner_id); 
		unsigned char set_LTC2990_voltage_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr);

		#define LTC2990_voltage_SDR { \
			0x00, 	  \
			0x00, 	  \
			0x51, 	  \
			0x01, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0xc1, 	  \
			0x00, 	  \
			0x7f, 	  \
			0xFC, 	  \
			VOLTAGE, 	  \
			0x01, 	  \
			0xff, 	  \
			0x7f, 	  \
			0xff, 	  \
			0x7f, 	  \
			0x3f, 	  \
			0x3f, 	  \
			0x0, 	  \
			0x1, 	  \
			0x00, 	  \
			0x00, 	  \
			0x01, 	  \
			0x01, 	  \
			0x00, 	  \
			0x25, 	  \
			0x88, 	  \
			0x00, 	  \
			0x07, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x7F, 	  \
			0x80, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x02, 	  \
			0x02, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0xC0 	  \
		}

	#endif 
#endif 
