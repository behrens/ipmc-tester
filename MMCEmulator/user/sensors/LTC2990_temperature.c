/*
* LTC2990_temperature.c
*
* Created: 2015/12/16 18:48:15
* Author: Julian Mendez <julian.mendez@cern.ch>
*
*/

#include <string.h> 
#include <math.h> 

#include "../../drivers/drivers.h" 
#include "../../application/inc/sdr.h" 
#include "../../application/inc/payload.h" 
#include "../../application/inc/project.h" 
#include "../../application/inc/toolfunctions.h" 
#include "../user_code/sensors.h" 

#include "LTC2990_temperature.h" 

#ifdef LTC2990_temperature
	LTC2990_temperature_t LTC2990_temperature_sensors[] = LTC2990_temperature; 

	#define LTC2990_temperature_CNT		(sizeof(LTC2990_temperature_sensors) / sizeof((LTC2990_temperature_sensors)[0]))
	
	#define LTC2990_STATUS_REGISTER 0x0
	#define LTC2990_CONTROL_REGISTER 0x1
	#define LTC2990_TRIGGER_REGISTER 0x2
	#define LTC2990_TINT_MSB 0x4
	#define LTC2990_TINT_LSB 0x5
	#define LTC2990_V1_MSB 0x6
	#define LTC2990_V1_LSB 0x7
	#define LTC2990_V2_MSB 0x8
	#define LTC2990_V2_LSB 0x9
	#define LTC2990_V3_MSB 0xa
	#define LTC2990_V3_LSB 0xb
	#define LTC2990_V4_MSB 0xc
	#define LTC2990_V4_LSB 0xd
	

	void LTC2990_temperature_init(unsigned char i2c_addr){ 
		/** To be defined: called to init the sensor 
				Sensor specific data defined in .h file can be used here.
				E.g.:
					-> LTC2990_temperature_sensors[id].i2c_addr
		*/
/************************************************************************/
/* // config a/b cases
// 0x1F => case 7, V1,V2,V3,V4 -> auto trigger // 0x5F => case 7, V1,V2,V3,V4 -> single trigger
// 0x1E => case 6, V1-V2,V3-V4 -> auto trigger // 0x5E => case 6, V1-V2,V3-V4 -> single trigger
// 0x18 => case 0, V1,T2       -> auto trigger // 0x58 => case 0, V1,T2       -> single trigger
// 0x19 => case 1, I1,T2       -> auto trigger // 0x59 => case 1, I1,T2       -> single trigger
                                                                     */
/************************************************************************/		
		unsigned char operation_mode = 0x58;
		
		ext_i2c_send(i2c_addr, LTC2990_CONTROL_REGISTER, 1, 1, &operation_mode);	//LTC2990 operation mode configuration
		
	}

	unsigned char LTC2990_temperature_getvalue(unsigned char i2c_addr){ 
		unsigned char ltc2990_trigger = 0xFF;
		unsigned char xRes[2];
		unsigned char conversion_status[1];
		unsigned char ack = 0;
		unsigned char timeout = 0;
		unsigned int val = 0;
		
		conversion_status[0] = 1;
		
		ext_i2c_send(i2c_addr, LTC2990_TRIGGER_REGISTER, 1, 1, &ltc2990_trigger);	//Trigger a conversion
		ack = ext_i2c_received(i2c_addr, LTC2990_STATUS_REGISTER, 1, 1, conversion_status);
		while(conversion_status[0] & 0x01 && timeout < 10){
			ack = ext_i2c_received(i2c_addr, LTC2990_STATUS_REGISTER, 1, 1, conversion_status); // while bit0 not 0 i.e. conversion in progress, keep reading the status
			timeout++;
		}
	
		if(timeout >= 10){
			return 0x00;
		}
		ack = ext_i2c_received(i2c_addr, LTC2990_V3_MSB, 1, 2, xRes);
/**
  //-- if single acquisition mode : begin
  if ((gSensors[pSensor].config_b & 0x40) || (gSensors[pSensor].config_a & 0x40))
  {
	  xTrig[0] = 0x02; // point to the trigger register on LTC2990
	  xTrig[1] = 0xFF; // write any value to trigger conversion
	  twim_write ( &SENS_TWIM_MODULE,  xTrig    , 2, gSensors[pSensor].addr, FALSE ); // execute trigger
	  twim_write ( &SENS_TWIM_MODULE, &xStatAddr, 1, gSensors[pSensor].addr, FALSE ); // set addr pointer to status reg
	  xStat[0]=0x01;
	  while (xStat[0] & 0x01) // while bit0 not 0 i.e. conversion in progress, keep reading the status
	  {
		  twim_read  ( &SENS_TWIM_MODULE, xStat, 1, gSensors[pSensor].addr, FALSE ); // read status reg
	  }
  }
  //-- if single acquisition mode : end
  
  
  
  
  //---- set addr pointer to result registers --//
  if ( twim_write ( &SENS_TWIM_MODULE, &xAddr, 1, gSensors[pSensor].addr, FALSE ) )
  {
	  dprintf ( "Sensor write error (%d)\n", pSensor );
  }

  if ( twim_read ( &SENS_TWIM_MODULE, xRegs, 12, gSensors[pSensor].addr, FALSE ) )
  {
	  dprintf ( "Sensor read error (%d)\n", pSensor );
  }
**/		
//		return xRes[0];
		return (xRes[0] << 5) + (xRes[1] >> 3);
//		return 33;	
//		return (unsigned char)(xRawValue*(255.0f/1024.0f));
		
	}

	unsigned sensor_to_be_updated = 0;	//JM: Added to accelerate sensor reading
	
	unsigned char update_LTC2990_temperature_value(){
		unsigned int i = sensor_to_be_updated;
		unsigned char sdr[] = LTC2990_temperature_SDR;	

		//for(i=0; i < LTC2990_temperature_CNT; i++){ 
			/** Check if init */
			if(!LTC2990_temperature_sensors[i].is_init){
				if(LTC2990_temperature_sensors[i].init_time == MP_PRESENT || (LTC2990_temperature_sensors[i].init_time == PP_PRESENT && get_fru_state() == ACTIVATED)){
					if(LTC2990_temperature_sensors[i].pre_user_func && LTC2990_temperature_sensors[i].pre_user_func(LTC2990_temperature_sensors[i].sensor_number)){
						set_sensor_value(LTC2990_temperature_sensors[i].sensor_number,0);
						continue;
					}
					LTC2990_temperature_init(LTC2990_temperature_sensors[i].i2c_addr); 
					if(LTC2990_temperature_sensors[i].post_user_func && LTC2990_temperature_sensors[i].post_user_func(LTC2990_temperature_sensors[i].sensor_number)){
						set_sensor_value(LTC2990_temperature_sensors[i].sensor_number,0);
						continue;
					}
					LTC2990_temperature_sensors[i].is_init = 1;
				}
			}else{
				if(LTC2990_temperature_sensors[i].init_time == PP_PRESENT && get_fru_state() != ACTIVATED){
					LTC2990_temperature_sensors[i].is_init = 0;
				}
			}

			/** Call getValue function */
			if(LTC2990_temperature_sensors[i].is_init){
				if(LTC2990_temperature_sensors[i].pre_user_func && LTC2990_temperature_sensors[i].pre_user_func(LTC2990_temperature_sensors[i].sensor_number)){
					set_sensor_value(LTC2990_temperature_sensors[i].sensor_number,0);
					continue;
				}
				set_sensor_value(LTC2990_temperature_sensors[i].sensor_number,LTC2990_temperature_getvalue(LTC2990_temperature_sensors[i].i2c_addr)); 
				if(LTC2990_temperature_sensors[i].post_user_func && LTC2990_temperature_sensors[i].post_user_func(LTC2990_temperature_sensors[i].sensor_number)){
					set_sensor_value(LTC2990_temperature_sensors[i].sensor_number,0);
					continue;
				}
				sensor_state_check(LTC2990_temperature_sensors[i].sensor_number, LTC2990_temperature_sensors[i].upper_non_rec, LTC2990_temperature_sensors[i].upper_critical, LTC2990_temperature_sensors[i].upper_non_critical, LTC2990_temperature_sensors[i].lower_non_critical, LTC2990_temperature_sensors[i].lower_critical, LTC2990_temperature_sensors[i].lower_non_rec); 
				check_temp_event(LTC2990_temperature_sensors[i].sensor_number, LTC2990_temperature_sensors[i].upper_non_rec, LTC2990_temperature_sensors[i].upper_critical, LTC2990_temperature_sensors[i].upper_non_critical, LTC2990_temperature_sensors[i].lower_non_critical, LTC2990_temperature_sensors[i].lower_critical, LTC2990_temperature_sensors[i].lower_non_rec, sdr[42], sdr[43]); 
			}
			else						set_sensor_value(LTC2990_temperature_sensors[i].sensor_number,0); 
		//}
		
		sensor_to_be_updated++;
		if(sensor_to_be_updated >= LTC2990_temperature_CNT) sensor_to_be_updated = 0;
		
		return 0;
	} 

	unsigned char init_LTC2990_temperature_sdr(unsigned char sdr_id, unsigned char entity_inst, unsigned char owner_id){
		/** Variables */
		unsigned char i;	
		float x1, x2, y1, y2;	
		float a, b;	
		signed char Bexp, Rexp;	

		unsigned char sdr[] = LTC2990_temperature_SDR;	

		/** Computing */
		for(i=0; i < LTC2990_temperature_CNT; i++){ 
			LTC2990_temperature_sensors[i].id = sdr_id+i;
			LTC2990_temperature_sensors[i].entity_inst = entity_inst;
			LTC2990_temperature_sensors[i].owner_id = owner_id;

			x1 = LTC2990_temperature_sensors[i].p1[0];
			x2 = LTC2990_temperature_sensors[i].p2[0];
			y1 = LTC2990_temperature_sensors[i].p1[1];
			y2 = LTC2990_temperature_sensors[i].p2[1];

			a = (y2-y1)/(x2-x1);
			b = y2-(a*x2);

			Bexp = Rexp = 0;

			if(floorf(a) != a || (a > 0 && a > 512) || (a < 1 && a < 512)){
				for(;((a > 0 && a > 512) || (a < 0 && a < -512)) && Rexp < 8; a/=10){
					Rexp++;
					Bexp--;
				}

				for(; ((a > 0 && a < 512) || (a < 0 && a > -512)) && Rexp > -8; a*=10){
					Rexp--;
					Bexp++;
				}

				if((a > 0 && a >= 512) || (a < 0 && a <= -512)){
					a /= 10;
					Rexp++;
					Bexp--;
				}
			}

			if(floorf(b) != b || (b > 0 && b > 512) || (b < 1 && b < 512)){
				for(;((b > 0 && b > 512) || (b < 0 && b < -512)) && Bexp < 8; b/=10){
					Bexp++;
				}

				for(; ((b > 0 && a < 512) || (b < 0 && b > -512)) && Bexp > -8; b*=10){
					Bexp--;
				}

				if((b > 0 && b >= 512) || (b < 0 && b <= -512)){
					b /= 10;
					Bexp++;
				}
			}

			LTC2990_temperature_sensors[i].M = floor(a);
			LTC2990_temperature_sensors[i].B = floor(b);
			LTC2990_temperature_sensors[i].Rexp = Rexp;
			LTC2990_temperature_sensors[i].Bexp = Bexp;

			/** Decimal to raw for thresholds */
			LTC2990_temperature_sensors[i].upper_non_rec = val_to_raw(LTC2990_temperature_sensors[i].upper_non_rec, LTC2990_temperature_sensors[i].M, LTC2990_temperature_sensors[i].B, LTC2990_temperature_sensors[i].Rexp, LTC2990_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
			LTC2990_temperature_sensors[i].upper_critical = val_to_raw(LTC2990_temperature_sensors[i].upper_critical, LTC2990_temperature_sensors[i].M, LTC2990_temperature_sensors[i].B, LTC2990_temperature_sensors[i].Rexp, LTC2990_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
			LTC2990_temperature_sensors[i].upper_non_critical = val_to_raw(LTC2990_temperature_sensors[i].upper_non_critical, LTC2990_temperature_sensors[i].M, LTC2990_temperature_sensors[i].B, LTC2990_temperature_sensors[i].Rexp, LTC2990_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
			LTC2990_temperature_sensors[i].lower_non_rec = val_to_raw(LTC2990_temperature_sensors[i].lower_non_rec, LTC2990_temperature_sensors[i].M, LTC2990_temperature_sensors[i].B, LTC2990_temperature_sensors[i].Rexp, LTC2990_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
			LTC2990_temperature_sensors[i].lower_critical = val_to_raw(LTC2990_temperature_sensors[i].lower_critical, LTC2990_temperature_sensors[i].M, LTC2990_temperature_sensors[i].B, LTC2990_temperature_sensors[i].Rexp, LTC2990_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
			LTC2990_temperature_sensors[i].lower_non_critical = val_to_raw(LTC2990_temperature_sensors[i].lower_non_critical, LTC2990_temperature_sensors[i].M, LTC2990_temperature_sensors[i].B, LTC2990_temperature_sensors[i].Rexp, LTC2990_temperature_sensors[i].Bexp, (sdr[20] & 0x80)); 
		}
		return LTC2990_temperature_CNT;
	}

	unsigned char get_LTC2990_temperature_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset){
		unsigned int i, j;
		unsigned char sdr[] = LTC2990_temperature_SDR;
		unsigned char max_sdr_size = sizeof(sdr);
		unsigned char read_len = 0;	

		for(i=0; i < LTC2990_temperature_CNT; i++){
			if(LTC2990_temperature_sensors[i].sensor_number == sensor_id){
				for(j=0; j < len && (j+offset) < (max_sdr_size+strlen((const char *)LTC2990_temperature_sensors[i].name)); j++, read_len++){
					if(j+offset == 0)					byte[j] = LTC2990_temperature_sensors[i].id;
					else if(j+offset == 4)				byte[j] = (max_sdr_size+strlen((const char *)LTC2990_temperature_sensors[i].name)) - 5;
					else if(j+offset == 9)				byte[j] = LTC2990_temperature_sensors[i].entity_inst;
					else if(j+offset == 5)				byte[j] = LTC2990_temperature_sensors[i].owner_id;
					else if(j+offset == 7)				byte[j] = LTC2990_temperature_sensors[i].sensor_number;
					else if(j+offset == max_sdr_size-1)	byte[j] = 0xC0 | strlen((const char *)LTC2990_temperature_sensors[i].name);
					else if(j+offset == 24)				byte[j] = ((unsigned char)LTC2990_temperature_sensors[i].M) & 0xFF;
					else if(j+offset == 25)				byte[j] = (((unsigned char)(LTC2990_temperature_sensors[i].M >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 26)				byte[j] = ((unsigned char)LTC2990_temperature_sensors[i].B) & 0xFF;
					else if(j+offset == 27)				byte[j] = (((unsigned char)(LTC2990_temperature_sensors[i].B >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 29)				byte[j] = ((LTC2990_temperature_sensors[i].Rexp & 0x0F) << 4) | (LTC2990_temperature_sensors[i].Bexp & 0x0F);
					else if(j+offset == 31)				byte[j] = (LTC2990_temperature_sensors[i].upper_non_critical - LTC2990_temperature_sensors[i].lower_non_critical)/2;
					else if(j+offset == 32)				byte[j] = LTC2990_temperature_sensors[i].upper_non_critical;
					else if(j+offset == 33)				byte[j] = LTC2990_temperature_sensors[i].lower_non_critical;
					else if(j+offset == 36)				byte[j] = LTC2990_temperature_sensors[i].upper_non_rec;
					else if(j+offset == 37)				byte[j] = LTC2990_temperature_sensors[i].upper_critical;
					else if(j+offset == 38)				byte[j] = LTC2990_temperature_sensors[i].upper_non_critical;
					else if(j+offset == 39)				byte[j] = LTC2990_temperature_sensors[i].lower_non_rec;
					else if(j+offset == 40)				byte[j] = LTC2990_temperature_sensors[i].lower_critical;
					else if(j+offset == 41)				byte[j] = LTC2990_temperature_sensors[i].lower_non_critical;
					else if(j+offset >= max_sdr_size)	byte[j] = LTC2990_temperature_sensors[i].name[(j+offset)-max_sdr_size];
					else								byte[j] = sdr[j+offset];
				}
				return read_len;
			}
		}
		return 0x00;
	}

unsigned char set_LTC2990_temperature_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr){ 
	unsigned char i; 

	for(i = 0; i < LTC2990_temperature_CNT; i++){ 
		if(LTC2990_temperature_sensors[i].sensor_number == sensor_id){ 
			if (mask & 0x01) LTC2990_temperature_sensors[i].lower_non_critical = lnc;  
			if (mask & 0x02) LTC2990_temperature_sensors[i].lower_critical = lc;  
			if (mask & 0x04) LTC2990_temperature_sensors[i].lower_non_rec = lnr;  
			if (mask & 0x08) LTC2990_temperature_sensors[i].upper_non_critical = unc; 
			if (mask & 0x10) LTC2990_temperature_sensors[i].upper_critical = uc; 
			if (mask & 0x20) LTC2990_temperature_sensors[i].upper_non_rec = unr; 

			return 0x00;		 
		} 
	} 

	return 0xFF; 
} 

#endif 
