#ifndef FRU_INFO_H 
#define FRU_INFO_H 

/*********************************************
 * Common defines
 *********************************************/
#define LANG_CODE               ENGLISH 
#define FRU_FILE_ID             "CoreFRU" 

/*********************************************  
 * Board information area                       
 *********************************************/ 
#define BOARD_INFO_AREA_ENABLE
#define PRODUCT_INFO_AREA_ENABLE
#define MULTIRECORD_AREA_ENABLE

#define PRODUCT_MANUFACTURER           "CERN"
#define PRODUCT_NAME                   "CERN MMC Template"
#define PRODUCT_PN                     "0001"
#define PRODUCT_VERSION                "v.3.0"
#define PRODUCT_SN                     "0001"
#define PRODUCT_ASSET_TAG              "No asset tag"

/*********************************************  
 * Board information area                    
 *********************************************/ 
#define BOARD_MANUFACTURER             "CERN"
#define BOARD_NAME                     "CERN MMC Template"
#define BOARD_SN                       "0001"
#define BOARD_PN                       "0001"

/*********************************************  
 * AMC: Point to point connectivity record      
 *********************************************/ 
#define PORTCONF_CNT	                0
	
/********************************************** 
 * PICMG: Module current record 
 **********************************************/
#define MODULE_CURRENT_RECORD            current_in_ma(6000)    //      current_in_ma(max_current_in_mA) 

#endif 
