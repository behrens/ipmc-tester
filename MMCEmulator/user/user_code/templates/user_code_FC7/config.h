/*
 * config.h
 *
 * Created: 19/03/2015 18:20:14
 *  Author: jumendez
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_

#define FORCE_WRITE_FRU

/** ENABLE OEM COMMAND */
        /** No OEM commands for MP7 */

/** Product information */
        #define IPMI_MSG_MANU_ID_LSB 0x60   //NOTE: Manufacturer identification is handled by http://www.iana.org/assignments/enterprise-numbers
        #define IPMI_MSG_MANU_ID_B2  0x00       //CERN IANA ID = 0x000060
        #define IPMI_MSG_MANU_ID_MSB 0x00

        #define IPMI_MSG_PROD_ID_LSB 0x35
        #define IPMI_MSG_PROD_ID_MSB 0x12

        #define MMC_FW_REL_MAJ       1                  // major firmware release version ( < 128 )
        #define MMC_FW_REL_MIN       0                  // minor firmware release version ( < 256 )

        #define FRU_NAME                        "MP7/FC7"
        
/** CUSTOM ADDRESSES FOR BENCHTOP */
	#define CUSTOM_ADDR_LIST									\
		ADDR(0xF0, UNCONNECTED, UNCONNECTED, UNCONNECTED)		\
		ADDR(0xFF, POWERED, POWERED, POWERED)

/** PAYLOD CONFIGURATION */
	#ifdef AT32UC3A
		//Define power on sequence (Mandatory)
		#define POWER_ON_SEQ                                                                                            \
			SET_PAYLOAD_SIGNAL(PWR_GPIO_ENABLE)

		//Define power OFF sequence (Mandatory)
		#define POWER_OFF_SEQ                                                                                           \
			CLEAR_PAYLOAD_SIGNAL(PWR_GPIO_ENABLE)

		//Define reboot sequence (Optional)
		#define REBOOT_SEQ                                                                                                      \
			CLEAR_PAYLOAD_SIGNAL(PWR_GPIO_ENABLE)                                                 \
			DELAY(500)                                                                                                              \
			SET_PAYLOAD_SIGNAL(PWR_GPIO_ENABLE)
	#endif
	
	#ifdef ATMEGA128
		//Define power on sequence (Mandatory)
		#define POWER_ON_SEQ                                                                                            \
			SET_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)

		//Define power OFF sequence (Mandatory)
		#define POWER_OFF_SEQ                                                                                           \
			CLEAR_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)

		//Define reboot sequence (Optional)
		#define REBOOT_SEQ                                                                                                      \
			CLEAR_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)                                                 \
			DELAY(500)                                                                                                              \
			SET_PAYLOAD_SIGNAL(LOCAL_REG_ENABLE)
	#endif

	
#endif /* CONFIG_H_ */