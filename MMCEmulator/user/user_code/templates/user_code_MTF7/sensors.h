#ifndef SENSORS_H_
#define SENSORS_H_

/*********************************************
 * Sensor ID declaration 
 *********************************************/
#define USER_SENSOR_CNT		32

/*********************************************
 * Sensor implementation 
 *********************************************/

#define MTF7_ADC_Voltages \
   { \
	   { \
		   sensor_number: 2,               /* n is the sensor number (first id : 2)*/ \
		   init_time: MP_PRESENT,          /* When init function is called PP_PRESENT or MP_PRESENT (default) */ \
		   name: "+3.3V BkEnd",            /* Name printed */ \
		   Mux_ch:  1,                     /* Sensor specific parameter */ \
		   ADC_ch:  0,                     /* Sensor specific parameter */ \
		   p1: POINT(0,0),                 /* x1 = raw value, y1 = decimal value */ \
		   p2: POINT(140.25, 3.3),         /* POINT((unsigned char)(255/3.*3.3*1000/(1000+1000)),3.3), x2 = raw value, y2 = decimal value */ \
		   upper_non_rec: 4,               /* Upper non recovery threshold (decimal value) */ \
		   upper_critical: 4,              /* Upper critical threshold (decimal value)     */ \
		   upper_non_critical: 4,          /* Upper non critical threshold (decimal value) */ \
		   lower_non_critical: 0,          /* Lower non critical threshold (decimal value) */ \
		   lower_critical: 0,              /* Lower critical threshold (decimal value)     */ \
		   lower_non_rec: 0                /* Lower non recovery threshold (decimal value) */ \
	   }, \
	   { \
		   sensor_number: 3, \
		   init_time: MP_PRESENT, \
		   name: "+3.3VD", \
		   Mux_ch:  2, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(140.25, 3.3), \
		   upper_non_rec: 4, \
		   upper_critical: 4, \
		   upper_non_critical: 4, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0  \
	   }, \
	   { \
		   sensor_number: 4, \
		   init_time: MP_PRESENT, \
		   name: "+1.4VS", \
		   Mux_ch:  3, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1.), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 5, \
		   init_time: MP_PRESENT, \
		   name: "+1.2VS", \
		   Mux_ch:  4, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1.), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 6, \
		   init_time: MP_PRESENT, \
		   name: "+1.0_CORE_INT", \
		   Mux_ch:  5, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 7, \
		   init_time: MP_PRESENT, \
		   name: "+1.8V_MGTAUX", \
		   Mux_ch:  6, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 8, \
		   init_time: MP_PRESENT, \
		   name: "+1.8V_FLASH", \
		   Mux_ch:  7, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 9, \
		   init_time: MP_PRESENT, \
		   name: "+1.2_AVTT_1", \
		   Mux_ch:  8, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 10, \
		   init_time: MP_PRESENT, \
		   name: "+1.2_AVTT_2", \
		   Mux_ch:  9, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 11, \
		   init_time: MP_PRESENT, \
		   name: "+1.2_CNT_AVTT", \
		   Mux_ch:  10, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 12, \
		   init_time: MP_PRESENT, \
		   name: "+1.2V_CORE_CNT", \
		   Mux_ch:  11, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 13, \
		   init_time: MP_PRESENT, \
		   name: "+1.0_CORE_AVCC_1", \
		   Mux_ch:  12, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 14, \
		   init_time: MP_PRESENT, \
		   name: "+1.0_CORE_AVCC_2", \
		   Mux_ch:  13, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 15, \
		   init_time: MP_PRESENT, \
		   name: "+1.0_CNT_INT", \
		   Mux_ch:  14, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   }, \
	   { \
		   sensor_number: 16, \
		   init_time: MP_PRESENT, \
		   name: "+1.0_CNT_AVCC", \
		   Mux_ch:  15, \
		   ADC_ch:  0, \
		   p1: POINT(0,0), \
		   p2: POINT(85., 1), \
		   upper_non_rec: 2, \
		   upper_critical: 2, \
		   upper_non_critical: 2, \
		   lower_non_critical: 0, \
		   lower_critical: 0, \
		   lower_non_rec: 0 \
	   } \
   }


#define MTF7_ADC_Temperatures    			    \
{												\
	{											\
		sensor_number: 17,						\
		init_time: MP_PRESENT,					\
		name: "Hot Spot Temp",				    \
		Mux_ch: 0,							    \
		ADC_ch: 1,							    \
		p1: POINT(42.5,0.),						\
		p2: POINT(85.,50.),						\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 18,						\
		init_time: MP_PRESENT,					\
		name: "Amb. Temp",					    \
		Mux_ch: 1,							    \
		ADC_ch: 1,							    \
		p1: POINT(42.5,0.),						\
		p2: POINT(85.,50.),						\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	}											\
}

#define MTF7_ADC_Currents    			\
{												\
	{											\
		sensor_number: 19,						\
		init_time: MP_PRESENT,					\
		name: "+1.0_CORE_AVCC0I",			    \
		Mux_ch: 2,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 20,						\
		init_time: MP_PRESENT,					\
		name: "+1.0_CORE_AVCC1I",			    \
		Mux_ch: 3,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 21,						\
		init_time: MP_PRESENT,					\
		name: "+1.0_CORE_AVCC2I",			    \
		Mux_ch: 4,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 22,						\
		init_time: MP_PRESENT,					\
		name: "+1.0_CORE_AVCC3I",			    \
		Mux_ch: 5,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 23,						\
		init_time: MP_PRESENT,					\
		name: "+1.0_CNT_INT_I",				    \
		Mux_ch: 6,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 24,						\
		init_time: MP_PRESENT,					\
		name: "+1.0_CNT_AVCC_I",			    \
		Mux_ch: 7,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 25,						\
		init_time: MP_PRESENT,					\
		name: "+1.2_CORE_AVTT0I",			    \
		Mux_ch: 8,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 26,						\
		init_time: MP_PRESENT,					\
		name: "+1.2_CORE_AVTT1I",			    \
		Mux_ch: 9,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 27,						\
		init_time: MP_PRESENT,					\
		name: "+1.2_CORE_AVTT2I",			    \
		Mux_ch: 10,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 28,						\
		init_time: MP_PRESENT,					\
		name: "+1.2_CORE_AVTT3I",			    \
		Mux_ch: 11,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 29,						\
		init_time: MP_PRESENT,					\
		name: "+1.2_CNT_AVTT_I",			    \
		Mux_ch: 12,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 30,						\
		init_time: MP_PRESENT,					\
		name: "+1.2_CORE_CNT_I",			    \
		Mux_ch: 13,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 31,						\
		init_time: MP_PRESENT,					\
		name: "+1.8_MGT_AUX_I",				    \
		Mux_ch: 14,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: 32,						\
		init_time: MP_PRESENT,					\
		name: "+1.8_FLASH_I",				    \
		Mux_ch: 15,							    \
		ADC_ch: 1,							    \
		p1: POINT(0,0),							\
		p2: POINT(0x6d,0.654),					\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	}											\
}

#endif 
