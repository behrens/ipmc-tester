/*
 * sensors.c
 *
 * Created: 27/03/2015 12:37:17
 *  Author: jumendez
 */ 
#include "../../drivers/drivers.h"
#include "../../application/inc/sdr.h"

#include "sensors.h"

float RTMCurrentSensorRead(){
	float Result;
	unsigned char bytes[2];
	
	ext_i2c_received(0x24, 0xA0, BYTE_LEN, 2, bytes);

	Result = (float)(0x3ff & (((bytes[0] << 8) + bytes[1]) >> 2)) * 3 / 1024;   // VB: This is when the converter is 10 bits (7997).
	
	return (Result < 2.5) ? 2.5 : Result;
}


unsigned char tempSensorRead(unsigned char sensor){
	unsigned char stemp;     //sensor temperature
	
	if(sensor == RTM_TEMPERATURE1)				ext_i2c_received(0x18, 0x00, ZERO_LEN, 1, &stemp);
	else if(sensor == RTM_TEMPERATURE2)			ext_i2c_received(0x4c, 0x00, ZERO_LEN, 1, &stemp);
	
	return stemp;
}

//Sensor initialization
void sensor_init_user(){
}

//Sensor are polled (Every 100 ms)
void sensor_monitoring_user(){	
	set_sensor_value(RTM_CURRENT_SENSOR, (unsigned char)(250*(RTMCurrentSensorRead() - 2.5)));
	set_sensor_value(RTM_TEMPERATURE1,tempSensorRead(RTM_TEMPERATURE1));
	set_sensor_value(RTM_TEMPERATURE2,tempSensorRead(RTM_TEMPERATURE2));
}

void user_sensor_func(unsigned char sens_num){
	return;
}