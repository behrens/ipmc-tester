//*****************************************************************************
// File Name    : user_code.c
//
// Author		: Markus Joos (markus.joos@cern.ch)
// Modified by	: Julian Mendez <julian.mendez@cern.ch>
//
// Description	: Implementation of use commands:
//					-	OEM function: mandatory
//					-	Controller specific function: optionnal, enabled with 
//						ENABLE_CONTROLLER_SPECIFIC in user_code.h
//*****************************************************************************

// Header file
#include "../../application/inc/ipmi_if.h"	//Used for error code definitions
#include "../../application/inc/payload.h"	//Used for user main function (payload activation/deactivation)
#include "../../drivers/drivers.h"

#include "config.h"

/** User main function */
unsigned char user_main(unsigned char addr){
		
	/*	
		manage_payload() function check the handle switch status and execute POWER_ON_SEQ on assertion
		and POWER_OFF_SEQ on de-assertion.
	*/
	
	while(1){
		manage_payload();		
	}
	return 0x01;
}

/** User init function */
void user_init(){
	/** Init of the IO extender */
	
	set_signal_dir(0x02,0x00);
	set_signal_dir(0x01,0x00);
	
	set_signal(0x02);	//Set pin PA2
	clear_signal(0x01);	//Clear pin PA1
	
	set_dir_port(0x20, 0, OUTPUT);
	set_dir_port(0x20, 1, OUTPUT);
	set_dir_port(0x20, 2, OUTPUT);
	set_dir_port(0x20, 3, OUTPUT);
	
	set_dir_port(0x21, 0, INPUT);
	set_dir_port(0x21, 1, INPUT);
	set_dir_port(0x21, 2, OUTPUT);
	set_dir_port(0x21, 3, INPUT);
	
	set_PCA9698_port(0x20, 0, 0x00);	//Modules presents
	set_PCA9698_port(0x20, 1, 0x00);
}

/** IPMI OEM commands */
	
unsigned char ipmi_oem_user(unsigned char command, unsigned char *iana, unsigned char *user_data, unsigned char data_size, unsigned char *buf, unsigned char *error){
		//Note: You must not return more than "MAX_BYTES_READ" bytes defined in project.h file

		//Check if this OEM command is supported by us.
		//The originator of the OEM command (see table 5.1 in the IPMI 1.5 spec will send a 3-byte IANA ID
		//If we recognise this ID we respond to the command. Otherwise we return an error code
	
		unsigned char reply_len = 0;
		
		*error = IPMI_CC_OK;

		if (iana[0] != IPMI_MSG_MANU_ID_MSB || iana[1] != IPMI_MSG_MANU_ID_B2 || iana[2] != IPMI_MSG_MANU_ID_LSB)
		{
			*error = IPMI_CC_PARAM_OUT_OF_RANGE;
			return(0);             //Not for us
		}

		buf[reply_len++] = IPMI_MSG_MANU_ID_MSB;   //We have to return our IANA ID as the first 3 bytes of the reply
		buf[reply_len++] = IPMI_MSG_MANU_ID_B2;
		buf[reply_len++] = IPMI_MSG_MANU_ID_LSB;

		switch (command){
			
			case 0x01:	/* switch PS1 : ON/OFF (cmd: 0x01, data: [0:CHANNEL, 1:(0/OFF 1/ON)])*/
						if(data_size < 2 || user_data[0] > 16 || user_data[0] == 0 || user_data[1] > 1){
							*error = IPMI_CC_PARAM_OUT_OF_RANGE;
							return (0);
						}
						
						if(user_data[0] < 9){
							if(user_data[1])		set_PCA9698_bit(0x20, 0, user_data[0]-1);
							else					clear_PCA9698_bit(0x20, 0, user_data[0]-1);
						}else{
							if(user_data[1])		set_PCA9698_bit(0x20, 1, user_data[0]-9);
							else					clear_PCA9698_bit(0x20, 1, user_data[0]-9);							
						}
						break;
						
			case 0x02:	/* switch RELAYS : ON/OFF (cmd: 0x02, data: [0:CHANNEL, 1:(0/OFF 1/ON)])*/
						if(data_size < 2 || user_data[0] > 16 || user_data[0] == 0 || user_data[1] > 1){
							*error = IPMI_CC_PARAM_OUT_OF_RANGE;
							return (0);
						}
						
						if(user_data[0] < 9){
							if(user_data[1])		set_PCA9698_bit(0x20, 2, user_data[0]-1);
							else					clear_PCA9698_bit(0x20, 2, user_data[0]-1);
						}else{
							if(user_data[1])		set_PCA9698_bit(0x20, 3, user_data[0]-9);
							else					clear_PCA9698_bit(0x20, 3, user_data[0]-9);
						}
						break;
						
			 case 0x03:	/* get ENABLE : ON/OFF (cmd: 0x03, data: [0:CHANNEL])*/
						if(data_size < 1 || user_data[0] > 16 || user_data[0] == 0){
							*error = IPMI_CC_PARAM_OUT_OF_RANGE;
							return (0);
						}
						
						if(user_data[0] < 9){
							buf[reply_len++] = get_PCA9698_bit(0x21, 0, user_data[0]-1);
						}else{
							buf[reply_len++] = get_PCA9698_bit(0x21, 1, user_data[0]-9);
						}
						break;
						
			 case 0x04:	/* switch PM_PS : ON/OFF (cmd: 0x04, data: [0:(0/GND 1/UNCONNECTED)])*/
						if(data_size < 1 || user_data[0] > 16 || user_data[0] == 0){
							*error = IPMI_CC_PARAM_OUT_OF_RANGE;
							return (0);
						}
						
						if(user_data[0] == 0x00){
							set_PCA9698_bit(0x21, 2, 0);
						}else{
							clear_PCA9698_bit(0x21, 2, 0);
						}
						break;
						
			 case 0x05:	/* switch PM_RSTN : ON/OFF (cmd: 0x05, data: [0:(0/RST 1/ON)])*/
						if(data_size < 1 || user_data[0] > 16 || user_data[0] == 0){
							*error = IPMI_CC_PARAM_OUT_OF_RANGE;
							return (0);
						}
						
						if(user_data[0] == 0x00){
							clear_PCA9698_bit(0x21, 2, 1);
						}else{
							set_PCA9698_bit(0x21, 2, 1);
						}
						break;
						
			 case 0x06:	/* switch POWER_ON_M1 : GND/VCC (cmd: 0x06, data: [0:(0/GND 1/VCC)])*/
						if(data_size < 1 || user_data[0] > 16 || user_data[0] == 0){
							*error = IPMI_CC_PARAM_OUT_OF_RANGE;
							return (0);
						}
						
						if(user_data[0] == 0x00){
							set_PCA9698_bit(0x21, 2, 2);
						}else{
							clear_PCA9698_bit(0x21, 2, 2);
						}
						break;
						
			 case 0x07:	/* switch POWER_ON_M2 : GND/VCC (cmd: 0x07, data: [0:(0/GND 1/VCC)])*/
						if(data_size < 1 || user_data[0] > 16 || user_data[0] == 0){
							*error = IPMI_CC_PARAM_OUT_OF_RANGE;
							return (0);
						}
						
						if(user_data[0] == 0x00){
							set_PCA9698_bit(0x21, 2, 3);
						}else{
							clear_PCA9698_bit(0x21, 2, 3);
						}
						break;
						
			 case 0x08:	/* switch PMP_A# : GND/UC (cmd: 0x08, data: [0:(0/GND 1/UNCONNECTED)])*/
						if(data_size < 1 || user_data[0] > 16 || user_data[0] == 0){
							*error = IPMI_CC_PARAM_OUT_OF_RANGE;
							return (0);
						}
						
						if(user_data[0] == 0x00){
							clear_PCA9698_bit(0x21, 2, 4);
						}else{
							set_PCA9698_bit(0x21, 2, 4);
						}
						break;
						
			 case 0x09:	/* switch PMP_B# : GND/UC (cmd: 0x08, data: [0:(0/GND 1/UNCONNECTED)])*/
						if(data_size < 1 || user_data[0] > 16 || user_data[0] == 0){
							*error = IPMI_CC_PARAM_OUT_OF_RANGE;
							return (0);
						}
						
						if(user_data[0] == 0x00){
							clear_PCA9698_bit(0x21, 2, 5);
						}else{
							set_PCA9698_bit(0x21, 2, 5);
						}
						break;
						
			case 0x0A:	/* switch PMP_C# : GND/UC (cmd: 0x0A, data: [0:(0/GND 1/UNCONNECTED)])*/
						if(data_size < 1 || user_data[0] > 16 || user_data[0] == 0){
							*error = IPMI_CC_PARAM_OUT_OF_RANGE;
							return (0);
						}
						
						if(user_data[0] == 0x00){
							clear_PCA9698_bit(0x21, 2, 6);
						}else{
							set_PCA9698_bit(0x21, 2, 6);
						}
						break;
						
			case 0x0B:	/* get PM_OKN (cmd: 0x0B)*/						
						buf[reply_len++] = get_PCA9698_bit(0x21, 3, 0);						
						break;
						
			case 0x0C:	/* get RST_PM_A# (cmd: 0x0C)*/						
						buf[reply_len++] = get_PCA9698_bit(0x21, 3, 1);						
						break;
						
			case 0x0D:	/* get RST_PM_B# (cmd: 0x0C)*/
						buf[reply_len++] = get_PCA9698_bit(0x21, 3, 2);
						break;
						
			case 0x0E:	/* get MPMUX_I (cmd: 0x0E)*/
						buf[reply_len++] = get_PCA9698_bit(0x21, 3, 4);
						break;
						
			case 0x0F:	/* get PPMUX_I (cmd: 0x0F)*/
						buf[reply_len++] = get_PCA9698_bit(0x21, 3, 5);
						break;
		}
		//Check if we are supporting the command
		//if (command == 0x01)       //supported
		//...do something
		//else if (command == 0x02)  //not supported
		//{
		//    *error = IPMI_CC_INV_CMD;
		//    return(0);
		//}

		//Put here the additional data you want to return

		return(reply_len);  //4 because we are returning 4 bytes.
}
	
	
/** IPMI Controller Specific commands */
	/*
	unsigned char ipmi_controller_specific(unsigned char cmd, unsigned char *data, unsigned char data_len, unsigned char *buf, unsigned char *err){
		unsigned char rsp_length = 0;
	
		*error = IPMI_CC_OK;
	
		switch(command){
			case FRU_PROM_REVISION_CMD:	
				// ipmi_prom_version_change(user_data[0]);
				rsp_length = 0;
				break;
			
			case JTAG_CTRL_SET_CMD:	
				// ipmi_jtag_ctrl(rqs.data[0]);
				rsp_length = 0;
				break;
			
			case FPGA_JTAG_PLR_CMD:	
				// ipmi_fpga_jtag_plr_set(rqs.data[0]);
				rsp_length = 0;
				break;
			
			default:
				*error = IPMI_CC_INV_CMD;
		}
	
		return rsp_length;
	}
	*/