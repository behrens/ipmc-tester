/*
 * sensors.c
 *
 * Created: 27/03/2015 12:37:17
 *  Author: jumendez
 */ 
#include <avr/io.h>

#include "../sdr.h"
#include "../a2d.h"
#include "../i2csw.h"
#include "../iodeclaration.h"

#include "../avrlibdefs.h"
#include "../avrlibtypes.h"

#include "sensors.h"

//Sensor initialization
void sensor_init_user(){	
	u08 cmd=0;
	
	cmd = 0x20;		// configure register for :  [continuous conversion] [unsigned temperature  (0 to 255C)]
	i2cswSend(0x19, CONFIG_REGISTER, 1, 1, &cmd);

	cmd = 0x05;		// configure control register [activate filter on the diode]
	i2cswSend(0x19, CONTROL_REGISTER, 1, 1, &cmd);

	cmd = 0x05;		// configure   [for mesure on procesor]
	i2cswSend(0x19, REMOTE_DIODE_MODEL, 1, 1, &cmd);

	cmd = 0x77;		// configure   [mode enable]
	i2cswSend(0x19, REMOTE_MODE_CONTROL, 1, 1, &cmd);
}


u08 tempSensorRead(u08 sensID){
	u08 stemp;     //sensor temperature

	CRITICAL_SECTION_START;
	if(sensID == LOCAL_TEMP_SENSOR)		i2cswReceive(ADD_SENSOR, LOCAL_SENSOR, 1, 1, &stemp);
	else if(sensID == TEMP1_SENSOR)		i2cswReceive(ADD_SENSOR, REMOTE_1, 1, 1, &stemp);
	else								stemp = 0xff;
	CRITICAL_SECTION_END;

	return stemp;
}

//Sensor are polled (Every 100 ms)
void sensor_monitoring_user(){ 
	set_sensor_value(LOCAL_TEMP_SENSOR,tempSensorRead(LOCAL_TEMP_SENSOR));
	set_sensor_value(TEMP1_SENSOR,tempSensorRead(TEMP1_SENSOR));
}