#ifndef FRU_INFO_H
#define FRU_INFO_H

#include "../fru.h"
#include "../languages.h"

/*********************************************
 * Common defines
 *********************************************/
#define LANG_CODE		ENGLISH
#define FRU_FILE_ID		"CoreFRU"	//Allows knowing the source of the FRU present in the memory


#define BOARD_INFO_AREA_ENABLE
#define PRODUCT_INFO_AREA_ENABLE
#define MULTIRECORD_AREA_ENABLE


/*********************************************
 * Board information area
 *********************************************/
#define BOARD_MANUFACTURER		"CPPM"
#define	BOARD_NAME				"AMC40"
#define BOARD_SN				"2.0"
#define BOARD_PN				""
 
/*********************************************
 * Product information area
 *********************************************/
#define PRODUCT_MANUFACTURER	"CPPM"
#define PRODUCT_NAME			"LHCb DAQ"
#define PRODUCT_PN				"1.0"
#define PRODUCT_VERSION			"2.0"
#define PRODUCT_SN				"1"
#define PRODUCT_ASSET_TAG		""
 
/*********************************************
 * AMC: Point to point connectivity record
 *********************************************/
/** Should be defines 

#define LIGHT_AMC_POINT_TO_POINT_RECORD_CNT		1
#define LIGHT_AMC_POINT_TO_POINT_RECORD_LIST															\
	LIGHT_AMC_MODULE_POINT_TO_POINT_RECORD(0, PORT(4), PCIE, NO_EXTENSION, EXACT_MATCHES)
*/

/*********************************************
 * AMC: Point to point clock record
 *********************************************/
/** Should be defined

//#define AMC_POINT_TO_POINT_CLOCK_CNT			1
//#define AMC_POINT_TO_POINT_CLOCK_LIST																	\
//	DIRECT_CLOCK_CONNECTION(0, 2, 0, 1, 0, 0, 0, 40000000, 38000000, 42000000)
*/

/**********************************************
 * PICMG: Module current record
 **********************************************/
#define MODULE_CURRENT_RECORD		 current_in_ma(6500)	//	current_in_ma(max_current_in_mA)
	
#endif