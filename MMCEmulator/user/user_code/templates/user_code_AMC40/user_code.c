//*****************************************************************************
// File Name    : user_code.c
//
// Author  : Julian Mendez (julian.mendez@cern.ch)
//
//*****************************************************************************


// Header file
	#include <avr/io.h>
	#include <avr/wdt.h>
	
	#include "../iodeclaration.h"
	#include "../ipmi_if.h"	//Used for error code definitions
	#include "../payload.h"	//Used for user main function (payload activation/deactivation)
	#include "../timer.h"
	
	#include "../avrlibdefs.h"
	#include "../avrlibtypes.h"
	
	#include "config.h"

//OEM commands declaration
	#define JTAG_CTRL_SET_CMD     1
	#define LOCAL_RESET_FPGA_CMD  2
	#define LOCAL_RELOAD_FGPA_CMD 3
	
//Function used to control the JTAG switch via controller specific IPMI command
//*******************************/
unsigned char ipmi_jtag_ctrl(u08 ctrl_type)   //Called from ipmi_if.c
//*******************************/
{	
    if (ctrl_type == 0){		//Front connector
		CLEAR_SIGNAL(GPIO_9);
		CLEAR_SIGNAL(GPIO_12);
	}else if(ctrl_type == 1){	//AMC connector
		CLEAR_SIGNAL(GPIO_9);
		SET_SIGNAL(GPIO_12);
	}else
		return 0xFF;
		
	return 0x00;
}

u08 ipmi_oem_user(u08 cmd, u08 *iana, u08 *data, u08 data_len, u08 *buf, u08 *err){
	u08 rsp_length = 0;
	
	*err = IPMI_CC_OK;
	
	if (iana[0] != 0x00 || iana[1] != 0xA1 || iana[2] != 0x2E){
		*err = IPMI_CC_PARAM_OUT_OF_RANGE;
		return 0;             //Wrong IANA code
	}

	buf[0] = 0x00;
	buf[1] = 0xA1;   //IANA ID must be returned as the first 3 bytes
	buf[2] = 0x2E;

	switch(cmd){		
		case JTAG_CTRL_SET_CMD:
			if(data_len != 1){
				*err = IPMI_CC_PARAM_OUT_OF_RANGE;
				return 0;
			}
			if(ipmi_jtag_ctrl(data[0])){
				*err = IPMI_CC_INV_CMD;
				return 0;	
			}
			rsp_length = 3;			
			break;
			
		case LOCAL_RESET_FPGA_CMD:
			if(data_len != 1){
				*err = IPMI_CC_PARAM_OUT_OF_RANGE;
				return 0;
			}
			CLEAR_SIGNAL(LOCAL_RESET_FPGA);
			DELAY(((unsigned)data[0])*10);
			SET_SIGNAL(LOCAL_RESET_FPGA);
			rsp_length = 3;
			break;
			
		case LOCAL_RELOAD_FGPA_CMD:
			if(data_len != 1){
				*err = IPMI_CC_PARAM_OUT_OF_RANGE;
				return 0;
			}
			CLEAR_SIGNAL(LOCAL_RELOAD_FPGA);
			DELAY(((unsigned)data[0])*10);
			SET_SIGNAL(LOCAL_RELOAD_FPGA);
			rsp_length = 3;
			break;
			
		default:
			*err = IPMI_CC_INV_CMD;
	}
	return rsp_length;
}

//*************************************************/
// User main function for bench top use
u08 user_main(u08 addr){
	
	while(1){
		manage_payload();		
	}
	
	return 1;
}