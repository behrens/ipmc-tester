#ifndef FRU_INFO_H
#define FRU_INFO_H

//#include "../fru.h"
//#include "../languages.h"

/*********************************************
 * Common defines
 *********************************************/
#define LANG_CODE               ENGLISH
#define FRU_FILE_ID             "CoreFRU"       //Allows knowing the source of the FRU present in the memory

#define BOARD_INFO_AREA_ENABLE
#define PRODUCT_INFO_AREA_ENABLE
#define MULTIRECORD_AREA_ENABLE

/*********************************************
 * Board information area
 *********************************************/
#define BOARD_MANUFACTURER              "CERN"
#define BOARD_NAME                      "IPMCTestPad"
#define BOARD_SN                        "000000000"
#define BOARD_PN                        "IPMC-TP"
 
/*********************************************
 * Product information area
 *********************************************/
#define PRODUCT_MANUFACTURER			"CERN"
#define PRODUCT_NAME                    "IPMCTestPad"
#define PRODUCT_PN                      "R1"
#define PRODUCT_VERSION                 "CERN-v0"
#define PRODUCT_SN                      "00000"
#define PRODUCT_ASSET_TAG               "No tag"
 
/*********************************************
 * AMC: Point to point connectivity record
 *********************************************/

/*********************************************
 * AMC: Point to point clock record
 *********************************************/

/**********************************************
 * PICMG: Module current record
 **********************************************/
#define MODULE_CURRENT_RECORD            current_in_ma(5000)    //      current_in_ma(max_current_in_mA)
        
#endif