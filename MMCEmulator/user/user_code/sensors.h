/*
 * sensors.h
 *
 * Created: 26/03/2015 22:22:20
 *  Author: jumendez
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_

#define USER_SENSOR_CNT		0

/*
//Sensors ID
#define TEMPERATURE_SENSOR  2	//First user sensor id: 2 (0: Hotswap, 1: 12V)

#define USER_SENSOR_CNT		1

#define LTC2990_voltage 					\
{											\
	{										\
		sensor_number: 2,	\
		init_time: MP_PRESENT,				\
		name: "FPGA-Temp",					\
		i2c_addr: 0x4c,						\
		p1: POINT(1, 0.5),				\
		p2: POINT(100, 50),				\
		upper_non_rec: 90,					\
		upper_critical: 70,					\
		upper_non_critical: 50,				\
		lower_non_critical: 10,				\
		lower_critical: 5,					\
		lower_non_rec: 0,					\
	}										\
}
*/

#endif /* SENSORS_H_ */