--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               CERN (PH-ESE-BE)                                                         
-- Engineer:              Julian Mendez (julian.mendez@cern.ch)
--                                                                                                 
-- Project Name:          CERN-IPMC TestPad                                                  
-- Module Name:           Altera Max V - Routing CPLD
--                                                                                                 
-- Language:              VHDL'93                                                                  
--                                                                                                   
-- Target Device:         Altera Max V                                                        
-- Tool version:          Quartus II 16.1                                                               
--                                                                                                   
-- Version:               1.0                                                                     
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        15/02/2017   1.0       J. Mendez         First .vhd module definition 
--                    
--                                                             
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity IPMCTestPad is
   port ( 

		--==============--
		-- Config bus   --
		--==============--
		CONF_CLK												: in  std_logic;
		CONF_DATA											: in  std_logic_vector(7 downto 0);
		CONF_WR							   				: in  std_logic;
		CONF_RD											   : out std_logic;
		
		CPLD_RDATA											: out std_logic;
		
		--==============--
		-- AMCs IPMC    --
		--==============--
		IPMC_PS1												: out std_logic_vector(8 downto 0);
		IPMC_ENABLE											: in  std_logic_vector(8 downto 0);
		IPMC_IPMBL_ENABLE									: in  std_logic_vector(8 downto 0);
		IPMC_MP_ENABLE										: in  std_logic_vector(8 downto 0);
		IPMC_PWR_ENABLE									: in  std_logic_vector(8 downto 0);
		IPMC_MP_GOOD										: out std_logic_vector(8 downto 0);
		IPMC_PWR_GOOD										: out std_logic_vector(8 downto 0);
		IPMC_MP_FAULT										: out std_logic_vector(8 downto 0);
		IPMC_PWR_FAULT										: out std_logic_vector(8 downto 0);
		--IPMC_PWR_ORING_0									: out std_logic_vector(8 downto 0);	-- Not connected
		
		--==============--
		-- AMC MMC      --
		--==============--
		AMC_PS1												: in  std_logic;
		AMC_ENABLE											: out std_logic;
		AMC_IPMBL_ENABLE									: out std_logic;
		AMC_MP_ENABLE										: out std_logic;
		AMC_PWR_ENABLE										: out std_logic;
		AMC_MP_GOOD											: in  std_logic;
		AMC_PWR_GOOD										: in  std_logic;
		--AMC_PWR_ORING										: in  std_logic;							-- Not connected
		
		AMC_GA												: out std_logic_vector(2 downto 0);

		--==============--
		-- GPIOs			 --
		--==============--	
		LAPP_IO												: inout std_logic_vector(34 downto 0);
		LAPP_IPM_IO											: inout std_logic_vector(15 downto 0)
		        
   );
end IPMCTestPad;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture structural of IPMCTestPad is 

	signal virtual_reset:	std_logic;
	signal amc_selected: 	std_logic_vector(8 downto 0);
		
	signal amc_wr_done:		std_logic := '0';
	signal io_cmd_done:		std_logic := '0';
	 
	 
begin
	 
	virtual_reset <= CONF_DATA(7) and CONF_DATA(6) and CONF_DATA(5) and CONF_DATA(4) and CONF_DATA(3) and CONF_DATA(2) and CONF_DATA(1) and CONF_DATA(0);
	CONF_RD <= amc_wr_done or io_cmd_done;
	
	amc_sel_proc: process(CONF_CLK, virtual_reset)
	begin
	
		if virtual_reset = '1' then
			amc_selected <= "000000000";
			amc_wr_done <= '0';
			
		elsif rising_edge(CONF_CLK) then
		
			 amc_wr_done <= '0';
			 
			 if CONF_WR = '1' and CONF_DATA(7 downto 4) = "1100" then	-- Select AMC
			 
				 amc_selected(0) <= not(CONF_DATA(3)) and not(CONF_DATA(2)) and not(CONF_DATA(1)) and not(CONF_DATA(0));
				 amc_selected(1) <= not(CONF_DATA(3)) and not(CONF_DATA(2)) and not(CONF_DATA(1)) and CONF_DATA(0);
				 amc_selected(2) <= not(CONF_DATA(3)) and not(CONF_DATA(2)) and CONF_DATA(1) and not(CONF_DATA(0));
				 amc_selected(3) <= not(CONF_DATA(3)) and not(CONF_DATA(2)) and CONF_DATA(1) and CONF_DATA(0);
				 amc_selected(4) <= not(CONF_DATA(3)) and CONF_DATA(2) and not(CONF_DATA(1)) and not(CONF_DATA(0));
				 amc_selected(5) <= not(CONF_DATA(3)) and CONF_DATA(2) and not(CONF_DATA(1)) and CONF_DATA(0);
				 amc_selected(6) <= not(CONF_DATA(3)) and CONF_DATA(2) and CONF_DATA(1) and not(CONF_DATA(0));
				 amc_selected(7) <= not(CONF_DATA(3)) and CONF_DATA(2) and CONF_DATA(1) and CONF_DATA(0);
				 amc_selected(8) <= CONF_DATA(3) and not(CONF_DATA(2)) and not(CONF_DATA(1)) and not(CONF_DATA(0));
				 				 
				 amc_wr_done <= '1';
				 
			 elsif CONF_WR = '1' and CONF_DATA(7 downto 4) = "1101" then
			 
				 amc_wr_done <= '1';
				 
				 if CONF_DATA(3 downto 0) = "0000" then			-- AMC site 1	(0x72)	 
					 AMC_GA(2) <= '0';
					 AMC_GA(1) <= '0';
					 AMC_GA(0) <= 'Z';
					 
				 elsif CONF_DATA(3 downto 0) = "0001" then		-- AMC site 2	(0x74)			 
					 AMC_GA(2) <= '0';
					 AMC_GA(1) <= 'Z';
					 AMC_GA(0) <= '0';
					 
				 elsif CONF_DATA(3 downto 0) = "0010" then		-- AMC site 3 (0x76)		 
					 AMC_GA(2) <= '0';
					 AMC_GA(1) <= 'Z';
					 AMC_GA(0) <= 'Z';
					 
				 elsif CONF_DATA(3 downto 0) = "0011" then		-- AMC site 4 (0x78)		 
					 AMC_GA(2) <= 'Z';
					 AMC_GA(1) <= '0';
					 AMC_GA(0) <= '0';
					 
				 elsif CONF_DATA(3 downto 0) = "0100" then		-- AMC site 5 (0x7A)			 
					 AMC_GA(2) <= 'Z';
					 AMC_GA(1) <= '0';
					 AMC_GA(0) <= 'Z';
					 
				 elsif CONF_DATA(3 downto 0) = "0101" then		-- AMC site 6 (0x7c)
					 AMC_GA(2) <= 'Z';
					 AMC_GA(1) <= 'Z';
					 AMC_GA(0) <= '0';
					 
				 elsif CONF_DATA(3 downto 0) = "0110" then		-- AMC site 7 (0x7e) 
					 AMC_GA(2) <= 'Z';
					 AMC_GA(1) <= 'Z';
					 AMC_GA(0) <= '1';
					 
				 elsif CONF_DATA(3 downto 0) = "0111" then		-- AMC site 8 (0x80)		 
					 AMC_GA(2) <= 'Z';
					 AMC_GA(1) <= '1';
					 AMC_GA(0) <= 'Z';
					 
				 elsif CONF_DATA(3 downto 0) = "1000" then		-- Specific address (used to set MMC in iRTM mode)	
					 AMC_GA(2) <= '1';
					 AMC_GA(1) <= '1';
					 AMC_GA(0) <= '0';
					 
				 else															-- Unconnected
					 AMC_GA(2) <= '0';
					 AMC_GA(1) <= '1';
					 AMC_GA(0) <= '1';
				 
				 end if;
			 				 
			 end if;
			 
		end if;
	
	end process;
	
	amc_routing_gen : for i in 0 to 8 generate
		
		IPMC_PS1(i)				<= AMC_PS1 when amc_selected(i) = '1' else '1';
		IPMC_MP_GOOD(i)		<=	AMC_MP_GOOD when amc_selected(i) = '1' else '0';
		IPMC_PWR_GOOD(i)		<=	AMC_PWR_GOOD when amc_selected(i) = '1' else '0';
		IPMC_MP_FAULT(i)		<=	not(AMC_MP_GOOD) when amc_selected(i) = '1' else '0';
		IPMC_PWR_FAULT(i)		<=	not(AMC_PWR_GOOD) when amc_selected(i) = '1' else '0';
		--IPMC_PWR_ORING_0(i)	<=	AMC_PWR_ORING and amc_selected(i);
		
	end generate;
		
	AMC_ENABLE				<=	not(IPMC_ENABLE(0)) when amc_selected <= "000000001" else
									not(IPMC_ENABLE(1)) when amc_selected <= "000000010" else
									not(IPMC_ENABLE(2)) when amc_selected <= "000000100" else
									not(IPMC_ENABLE(3)) when amc_selected <= "000001000" else
									not(IPMC_ENABLE(4)) when amc_selected <= "000010000" else
									not(IPMC_ENABLE(5)) when amc_selected <= "000100000" else
									not(IPMC_ENABLE(6)) when amc_selected <= "001000000" else
									not(IPMC_ENABLE(7)) when amc_selected <= "010000000" else
									not(IPMC_ENABLE(8)) when amc_selected <= "100000000" else
									'0';
									
	AMC_IPMBL_ENABLE		<= IPMC_IPMBL_ENABLE(0) when amc_selected <= "000000001" else
									IPMC_IPMBL_ENABLE(1) when amc_selected <= "000000010" else
									IPMC_IPMBL_ENABLE(2) when amc_selected <= "000000100" else
									IPMC_IPMBL_ENABLE(3) when amc_selected <= "000001000" else
									IPMC_IPMBL_ENABLE(4) when amc_selected <= "000010000" else
									IPMC_IPMBL_ENABLE(5) when amc_selected <= "000100000" else
									IPMC_IPMBL_ENABLE(6) when amc_selected <= "001000000" else
									IPMC_IPMBL_ENABLE(7) when amc_selected <= "010000000" else
									IPMC_IPMBL_ENABLE(8) when amc_selected <= "100000000" else
									'0';
									
	AMC_MP_ENABLE			<= IPMC_MP_ENABLE(0) when amc_selected <= "000000001" else
									IPMC_MP_ENABLE(1) when amc_selected <= "000000010" else
									IPMC_MP_ENABLE(2) when amc_selected <= "000000100" else
									IPMC_MP_ENABLE(3) when amc_selected <= "000001000" else
									IPMC_MP_ENABLE(4) when amc_selected <= "000010000" else
									IPMC_MP_ENABLE(5) when amc_selected <= "000100000" else
									IPMC_MP_ENABLE(6) when amc_selected <= "001000000" else
									IPMC_MP_ENABLE(7) when amc_selected <= "010000000" else
									IPMC_MP_ENABLE(8) when amc_selected <= "100000000" else
									'0';
									
	AMC_PWR_ENABLE			<= IPMC_PWR_ENABLE(0) when amc_selected <= "000000001" else
									IPMC_PWR_ENABLE(1) when amc_selected <= "000000010" else
									IPMC_PWR_ENABLE(2) when amc_selected <= "000000100" else
									IPMC_PWR_ENABLE(3) when amc_selected <= "000001000" else
									IPMC_PWR_ENABLE(4) when amc_selected <= "000010000" else
									IPMC_PWR_ENABLE(5) when amc_selected <= "000100000" else
									IPMC_PWR_ENABLE(6) when amc_selected <= "001000000" else
									IPMC_PWR_ENABLE(7) when amc_selected <= "010000000" else
									IPMC_PWR_ENABLE(8) when amc_selected <= "100000000" else
									'0';

	-- GPIO Process
	pin_setget_proc: process(CONF_CLK, virtual_reset)
	
		variable select_pin: integer := 0;
		
	begin
	
		if virtual_reset = '1' then
			LAPP_IO	<= (others => 'Z');
			LAPP_IPM_IO <= (others => 'Z');
			io_cmd_done <= '0';
			
		elsif rising_edge(CONF_CLK) then
			
			io_cmd_done <= '0';
			
			if CONF_WR = '1' and CONF_DATA(7) = '0' then	-- Set or Clear pin
				
				select_pin := to_integer(unsigned(CONF_DATA(5 downto 0)));
				
				if	(select_pin < 35) then
					LAPP_IO(select_pin) <= CONF_DATA(6);
					
				elsif (select_pin < (35+16)) then
					LAPP_IPM_IO(select_pin-35) <= CONF_DATA(6);
				
				end if;
				
				io_cmd_done <= '1';
				
			elsif CONF_WR = '1' and CONF_DATA(7 downto 6) = "10" then	-- Get pin
			
				select_pin := to_integer(unsigned(CONF_DATA(5 downto 0)));
				
				if	(select_pin < 35) then
					LAPP_IO(select_pin)    <= 'Z';
					CPLD_RDATA             <= LAPP_IO(select_pin);
					
				elsif (select_pin < (35+16)) then
					LAPP_IPM_IO(select_pin-35)  <= 'Z';
					CPLD_RDATA                  <= LAPP_IPM_IO(select_pin-35);
				
				end if;
				
				io_cmd_done <= '1';
				
			end if;
			
		end if;
	
	end process;
   
end structural;