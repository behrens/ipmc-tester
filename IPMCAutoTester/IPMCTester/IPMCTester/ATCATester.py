#!/usr/bin/env python
# encoding: utf-8
import time

from IPMCDevKit.IPMCDevLib import IPMCDevATCA

class ATCATester:
    
    def __init__(self, IPMCDevATCAObject):  
        
        IPMCDevATCAObject.ASSERTPGOODA()
        IPMCDevATCAObject.ASSERTPGOODB()
        
        setha = IPMCDevATCAObject.SETHA(0x00)
        timeout = 0
        while IPMCDevATCAObject.GET12VEN() != 0 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            raise ValueError('ATCA Power ON timeout')
            
        setha = IPMCDevATCAObject.SETHA(0x43)              
        IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.VCC)
    
        timeout = 0
        while IPMCDevATCAObject.GET12VEN() != 1 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            raise ValueError('ATCA Power ON timeout')
        
        self.IPMCDevATCAObject = IPMCDevATCAObject

    def testATCAPowerCycle(self):
    
        # Close handle switch
        self.IPMCDevATCAObject.CONFHSW(self.IPMCDevATCAObject.VCC)
        
        # Wait for 12V Enable
        timeout = 0
        while self.IPMCDevATCAObject.GET12VEN() != 1 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            return -3
    
        # Open handle switch
        self.IPMCDevATCAObject.CONFHSW(self.IPMCDevATCAObject.GND)
        
        # Wait for 12V Enable OFF
        timeout = 0
        while self.IPMCDevATCAObject.GET12VEN() != 0 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            return -6

        # Wait for BLUE Led ON (Ready)
        timeout = 0
        while self.IPMCDevATCAObject.GETBLUELED() != 1 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            return -2
            
        # Close handle switch
        self.IPMCDevATCAObject.CONFHSW(self.IPMCDevATCAObject.VCC)
        
        # Wait for 12V Enable
        timeout = 0
        while self.IPMCDevATCAObject.GET12VEN() != 1 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            return -7
        
        # De-assert PGOOD signals
        self.IPMCDevATCAObject.DEASSERTPGOODA()
        self.IPMCDevATCAObject.DEASSERTPGOODB()
        
        # Wait for 12V Enable OFF
        timeout = 0
        while self.IPMCDevATCAObject.GET12VEN() != 0 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            return -4
            
        self.IPMCDevATCAObject.ASSERTPGOODA()
        self.IPMCDevATCAObject.ASSERTPGOODB()
        
        self.IPMCDevATCAObject.CONFHSW(self.IPMCDevATCAObject.GND)
        time.sleep(1)
        self.IPMCDevATCAObject.CONFHSW(self.IPMCDevATCAObject.VCC)
        time.sleep(1)
        
        # Wait for 12V Enable
        timeout = 0
        while self.IPMCDevATCAObject.GET12VEN() != 1 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            return -8
        
        return 0