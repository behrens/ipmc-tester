#!/usr/bin/env python
# encoding: utf-8
import time

from IPMCDevKit.IPMCDevLib import IPMCDevATCA
from . import IPMCIPMICom

class IOTester:
    
    def __init__(self, IPMCDevATCAObject, ipAddr):  
    
        setha = IPMCDevATCAObject.SETHA(0x00)
        timeout = 0
        while IPMCDevATCAObject.GET12VEN() != 0 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            raise ValueError('ATCA Power ON timeout')
            
        setha = IPMCDevATCAObject.SETHA(0x43)              
        IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.VCC)
    
        timeout = 0
        while IPMCDevATCAObject.GET12VEN() != 1 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            raise ValueError('ATCA Power ON timeout')
        
        self.IPMCDevATCAObject = IPMCDevATCAObject    
        self.IPMCIPMIComObject = IPMCIPMICom(ipAddr)

    def readIPMCUSERIOs(self):
    
        compcode = 0
        id = 0
        
        arr = []
        while compcode != 0xc9:
            getio = self.IPMCIPMIComObject.sendIPMICommand(46, 72, [96, 0, 0, id]) #Configure I/O in input
            compcode = getio[0]
            if compcode != 0xc9:
                arr.append({'id': id, 'value': getio[4]})
                
            id = id + 1
            
        return arr

    def activateIPMCUSERIOs(self, id):    
        iostate = self.IPMCDevATCAObject.GETIO(id)
        self.IPMCIPMIComObject.sendIPMICommand(46, 0x44, [96, 0, 0, id])
        return self.IPMCDevATCAObject.GETIO(id)

    def deactivateIPMCUSERIOs(self, id):    
        iostate = self.IPMCDevATCAObject.GETIO(id)
        self.IPMCIPMIComObject.sendIPMICommand(46, 0x46, [96, 0, 0, id])
        return self.IPMCDevATCAObject.GETIO(id)
    
    def readActiveIPMCUSERIO(self, id):   
        getio = self.IPMCIPMIComObject.sendIPMICommand(46, 72, [96, 0, 0, id]) #Configure I/O in input
        self.IPMCDevATCAObject.SETIO(id, self.IPMCDevATCAObject.GND)
        getio = self.IPMCIPMIComObject.sendIPMICommand(46, 72, [96, 0, 0, id]) #Configure I/O in input
        return getio[4]
    
    def readInactiveIPMCUSERIO(self, id):   
        getio = self.IPMCIPMIComObject.sendIPMICommand(46, 72, [96, 0, 0, id]) #Configure I/O in input
        self.IPMCDevATCAObject.SETIO(id, self.IPMCDevATCAObject.VCC)
        getio = self.IPMCIPMIComObject.sendIPMICommand(46, 72, [96, 0, 0, id]) #Configure I/O in input
        return getio[4]
        
    def testUSERIO(self, id):    
        if self.activateIPMCUSERIOs(id) != 0:
            return -1
           
        if self.deactivateIPMCUSERIOs(id) != 1: 
            return -2
            
        if self.readActiveIPMCUSERIO(id) != 1:
            return -3
            
        if self.readInactiveIPMCUSERIO(id) != 0:
            return -4
            
        return 0