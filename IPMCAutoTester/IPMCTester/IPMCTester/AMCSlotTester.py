#!/usr/bin/env python
# encoding: utf-8
import time

from IPMCDevKit.IPMCDevLib import IPMCDevATCA
from IPMCDevKit.IPMCDevLib import IPMCDevAMC

class AMCSlotTester:
    
    def __init__(self, IPMCDevATCAObject, IPMCDevAMCObject):  
    
        IPMCDevAMCObject.EXTRACTAMC()
    
        setha = IPMCDevATCAObject.SETHA(0x00)
        timeout = 0
        while IPMCDevATCAObject.GET12VEN() != 0 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            raise ValueError('ATCA Power ON timeout')
            
        setha = IPMCDevATCAObject.SETHA(0x43)              
        IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.VCC)
    
        timeout = 0
        while IPMCDevATCAObject.GET12VEN() != 1 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            raise ValueError('ATCA Power ON timeout')
        
        self.IPMCDevATCAObject = IPMCDevATCAObject
        self.IPMCDevAMCObject = IPMCDevAMCObject

    def testAMCPort(self, port, targetAddr):

        self.IPMCDevAMCObject.EXTRACTAMC()
        
        self.IPMCDevAMCObject.SELAMC(port)
        
        self.IPMCDevAMCObject.SETGA(targetAddr)
        self.IPMCDevAMCObject.ASSERTMPGOOD()
        self.IPMCDevAMCObject.ASSERTPPGOOD()
        self.IPMCDevAMCObject.CLOSEHSW()
        
        self.IPMCDevAMCObject.INSERTAMC()
        
        #exit(0)
        
        timeout = 0
        while self.IPMCDevAMCObject.GETPPEN() != 1 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            self.IPMCDevAMCObject.EXTRACTAMC()
            return -1

        self.IPMCDevAMCObject.DEASSERTPPGOOD()
        
        timeout = 0
        while self.IPMCDevAMCObject.GETPPEN() != 0 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            self.IPMCDevAMCObject.EXTRACTAMC()
            return -2
        
        self.IPMCDevAMCObject.DEASSERTMPGOOD()
        
        timeout = 0
        while self.IPMCDevAMCObject.GETMPEN() != 0 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            self.IPMCDevAMCObject.EXTRACTAMC()
            return -3
        
        self.IPMCDevAMCObject.EXTRACTAMC()
        
        return 0
