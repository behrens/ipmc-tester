#!/usr/bin/env python
# encoding: utf-8
import time
import random

from IPMCDevKit.IPMCDevLib import IPMCDevATCA
from . import IPMCIPMICom

class I2CTester:
    
    def __init__(self, IPMCDevATCAObject, ipAddr):  
    
        setha = IPMCDevATCAObject.SETHA(0x00)
        timeout = 0
        while IPMCDevATCAObject.GET12VEN() != 0 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            raise ValueError('ATCA Power ON timeout')
            
        setha = IPMCDevATCAObject.SETHA(0x43)              
        IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.VCC)
    
        timeout = 0
        while IPMCDevATCAObject.GET12VEN() != 1 and timeout < 1000:
            time.sleep(0.1)
            timeout = timeout + 1
            
        if timeout >= 50:
            raise ValueError('ATCA Power ON timeout')
        
        self.IPMCDevATCAObject = IPMCDevATCAObject    
        self.IPMCIPMIComObject = IPMCIPMICom(ipAddr)

    def testMGTI2c(self, count):
        for i in range(0,count):
            addrMSB = random.randint(0, 254)
            addrLSB = random.randint(0, 254)
            val = random.randint(0, 254)
            geti2c = self.IPMCIPMIComObject.sendIPMICommand(46, 0x50, [96, 0, 0, addrMSB, addrLSB, val]) #Configure I/O in input
        
            if geti2c[4] != val:
                return -1
            
        return 0

    def testSensorI2c(self):    
        geti2c = self.IPMCIPMIComObject.sendIPMICommand(46, 0x51, [96, 0, 0, 140])
        if geti2c[4] != 0xFF:
			return -1
			
        geti2c = self.IPMCIPMIComObject.sendIPMICommand(46, 0x51, [96, 0, 0, 144])
        if geti2c[4] != 0x00:
			return -2

        return 0