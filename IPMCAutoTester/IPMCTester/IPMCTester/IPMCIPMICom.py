#!/usr/bin/env python
# encoding: utf-8
import ctypes
import os
import subprocess

class IPMCIPMICom:
    
    def __init__(self, ipaddr):    
        #Load DLL into memory.        
        filePath = os.path.dirname(os.path.abspath(__file__))           
        self.IPMIDll = ctypes.CDLL(filePath+"/libmtca.dll")
        
        self.ipaddr = ipaddr
        
        response = -1
        timeout = 0
        
        while response != 0 and timeout < 50:
            proc = subprocess.Popen(["ping","-n","1", "-w", "5", ipaddr], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            response = proc.returncode
            timeout = timeout + 1
            
        if timeout >= 50:
            raise ValueError('Connection error: '+ipaddr+' cannot be reached')
            
    def sendIMPBLCommand(self, target, netFn, command):
    
        reply = (ctypes.c_ubyte*128)()
        data = (ctypes.c_byte*8)()
        
        data[0] = 0x47
        data[1] = ctypes.c_byte(target)
        data[2] = ctypes.c_byte(netFn*4)
        data[3] = ctypes.c_byte(0 - target + (netFn*4))
        data[4] = 0x20
        data[5] = 0
        data[6] = ctypes.c_byte(command)
        data[7] = ctypes.c_byte(0 - (0x20 + command))
        
        
        ret = self.IPMIDll.send_ipmi_cmd(self.ipaddr, "", "", 0x06, 0x52, ctypes.byref(data), 8, ctypes.byref(reply))
        
        for r in reply:
            print('Rep: {:02X}'.format(r))

    def sendIPMICommand(self, netFn, command, data):
    
        reply = (ctypes.c_ubyte*128)()
        cbytedata = (ctypes.c_byte*len(data))()
                
        i = 0
        for d in data:
            cbytedata[i] = ctypes.c_byte(d)
            i = i + 1
                
        ret = self.IPMIDll.send_ipmi_cmd(self.ipaddr, "", "", netFn, command, ctypes.byref(cbytedata), len(data), ctypes.byref(reply))

        arr = []
        for r in reply:
            arr.append(r)
            
        return arr