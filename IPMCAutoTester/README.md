# CERN-IPMC Tester

This tools test automatically the CERN-IPMC. In order to check all of the interfaces, the mezzanine card must be configured using the binaries located into the IPMCBinaries folder.
The configuration procedure is described in the README file located into this specific folder.

# How-To

This python library was designed using python 2.7.14

## Dependencies

List of dependency(ies): 
- <b>pySerial</b> (installation command: pip install pyserial)
- <b>colorama</b> (installation command: pip install colorama)
