#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import time

from IPMCDevKit.IPMCDevLib import IPMCDevCom
from IPMCDevKit.IPMCDevLib import IPMCDevMgt
from IPMCDevKit.IPMCDevLib import IPMCDevATCA
from IPMCDevKit.IPMCDevLib import IPMCDevAMC

from IPMCTester.IPMCTester import IPMCIPMICom
from IPMCTester.IPMCTester import AMCSlotTester
from IPMCTester.IPMCTester import ATCATester
from IPMCTester.IPMCTester import IOTester
from IPMCTester.IPMCTester import I2CTester

from colorama import init

#Initialization
init()
print('')

if sys.version_info[0] < 2 or sys.version_info[0] >=3 or sys.version_info[0] == 2 and sys.version_info[1] < 4:
    print "\n\tERROR: Python 2.xx (2.4 or higher) is required to run this program.\n\tPython version 3.xx is not supported.\n\tYou can download Python from http://www.python.org" 
    sys.exit(-1)

# Tester
#========
if __name__ == '__main__':

    IPMCDevObject = IPMCDevCom()
    IPMCDevMgtObject = IPMCDevMgt(IPMCDevObject)
    IPMCDevATCAObject = IPMCDevATCA(IPMCDevObject)
    IPMCDevAMCObject = IPMCDevAMC(IPMCDevObject)
    
    #+++++++++++++++++++++++++++++++++++++
    #+ First test: Read voltages/current +
    #+++++++++++++++++++++++++++++++++++++    
    print("\033[1mLife check: voltages and IPMC current\033[0m")
        
    testerVoltage = IPMCDevMgtObject.GETMGTVOLTAGE()
    ipmcVoltage = IPMCDevMgtObject.GETIPMCVOLTAGE()
    ipmcCurrent = IPMCDevMgtObject.GETIPMCCURRENT()

    print("\tTester voltage: {} Volts".format(testerVoltage))
    print("\tIPMC Voltage: {} Volts".format(ipmcVoltage))
    print("\tIPMC Current: {} Amps".format(ipmcCurrent)) 
    
    if testerVoltage > 3.4 or testerVoltage < 3.1:
        print("\t\033[91m[Error] Tester voltage is out of range \033[0m")
        
    if ipmcVoltage > 3.4 or ipmcVoltage < 3.1:
        print("\t\033[91m[Error] IPMC voltage is out of range \033[0m")
        
    if ipmcCurrent > 0.3:
        print("\t\033[91m[Error] IPMC current is out of range \033[0m")
    
    print('')
    
    #+++++++++++++++++++++++++++++++++++
    #+ Second test: test ATCA features +
    #+++++++++++++++++++++++++++++++++++
    print("\033[1mATCA Power cycle\033[0m")
    ATCATesterObject = ATCATester(IPMCDevATCAObject)
    
    atcatester = ATCATesterObject.testATCAPowerCycle()
    if atcatester == 0:
        print("\tATCA Power cycle: success")
    elif atcatester == -1:
        print("\t\033[91m[Error] IPMC not in reset \033[0m")
    elif atcatester == -2:
        print("\t\033[91m[Error] Blue led is not ON \033[0m")
    elif atcatester == -3:
        print("\t\033[91m[Error] Payload power remains OFF after handle switch closed\033[0m")
    elif atcatester == -4:
        print("\t\033[91m[Error] Payload power remains ON after PGOOD signals are de-asserted\033[0m")
    elif atcatester == -5:
        print("\t\033[91m[Error] Payload power is ON before closing the handle switch\033[0m")
    elif atcatester == -6:
        print("\t\033[91m[Error] Payload power did not go OFF after opening the handle switch\033[0m")
    elif atcatester == -7:
        print("\t\033[91m[Error] Payload power did not go ON after closing the handle switch\033[0m")
    elif atcatester == -8:
        print("\t\033[91m[Error] Payload power remains OFF after asserting back the PGOOD signals\033[0m")
    
    print('')
    
    #++++++++++++++++++++++++++++++
    #+ Third test: Test AMC ports +
    #++++++++++++++++++++++++++++++  
    print("\033[1mAMC Check: Test AMC slots from 0 to 7\033[0m")
    
    AMCSlots = [
            {
                'port': 0,
                'target': 0xea
            },
            {
                'port': 1,
                'target': 0x72
            },
            {
                'port': 2,
                'target': 0x74
            },
            {
                'port': 3,
                'target': 0x76
            },
            {
                'port': 4,
                'target': 0x78
            },
            {
                'port': 5,
                'target': 0x7a
            },
            {
                'port': 6,
                'target': 0x7c
            },
            {
                'port': 7,
                'target': 0x7e
            }
        ]
        
    AMCSlotTesterObject = AMCSlotTester(IPMCDevATCAObject, IPMCDevAMCObject)
    
    for slot in AMCSlots:
        amctest = AMCSlotTesterObject.testAMCPort(slot['port'], slot['target']) 
        if amctest == 0:
            print("\tPort {}: Success".format(slot['port']))
        elif amctest == -1:
            print("\t[Error] Port {}: AMC Power ON timeout".format(slot['port']))
        elif amctest == -2:
            print("\t[Error] Port {}: Payload power failure not detected".format(slot['port']))
        elif amctest == -3:
            print("\t[Error] Port {}: Management power issue not detected".format(slot['port']))
    
    print('')
    
    #+++++++++++++++++++++++++++++++
    #+ Fourth test: Test IPMC I/Os +
    #+++++++++++++++++++++++++++++++
    print("\033[1mIPMC's I/O check: write and read\033[0m")
    
    IOTesterObjects = IOTester(IPMCDevATCAObject, '192.168.1.34')
    
    #print("Get VCC: {}".format(IOTesterObjects.deactivateIPMCUSERIOs(15)))
    #print("Get GND: {}".format(IOTesterObjects.activateIPMCUSERIOs(15)))
    #print("Active IO ('1' / GND): {}".format(IOTesterObjects.readActiveIPMCUSERIO(15)))
    #print("Inactive IO ('0' / VCC): {}".format(IOTesterObjects.readInactiveIPMCUSERIO(15)))
    
    #while True:
    #    print('Pin 20: {}'.format(IOTesterObjects.readIPMCUSERIOs()[20]['value']))
    for i in range(0,35):        
        iotest = IOTesterObjects.testUSERIO(i)
        if iotest == 0:
            print("\tUser I/O {}: Success".format(i))
        elif iotest == -1:
            print("\t\033[91m[Error] Activate I/O failed \033[0m")
        elif iotest == -2:
            print("\t\033[91m[Error] De-activate I/O failed \033[0m")
        elif iotest == -3:
            print("\t\033[91m[Error] Get activated I/O failed \033[0m")
        elif iotest == -4:
            print("\t\033[91m[Error] Get de-activated I/O failed \033[0m")
    print('')
    
    #++++++++++++++++++++++++++++++
    #+ Fifth test: Test IPMC I/Os +
    #++++++++++++++++++++++++++++++
    print("\033[1mIPMC's I2C check\033[0m")
    
    I2CTesterObjects = I2CTester(IPMCDevATCAObject, '192.168.1.34')
    i2cMgt = I2CTesterObjects.testMGTI2c(10)
    if i2cMgt == 0:
        print("\tTest MGT I2C: Success".format(i+1))
    else:
        print("\t\033[91m[Error] Mgt I2c read/write failed \033[0m")
    
    i2cSensor = I2CTesterObjects.testSensorI2c()
    if i2cSensor == 0:
        print("\tTest Sensor I2C: Success".format(i+1))
    elif i2cSensor == -1:
        print("\t\033[91m[Error] No reply on sensor polling \033[0m")
    elif i2cSensor == -2:
        print("\t\033[91m[Error] Reply received when a wrong sensor address is polled \033[0m")
