/*
 * CPLD_management.c
 *
 * Created: 08/05/2017 15:44:54
 *  Author: jumendez
 */
 #include <avr/io.h>

void clock_cycle(){
	
	PORTG &= 0x0D;	// Clear CLK pin
	PORTG |= 0x02;  // Set CLK pin
	PORTG &= 0x0D;	// Clear CLK pin	
	
}

unsigned char get_write_done(){
	return (PIND & 0x40) ? 0xFF : 0x00;
}

void CPLD_set_reset(){
	PORTE = 0xFF;
}

void CPLD_clr_reset(){
	PORTE = 0x00;
}

void write_config(unsigned char cmd){
	PORTE = cmd;
	PORTD |= 0x20;
	clock_cycle();
	PORTD &= 0xDF;
	
	while(get_write_done() == 0x00)
		clock_cycle();	
	
	clock_cycle();
}

void select_amc_port(unsigned char amcslot){	
	write_config(0xC0 | (amcslot & 0x0F));	
}

signed char set_amc_ga(unsigned char amc_ga){
	switch (amc_ga){
		case 0x72: write_config(0xD0); return 0;
		case 0x74: write_config(0xD1); return 0;
		case 0x76: write_config(0xD2); return 0;
		case 0x78: write_config(0xD3); return 0;
		case 0x7a: write_config(0xD4); return 0;
		case 0x7c: write_config(0xD5); return 0;
		case 0x7e: write_config(0xD6); return 0;
		case 0x80: write_config(0xD7); return 0;
		case 0xEA: write_config(0xD8); return 0;
		default: return -1;
	}
	
	return -1;
}

unsigned char get_io(unsigned char io_id){
	write_config((0x80 | (io_id & 0x3F)));
	return (PIND & 0x80) ? 0x01 : 0x00;
}

void set_io(unsigned char io_id){
	write_config((0x40 | (io_id & 0x3F)));
	return;
}

void clr_io(unsigned char io_id){
	write_config((0x00 | (io_id & 0x3F)));
	return;
}