/*
 * pinout.h
 *
 * Created: 24/05/2017 09:57:34
 *  Author: jumendez
 */ 


#ifndef PINOUT_H_
#define PINOUT_H_

//IPMC pins
#define PAYLD_nRST		conf_pin(PORT_C, 0)

#define HA0				conf_pin(PORT_A, 0)
#define HA2				conf_pin(PORT_A, 1)
#define HA4				conf_pin(PORT_A, 2)
#define HA6				conf_pin(PORT_A, 3)
#define HA7				conf_pin(PORT_A, 4)
#define HA5				conf_pin(PORT_A, 5)
#define HA3				conf_pin(PORT_A, 6)
#define HA1				conf_pin(PORT_A, 7)

#define HSWITCH			conf_pin(PORT_C, 1)
#define V12_Enable		conf_pin(PORT_C, 2)
#define Alarm_A			conf_pin(PORT_C, 3)
#define Alarm_B			conf_pin(PORT_C, 4)
#define PGood_A			conf_pin(PORT_C, 5)
#define PGood_B			conf_pin(PORT_C, 6)

#define BLUE_LED		conf_pin(PORT_F, 0
#define LED_1			conf_pin(PORT_F, 1)
#define LED_2			conf_pin(PORT_F, 2)
#define LED_3			conf_pin(PORT_F, 3)

//AMC pins
#define AMC_PS1			conf_pin(PORT_B, 0)
#define AMC_Enable		conf_pin(PORT_B, 1)
#define AMC_IPMBL_En	conf_pin(PORT_B, 2)
#define AMC_MP_En		conf_pin(PORT_B, 3)
#define AMC_PP_En		conf_pin(PORT_B, 4)
#define AMC_MP_Good		conf_pin(PORT_B, 5)
#define AMC_PP_Good		conf_pin(PORT_B, 6)
#define AMC_PWR_Oring	conf_pin(PORT_B, 7)
#define AMC_Handle		conf_pin(PORT_D, 4)

//I2C interface
#define MGT_Mon_SCL		conf_pin(PORT_D, 0)
#define MGT_Mon_SDA		conf_pin(PORT_D, 1)

//UART interface
#define UART_Rx			conf_pin(PORT_D, 2)
#define UART_Tx			conf_pin(PORT_D, 3)

//CPLD interface
#define CPLD_wr			conf_pin(PORT_D, 5)
#define CPLD_done		conf_pin(PORT_D, 6)
#define CPLD_rddata		conf_pin(PORT_D, 7)
#define CPLD_clk		conf_pin(PORT_G, 1)

#define CPLD_D0			conf_pin(PORT_E, 0)
#define CPLD_D1			conf_pin(PORT_E, 1)
#define CPLD_D2			conf_pin(PORT_E, 2)
#define CPLD_D3			conf_pin(PORT_E, 3)
#define CPLD_D4			conf_pin(PORT_E, 4)
#define CPLD_D5			conf_pin(PORT_E, 5)
#define CPLD_D6			conf_pin(PORT_E, 6)
#define CPLD_D7			conf_pin(PORT_E, 7)

//I2C selection interface
#define SEL_I2C_0		conf_pin(PORT_G, 2)
#define SEL_I2C_1		conf_pin(PORT_C, 7)
#define SEL_I2C_2		conf_pin(PORT_G, 0)
#define SEL_I2C_3		conf_pin(PORT_G, 3)
#define SEL_I2C_4		conf_pin(PORT_G, 4)

#endif /* PINOUT_H_ */