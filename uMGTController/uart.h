/*
 * uart.h
 *
 * Created: 24/04/2017 14:29:16
 *  Author: jumendez
 */ 


#ifndef UART_H_
#define UART_H_

FILE *USART_Init();
void USART_Transmit( unsigned char data );
unsigned char USART_Receive( void );
int USART_wait_for_command(unsigned char *command);
int USART_print(unsigned char *str);
int printCHAR(char character, FILE *stream);

#define MAX_COMMAND_LEN		255

#endif /* UART_H_ */