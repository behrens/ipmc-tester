/*
 * IPMC_management.h
 *
 * Created: 08/05/2017 12:02:24
 *  Author: jumendez
 */ 


#ifndef IPMC_MANAGEMENT_H_
#define IPMC_MANAGEMENT_H_

void set_hardware_addr(unsigned char addr);
void set_handle_swtich();
void clear_handle_swtich();
void reset_IPMC();
unsigned char get_blue_led_status();
unsigned char get_12vEn_status();

#endif /* IPMC_MANAGEMENT_H_ */