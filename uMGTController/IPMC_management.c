/*
 * IPMC_management.c
 *
 * Created: 08/05/2017 11:04:36
 *  Author: jumendez
 */ 

#include <avr/io.h>
#include <stdio.h>

#include "IPMC_management.h"
#include "gpio_intf.h"
#include "pinout.h"
#include "tools.h"

void set_hardware_addr(unsigned char addr){
	set_signal_val(HA0, (addr & 0x01) ? HIGH : LOW);
	set_signal_val(HA1, (addr & 0x02) ? HIGH : LOW);
	set_signal_val(HA2, (addr & 0x04) ? HIGH : LOW);
	set_signal_val(HA3, (addr & 0x08) ? HIGH : LOW);
	set_signal_val(HA4, (addr & 0x10) ? HIGH : LOW);
	set_signal_val(HA5, (addr & 0x20) ? HIGH : LOW);
	set_signal_val(HA6, (addr & 0x40) ? HIGH : LOW);
	set_signal_val(HA7, (addr & 0x80) ? HIGH : LOW);
	
	reset_IPMC();
}

void set_handle_swtich(){
	unsigned char portc_v = PORTC;
	portc_v &= 0xFD;
	PORTC = portc_v;
}

void clear_handle_swtich(){
	unsigned char portc_v = PORTC;
	portc_v |= 0x02;
	PORTC = portc_v;
}

void reset_IPMC(){
	unsigned char ddrc_v = DDRC, ddrc_o = DDRC;
	unsigned char portc_v = PORTC, portc_o = PORTC;
	
	ddrc_v |= 0x01;					//Change PC0 from input to output (PAYLD_nRST)
	portc_v &= 0xFE;				//Set PC0 to '0'
	
	DDRC = ddrc_v;
	PORTC = portc_v;
	
	delay_us(100);
	
	PORTC = portc_o;
	DDRC = ddrc_o;
}

unsigned char get_blue_led_status(){
	return (PINF & 0x01) ? 0xFF : 0x00;
}

unsigned char get_12vEn_status(){
	return (PINC & 0x04) ? 0xFF : 0x00;
}