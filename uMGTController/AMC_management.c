/*
 * AMC_management.c
 *
 * Created: 08/05/2017 17:52:46
 *  Author: jumendez
 */ 
#include <avr/io.h>
#include "AMC_management.h"

void set_ps_gnd(){
	PORTB &= 0xFE;
}

void set_ps_vcc(){
	PORTB |= 0x01;
}

unsigned char get_amc_status(){
	return PINB;
}

void close_AMC_handle(){
	PORTD &= 0xEF;
}

void open_AMC_handle(){
	PORTD |= 0x10;
}

unsigned char get_mpen_status(){
	return (PINB & 0x08) ? 0xFF : 0x00;
}

unsigned char get_ppen_status(){
	return (PINB & 0x10) ? 0xFF : 0x00;
}