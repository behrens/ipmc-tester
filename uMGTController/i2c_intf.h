/*! \file i2csw.h \brief Software I2C interface using port pins. */
//*****************************************************************************
//
// File Name	: 'i2csw.h'
// Title		: Software I2C interface using port pins
// Author		: Pascal Stang
// Created		: 11/22/2000
// Revised		: 5/2/2002
// Version		: 1.1
// Target MCU	: Atmel AVR series
// Editor Tabs	: 4
//
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
///	\ingroup driver_sw
/// \defgroup i2csw Software I2C Serial Interface Function Library (i2csw.c)
/// \code #include "i2csw.h" \endcode
/// \par Overview
///		This library provides a very simple bit-banged I2C serial interface.
/// The library supports MASTER mode send and receive of single or multiple
/// bytes.  Thanks to the standardization of the I2C protocol and register
/// access, the send and receive commands are everything you need to talk to
/// thousands of different I2C devices including: EEPROMS, Flash memory,
/// MP3 players, A/D and D/A converters, electronic potentiometers, etc.
///
/// Although some AVR processors have built-in hardware to help create an I2C
/// interface, this library does not require or use that hardware.
///
/// For general information about I2C, see the i2c library.
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//*****************************************************************************

#ifndef I2CINTF_H
#define I2CINTF_H

// Constants
#define READ            0x01  // I2C READ bit

// Macros
#define I2C_SDL_LO      set_signal_dir(sda_pin, OUTPUT)   // Set pin as output and force a '0'
#define I2C_SDL_HI      set_signal_dir(sda_pin, INPUT)    // Set pin as input and release line
#define I2C_SCL_LO      set_signal_dir(scl_pin, OUTPUT)	  // Set pin as output and force a '0'
#define I2C_SCL_HI      set_signal_dir(scl_pin, INPUT)    // Set pin as input and release line

#define I2C_SCL_TOGGLE  { HDEL; I2C_SCL_HI; HDEL; I2C_SCL_LO; }       //MJ: As these macros consist of several sub-commands it is better to put them into "{}"
#define I2C_START       { I2C_SDL_LO; QDEL; I2C_SCL_LO; }             //MJ: Consequently they should be called without a ";" at the end
#define I2C_STOP        { HDEL; I2C_SCL_HI; QDEL; I2C_SDL_HI; HDEL; }
#define HQDEL           { asm volatile("nop"); asm volatile("nop"); asm volatile("nop"); asm volatile("nop"); asm volatile("nop"); }      // i2c half quarter-bit delay
#define QDEL			{ HQDEL; HQDEL; }  
#define HDEL            { QDEL; QDEL; QDEL; QDEL;}                                                                                                   // i2c half-bit delay
	
// Functions
void i2c_init(unsigned char sda_pin_i, unsigned char scl_pin_i);
unsigned char i2cPutbyte(unsigned char b);
unsigned char i2cGetbyte(unsigned int last);

void ext_i2c_send(unsigned char device, unsigned short subAddr, unsigned char nsub, unsigned char length, unsigned char *data);
unsigned char ext_i2c_received(unsigned char device, unsigned short subAddr, unsigned char nsub, unsigned char length, unsigned char *data);
unsigned char ext_i2c_poll(unsigned char addr);

#endif
