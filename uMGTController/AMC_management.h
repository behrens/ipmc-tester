/*
 * AMC_management.h
 *
 * Created: 08/05/2017 17:59:36
 *  Author: jumendez
 */ 


#ifndef AMC_MANAGEMENT_H_
#define AMC_MANAGEMENT_H_

void set_ps_gnd();
void set_ps_vcc();
unsigned char get_amc_status();
void close_AMC_handle();
void open_AMC_handle();
unsigned char get_mpen_status();
unsigned char get_ppen_status();

#endif /* AMC_MANAGEMENT_H_ */