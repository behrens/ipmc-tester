/*
 * uart.c
 *
 * Created: 24/04/2017 14:21:42
 *  Author: jumendez
 */ 
#include <stdio.h>
#include <string.h>
#include <avr/io.h>

#include "uart.h"
#include "tools.h"

#define BAUD 9600                                   // define baud
#define BAUDRATE ((F_CPU)/(BAUD*16UL)-1)            // set baud rate value for UBRR

FILE uart_str = FDEV_SETUP_STREAM(printCHAR, NULL, _FDEV_SETUP_RW);

FILE * USART_Init(){
	
	unsigned char ddrd_v = DDRD;
	
	ddrd_v |= 0x04;
	ddrd_v &= 0xF7;
	
	DDRD = ddrd_v;	
	
	UBRR1H  = (BAUDRATE>>8);						// shift the register right 7by 8 bits
	UBRR1L  = BAUDRATE;								// set baud rate
	UCSR1B |= (1<<TXEN1)|(1<<RXEN1);                // enable receiver and transmitter
	UCSR1C |= (1<<UCSZ01)|(1<<UCSZ11);				// 8bit data format
	
	return &uart_str;
}



void USART_Transmit( unsigned char data ){

	/* Wait for empty transmit buffer */
	while ( !( UCSR1A & (1<<UDRE1)) );

	/* Put data into buffer, sends the data */
	UDR1 = data;
}



unsigned char USART_Receive( void ){
	
	/* Wait for data to be received */
	while ( !(UCSR1A & (1<<RXC1)) );

	/* Get and return received data from buffer */
	return UDR1;
}

unsigned char rec;
	
int USART_wait_for_command(unsigned char *command){

	unsigned ptr=0;
	unsigned char prev_cmd[MAX_COMMAND_LEN];
	
	memcpy(prev_cmd, command, MAX_COMMAND_LEN);
	
	while (1){
		rec = USART_Receive();
		
		if((rec == '\n' || rec == '\r') && ptr > 0){
			if(command[0] == 0x1b && command[1] == 0x5b && command[2] == 0x41)
				memcpy(command, prev_cmd, MAX_COMMAND_LEN);
			else
				command[ptr] = 0;
				
			return 0;
			
		}else if((rec == '\n' || rec == '\r') && ptr <= 0){
			//Send previous command
			memcpy(command, prev_cmd, MAX_COMMAND_LEN);
			return 0;
			
		}else if(ptr >= MAX_COMMAND_LEN){
			return -1;
			
		}else{
			command[ptr] = rec;
			ptr++;
			
		}
	}	
	
	return -2;
}

int printCHAR(char character, FILE *stream){
	USART_Transmit(character);
	return 0;
}


int USART_print(unsigned char *str){
	unsigned ptr=0;
	while(str[ptr] != 0)	USART_Transmit(str[ptr++]);	
	
	return ptr;
}