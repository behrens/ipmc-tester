/*
 * tools.h
 *
 * Created: 08/05/2017 12:06:51
 *  Author: jumendez
 */ 


#ifndef TOOLS_H_
#define TOOLS_H_

#ifndef F_CPU
	#define F_CPU 4000000
#endif

#define CYCLES_PER_US ((F_CPU + 500000) / 1000000) 	// CPU cycles per microsecond

#ifndef cbi
	#define cbi(reg, bit)	    reg &= ~(BV(bit))
#endif

#ifndef sbi
	#define sbi(reg, bit)	    reg |= (BV(bit))
#endif

#ifndef outb
	#define	outb(addr, data)	addr = (data)
#endif

#ifndef inb
	#define	inb(addr)			(addr)
#endif

#ifndef BV
	#define BV(bit)			    (1 << (bit))
#endif

void delay_us(unsigned short time_us);

#endif /* TOOLS_H_ */