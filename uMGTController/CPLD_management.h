/*
 * CPLD_management.h
 *
 * Created: 09/05/2017 10:17:45
 *  Author: jumendez
 */ 


#ifndef CPLD_MANAGEMENT_H_
#define CPLD_MANAGEMENT_H_

void select_amc_port(unsigned char amcslot);
signed char set_amc_ga(unsigned char amc_ga);
void set_io(unsigned char io_id);
void clr_io(unsigned char io_id);
unsigned char get_io(unsigned char io_id);

void CPLD_set_reset();
void CPLD_clr_reset();

#endif /* CPLD_MANAGEMENT_H_ */