/*
* testpad_manager.c
*
* Created: 24/05/2017 09:44:46
*  Author: jumendez
*/

#include <avr/io.h>

#include "testpad_manager.h"
#include "gpio_intf.h"

void sel_i2c_port(unsigned char port){
	
	clear_signal(SEL_I2C_0);
	clear_signal(SEL_I2C_1);
	clear_signal(SEL_I2C_2);
	clear_signal(SEL_I2C_3);
	clear_signal(SEL_I2C_4);
	
	switch(port){
		case 0x00: set_signal(SEL_I2C_0); break;  //IPMB-A
		case 0x01: set_signal(SEL_I2C_1); break;  //IPMB-B
		case 0x02: set_signal(SEL_I2C_2); break;  //IPMB-L
		case 0x03: set_signal(SEL_I2C_3); break;  //IPMC Sensor
		case 0x04: set_signal(SEL_I2C_4); break;  //IPMC Mgt 
	}
}