/*
 * gpio_intf.h
 *
 * Created: 24/05/2017 09:53:56
 *  Author: jumendez
 */ 


#ifndef GPIO_INTF_H_
#define GPIO_INTF_H_

#include "pinout.h"

#define conf_pin(port, pin_id)		(port | pin_id)

//Port definition
#define PORT_A		0x00
#define PORT_B		0x10
#define PORT_C		0x20
#define PORT_D		0x30
#define PORT_E		0x40
#define PORT_F		0x50
#define PORT_G		0x60

//IO state
#define HIGH                                                    1
#define LOW                                                     0
#define ACTIVE                                                  0xFF
#define INACTIVE                                                0x00

//IO Direction
#define INPUT                                                   0x01
#define OUTPUT                                                  0x00

unsigned char get_signal(unsigned char id);
void set_signal(unsigned char id);
void clear_signal(unsigned char id);
void set_signal_val(unsigned char id, unsigned char state);
void set_signal_dir(unsigned char id, unsigned char inout);
void init_port(void);

#endif /* GPIO_INTF_H_ */